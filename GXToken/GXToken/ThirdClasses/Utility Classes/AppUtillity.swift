//
//  AppUtillity.swift
//  GXToken
//
//  Created by Shorupan Pirakaspathy on 2020-03-27.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

@objc class AppUtillity: NSObject {
    static let shared = AppUtillity()
    
    @objc class var swiftSharedInstance: AppUtillity {
        struct Singleton {
            static let instance = AppUtillity()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc class func sharedInstance() -> AppUtillity {
        return AppUtillity.swiftSharedInstance
    }
    
    @objc public func NetworkIndicator(status: Bool) {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = status
//            UserUtility.getRootViewcontroller().view.isUserInteractionEnabled = !status
        }
    }
    
    @objc public func StringDateformat(date: Date, format: String) -> String {
        /*
         Wednesday, Sep 12, 2018           --> EEEE, MMM d, yyyy
         09/12/2018                        --> MM/dd/yyyy
         09-12-2018 14:11                  --> MM-dd-yyyy HH:mm
         Sep 12, 2:11 PM                   --> MMM d, h:mm a
         September 2018                    --> MMMM yyyy
         Sep 12, 2018                      --> MMM d, yyyy
         Wed, 12 Sep 2018 14:11:54 +0000   --> E, d MMM yyyy HH:mm:ss Z
         2018-09-12T14:11:54+0000          --> yyyy-MM-dd'T'HH:mm:ssZ
         12.09.18                          --> dd.MM.yy
         10:41:02.112                      --> HH:mm:ss.SSS
         */
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let datestr = dateFormatter.string(from: date)
        return datestr
    }
    
    /**
     Get Random string as per len pass.
     - Parameters len: Pass length with int type.
     - Return: That will return with string with length of pass int value by user.
     
     ### Usage Example: ###
     ````
     UserUtility.randomString(len: 30)
     ````
     */
    @objc func randomString(len:Int) -> String {
        let charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let c = Array(charSet)
        var s:String = ""
        for _ in (0...len) {
            s.append(c[Int(arc4random()) % c.count])
        }
        return s
    }
    
    /**
     Get Userpushnotification Device token which register to pushnotification cetificate
     - Return: That will return devices token which is store in userdefault with key "DeviecToken"
     - Return: If user had not store the device token in userdefault and get nil devices token thatn it will automatically return random 30 charactor string
     
     - Notes: With store devicetoken into user default
     ### Success Usage Example: ###
     ````
     UserUtility.getDevicetoken()
     ````
     - Notes: With does not store devicetoken into user default
     ### Fail Usage Example: ###
     ````
     UserUtility.randomString(len: 30)
     ````
     */
    @objc func getDevicetoken() -> String {
        let device_token = ""
        if device_token.count == 0 {
            return device_token
        }
        else{
            return self.randomString(len: 30)
        }
    }
    
    /**
    Read File from document directory
    
    - Description: Call methods, that will automatically read give file with extension and return [String : Any] format
    
    ### Usage Example: ###
    ````
    UserUtility.ReadFile(filename: "", filetype: "")
    ````
    */
    @objc func ReadFile(filename name: String, filetype type: String) -> [String : Any] {
        if let path = Bundle.main.path(forResource: name, ofType: type) {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let responseObject = jsonResult as? [String : Any] {
                    return responseObject
                }
                else {
                    return [:]
                }
            } catch {
                return [:]
            }
        }
        return [:]
    }
    
    @objc func isObjectNotNil(object:AnyObject!) -> Bool
    {
        if let _:AnyObject = object
        {
            return true
        }
        return false
    }
    
    @objc func IsStringNilOrCount(value: Any?) -> Bool {
        guard let myString: String = value as? String, !myString.isEmpty else {
            print("String is nil or empty.")
            return true
        }
        if myString.count == 0 && myString.isEmpty {
            guard myString.isEmpty else {
                print("String is nil or empty.")
                return true
            }
            return false
        }
        guard myString.isBlank else {
            return false
        }
        return true
    }
    
    @objc func getRootViewcontroller() -> UIViewController {
        return UIApplication.shared.windows.first!.rootViewController!
    }
    
    @objc func removeSpecialCharsFromString(text: String) -> String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+")
        return text.filter {okayChars.contains($0) }
    }
    
    @objc func classNameAsString(_ obj: Any) -> String {
        //prints more readable results for dictionaries, arrays, Int, etc
        return String(describing: type(of: obj))
    }

    @objc func Getheader() -> [String : String] {
        let header : [String : String] = ["Content-Type":"application/json"]
        return header
    }
    
    @objc public func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat.removeWhiteSpace())
        return emailPredicate.evaluate(with: enteredEmail.removeWhiteSpace())
    }
    
    func ShareTools(sender: UIButton, shareSTR: String = "", shareURL: String = "", shareIMG: UIImage = UIImage.init(), shareFile: String = "", FileEXT: String = "", vc: UIViewController) {
        
        var items: [Any] = []
        if !shareSTR.IsStrEmpty() {
            items.append(shareSTR)
        }
        if !shareURL.IsStrEmpty() {
            let url = URL(string: shareURL)!
            items.append(url)
        }
        items.append(shareIMG)
        if shareFile.count != 0 && FileEXT.count != 0 {
            let file = Bundle.main.url(forResource: shareFile,
                                        withExtension: FileEXT)
            items.append(file as Any)
        }
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: items, applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = sender
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = .any
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToFacebook
            ,UIActivity.ActivityType.postToTwitter
            ,UIActivity.ActivityType.postToWeibo
            ,UIActivity.ActivityType.message
            ,UIActivity.ActivityType.mail
            ,UIActivity.ActivityType.print
            ,UIActivity.ActivityType.copyToPasteboard
            ,UIActivity.ActivityType.assignToContact
            ,UIActivity.ActivityType.saveToCameraRoll
            ,UIActivity.ActivityType.addToReadingList
            ,UIActivity.ActivityType.postToFlickr
            ,UIActivity.ActivityType.postToVimeo
            ,UIActivity.ActivityType.postToTencentWeibo
            ,UIActivity.ActivityType.airDrop
            ,UIActivity.ActivityType.openInIBooks
            ,UIActivity.ActivityType.markupAsPDF
        ]
        vc.present(activityViewController, animated: true, completion: nil)
    }
    
}

struct AppUtility {

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {

        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.orientationLock = orientation
        }
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {

        self.lockOrientation(orientation)

        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
    }

}
