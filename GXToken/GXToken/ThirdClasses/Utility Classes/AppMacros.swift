//
//  AppMacros.swift
//  GXToken
//
//  Created by Shorupan Pirakaspathy on 2020-03-27.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import Foundation

public let ViewBGColor                          = UIColor.init(named: "BGColor")!
public let SubBGColor                          = UIColor.init(named: "SubBGColor")!
public let DarkBlue                             = UIColor.init(named: "AppDarkColor")!
public let LightBlue                            = UIColor.init(named: "AppLightColor")!
public let ShadowBlue                           = UIColor.init(named: "ShadowBlue")!
public let ShadowDarkBlue                       = UIColor.init(named: "ShadowDarkBlue")!
public let DarkGreen                            = UIColor.init(named: "AppDarkGreen")!
public let DarkRed                              = UIColor.init(named: "AppDarkRed")!
public let TextWhite                            = UIColor.init(named: "TXTWhite")!
public let TextDark                             = UIColor.init(named: "TXTDark")!
public let LightWhite                           = UIColor.init(named: "LightWhite")!
public let TextColor                            = UIColor.init(named: "TextColor")!
public let LightGroup                           = UIColor.init(named: "LightGroup")!
public let LightGray                            = UIColor.init(named: "TXTGray")!
public let SepratorColor                        = UIColor.init(named: "Seprator")

public let REGEX_USER_NAME_LIMIT                = "^.{3,10}$"
public let REGEX_USER_NAME                      = "[A-Za-z0-9]{3,10}"
public let REGEX_TITLE_LIMIT                    = "^.{3,20}$"
public let REGEX_TITLE                          = "[A-Za-z0-9]{3,20}"
public let REGEX_DATE                           = "[0-9]{1,2}+[/]{1,1}+[0-9]{2,4}"
public let REGEX_TIME                           = "[0-9]{1,2}+[:]{1,1}+[0-9]{1,2}"
public let REGEX_LOCATION                       = "[A-Za-z0-9,-_ ]{1,50}"
public let REGEX_EMAIL                          = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
public let REGEX_PASSWORD_LIMIT                 = "^.{6,20}$"
public let REGEX_PASSWORD                       = "[A-Za-z0-9]{6,20}"
public let REGEX_PHONE_DEFAULT                  = "[0-9]{10,12}"

public let ShortVersion                         = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
public let DisplayName                          = Bundle.main.infoDictionary?["CFBundleName"] as! String
public let BuildName                            = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

// MARK: - -------------------- UIResponder --------------------
// MARK: -

public let iPhone12_ProMax      = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1170, height: 2532)) ?? false
public let iPhone12_Pro         = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1284, height: 2778)) ?? false
public let iPhone12             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1170, height: 2532)) ?? false
public let iPhone12_mini        = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1080, height: 2340)) ?? false
public let iPhone11             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 828, height: 1792)) ?? false
public let iPhone11_Pro         = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1125, height: 2436)) ?? false
public let iPhone11_ProMax      = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1242, height: 2688)) ?? false
public let iPhoneXR             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 828, height: 1792)) ?? false
public let iPhoneXS             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1125, height: 2436)) ?? false
public let iPhoneXS_Max         = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1242, height: 2688)) ?? false
public let iPhoneX              = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1125, height: 2436)) ?? false
public let iPhone8_Plus         = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1080, height: 1920)) ?? false
public let iPhone8              = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 750, height: 1334)) ?? false
public let iPhoneSE_2ndGen      = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 750, height: 1334)) ?? false
public let iPhone7_Plus         = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1080, height: 1920)) ?? false
public let iPhone7              = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 750, height: 1334)) ?? false
public let iPhoneSE             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 640, height: 1136)) ?? false
public let iPhone6s_Plus        = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1080, height: 1920)) ?? false
public let iPhone6s             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 750, height: 1334)) ?? false
public let iPhone6_Plus         = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1080, height: 1920)) ?? false
public let iPhone6              = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 750, height: 1334)) ?? false
public let iPhone5S             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 640, height: 1136)) ?? false
public let iPhone5C             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 640, height: 1136)) ?? false
public let iPhone5              = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 640, height: 1136)) ?? false
public let iPhone4S             = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 640, height: 960)) ?? false
public let iPhone4              = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 640, height: 960)) ?? false
public let iPhone3GS            = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 480, height: 320)) ?? false

public let Screen_width                         = UIScreen.main.bounds.size.width
public let Screen_height                        = UIScreen.main.bounds.size.height
public let Language                             = NSLocale.preferredLanguages.first
public let isIphoneXR                           = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 828, height: 1792)) ?? false
public let isIphoneXSMAX                        = UIScreen.main.currentMode?.size.equalTo(CGSize (width: 1242, height: 2688)) ?? false
public let isXseries                            = isIphoneX || isIphoneXR || isIphoneXSMAX || iPhone12_ProMax || iPhone12_Pro || iPhone12 || iPhone12_mini || iPhone11 || iPhone11_Pro || iPhone11_ProMax || iPhoneXR || iPhoneXS || iPhoneXS_Max || iPhoneX

public let TopNavHeight : CGFloat               = isXseries ? 84 : 64
public let TabbarHeight : CGFloat               = isXseries ? 83 : 49
public let StateBarHeight : CGFloat             = isXseries ? 44 : 20
public let NavBarHeight : CGFloat               = isXseries ? 64 : 44
public let BottomSafeAreaHeight : CGFloat       = isXseries ? 34 : 0
public let TopSafeAreaHeight : CGFloat          = isXseries ? 24 : 0
public let UnderSafeArea : CGFloat              = isXseries ? 24 : 20

public let isIphone5 : Bool                     = (UIScreen.main.bounds.height == 568) //se
public let isIphone6 : Bool                     = (UIScreen.main.bounds.height == 667) //6/6s/7/7s/8
public let isIphone6P : Bool                    = (UIScreen.main.bounds.height == 736) //6p/6sp/7p/7sp/8p
public let isIphoneX : Bool                     = Int((Screen_height / Screen_width) * 100) == 216 ? true : false

let App                                         = UIApplication.shared.delegate as? AppDelegate

// MARK: - -------------------- Storyboard --------------------
// MARK: -

func getStroyboard(_ StoryboardWithName: Any) -> UIStoryboard {
    return UIStoryboard(name: StoryboardWithName as! String, bundle: nil)
}
func loadViewController(_ StoryBoardName: Any, _ VCIdentifier: Any) -> UIViewController {
    return getStroyboard(StoryBoardName).instantiateViewController(withIdentifier: VCIdentifier as! String)
}

extension Notification.Name {
    static let FAQView = Notification.Name("FAQView")
}

//MARK:- Threading with Background and Main

func background(work: @escaping () -> ()) {
    DispatchQueue.global(qos: .userInitiated).async {
        work()
    }
}

func main(work: @escaping () -> ()) {
    DispatchQueue.main.async {
        work()
    }
}

class CommonSetters {
    lazy var email: String = {
        return UserInfoData.shared.GetUserEmail()!
    }()
    lazy var token: String = {
        return UserInfoData.shared.GetUserToken()!
    }()
    lazy var Username: String = {
        guard let rootdata = UserInfoData.shared.GetUserdata() else {
            return ""
        }
        guard let username: String = rootdata.username else {
            return ""
        }
        return username
    }()
    lazy var UserId: String = {
        guard let rootdata = UserInfoData.shared.GetUserdata() else {
            return ""
        }
        guard let userId: String = rootdata.userId else {
            return ""
        }
        return userId
    }()
    var isLogin: Bool {
        get {
            if email.IsStrEmpty() && token.IsStrEmpty() && Username.IsStrEmpty() && UserId.IsStrEmpty() {
                return true
            }
            else {
                return false
            }
        }
    }
}
