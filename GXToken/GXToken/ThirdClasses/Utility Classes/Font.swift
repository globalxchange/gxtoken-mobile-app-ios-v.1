//
//  Font.swift
//
//  Created by Hiren Joshi on 05/08/19.
//  Copyright © 2019 Hiren Joshi. All rights reserved.
//

import UIKit
import Foundation

/**
 Font category classes
 */
public class Font: UIFont {
    
    /**
     Will return the best font conforming to the descriptor which will fit in the provided bounds.
     */
    class func bestFittingFontSize(for text: String, in bounds: CGRect, fontDescriptor: UIFontDescriptor, additionalAttributes: [NSAttributedString.Key: Any]? = nil) -> CGFloat {
        let constrainingDimension = min(bounds.width, bounds.height)
        let properBounds = CGRect(origin: .zero, size: bounds.size)
        var attributes = additionalAttributes ?? [:]
        
        let infiniteBounds = CGSize(width: CGFloat.infinity, height: CGFloat.infinity)
        var bestFontSize: CGFloat = constrainingDimension
        
        for fontSize in stride(from: bestFontSize, through: 0, by: -1) {
            let newFont = UIFont(descriptor: fontDescriptor, size: fontSize)
            attributes[.font] = newFont
            
            let currentFrame = text.boundingRect(with: infiniteBounds, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: attributes, context: nil)
            
            if properBounds.contains(currentFrame) {
                bestFontSize = fontSize
                break
            }
        }
        return bestFontSize
    }
    
    class func bestFittingFont(for text: String, in bounds: CGRect, fontDescriptor: UIFontDescriptor, additionalAttributes: [NSAttributedString.Key: Any]? = nil) -> UIFont {
        let bestSize = bestFittingFontSize(for: text, in: bounds, fontDescriptor: fontDescriptor, additionalAttributes: additionalAttributes)
        return UIFont(descriptor: fontDescriptor, size: bestSize)
    }
    
    class func MontserratBlackFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Black", size: size)
        return font!
    }
    
    class func MontserratBlackItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-BlackItalic", size: size)
        return font!
    }
    
    class func MontserratBoldFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Bold", size: size)
        return font!
    }
    
    class func MontserratBoldItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-BoldItalic", size: size)
        return font!
    }
    
    class func MontserratExtraBoldFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ExtraBold", size: size)
        return font!
    }
    
    class func MontserratExtraBoldItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ExtraBoldItalic", size: size)
        return font!
    }
    
    class func MontserratExtraLightFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ExtraLight", size: size)
        return font!
    }
    
    class func MontserratExtraLightItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ExtraLightItalic", size: size)
        return font!
    }
    
    class func MontserratItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Italic", size: size)
        return font!
    }
    
    class func MontserratLightFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Light", size: size)
        return font!
    }
    
    class func MontserratLightItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-LightItalic", size: size)
        return font!
    }
    
    class func MontserratMediumFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Medium", size: size)
        return font!
    }
    
    class func MontserratMediumItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-MediumItalic", size: size)
        return font!
    }
    
    class func MontserratRegularFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Regular", size: size)
        return font!
    }
    
    class func MontserratSemiBoldFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-SemiBold", size: size)
        return font!
    }
    
    class func MontserratSemiBoldItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-SemiBoldItalic", size: size)
        return font!
    }
    
    class func MontserratThinFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-Thin", size: size)
        return font!
    }
    
    class func MontserratThinItalicFont(font size: CGFloat) -> UIFont {
        let font = UIFont(name: "Montserrat-ThinItalic", size: size)
        return font!
    }
    
}
