//
//  UserInfoData.swift
//  GXToken
//
//  Created by Shorupan Pirakaspathy on 2020-03-31.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

let LoginKey = "UserInfo"
let UserDataKey = "UserData"
let BetsRegister = "BetsRegister"
let GXTokenApp = "GXTokenApp"

@objc public class UserInfoData: NSObject {

    static let shared = UserInfoData()
    public var mydefault : UserDefaults!
    
    @objc class var swiftSharedInstance: UserInfoData {
        struct Singleton {
            static let instance = UserInfoData()
        }
        return Singleton.instance
    }
    
    // the sharedInstance class method can be reached from ObjC
    @objc class func sharedInstance() -> UserInfoData {
        return UserInfoData.swiftSharedInstance
    }
    
    private override init() {
        self.mydefault = UserDefaults.standard
        self.mydefault.synchronize()
    }
    
    func SaveOtherInfo(info: String, key: String) {
        self.mydefault.set(info, forKey: key)
    }
    
    func GetOtherInfo(key: String) -> String? {
        let info: String = self.mydefault.object(forKey: key) as? String ?? ""
        return info
    }
    
    func SaveUserEmail(email: String) {
        self.mydefault.set(email, forKey: "email")
    }
    
    func GetUserEmail() -> String? {
        guard var email: String = self.mydefault.object(forKey: "email") as? String else {
            return ""
        }
        email = self.mydefault.object(forKey: "email") as? String ?? ""
        return email
    }
    
    func GetUserToken() -> String? {
        guard let rootdata = UserInfoData.shared.GetUserInfodata() else {
            return ""
        }
        guard let token: String = rootdata.idToken else {
            return ""
        }
        return token
    }
    
    func SaveUserdata(info: UserDataRootClass) -> Bool {
        let personData = NSKeyedArchiver.archivedData(withRootObject: info)
        self.mydefault.set(personData, forKey: UserDataKey)
        self.mydefault.synchronize()
        if info.user.userId.isEmpty && info.user.userId.isBlank && info.user.userId.isZero {
            return false
        }
        return true
    }
    
    func GetUserdata() -> UserDataUser? {
        let personData = self.mydefault.object(forKey: UserDataKey) as! NSData?
        if personData == nil {
            return nil
        }
        else {
            let info = NSKeyedUnarchiver.unarchiveObject(with: personData! as Data) as! UserDataRootClass
            return info.user
        }
    }
    
    func SaveUserInfodata(info: UserinfoRootClass) -> Bool {
        let personData = NSKeyedArchiver.archivedData(withRootObject: info)
        self.mydefault.set(personData, forKey: LoginKey)
        self.mydefault.synchronize()
        if info.idToken.uppercased() == self.GetUserToken()?.uppercased() {
            return true
        }
        return false
    }
    
    func GetUserInfodata() -> UserinfoRootClass? {
        let personData = self.mydefault.object(forKey: LoginKey) as! NSData?
        if personData == nil {
            return nil
        }
        else {
            let info = NSKeyedUnarchiver.unarchiveObject(with: personData! as Data) as! UserinfoRootClass
//            let info = try NSKeyedUnarchiver.unarchivedObject(ofClass: UserinfoRootClass.self, from: personData! as Data)
            return info
        }
    }
    
    func SaveGXToken(data: AppListUserApp) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(data), forKey:GXTokenApp)
    }
    
    func GetGXToken() -> AppListUserApp? {
        if let data = UserDefaults.standard.value(forKey:GXTokenApp) as? Data {
            let apps = try? PropertyListDecoder().decode(AppListUserApp.self, from: data)
            return apps
        }
        return nil
    }
    
    func SaveObjectdata(data: AnyObject, forkey Key: String) {
        let objc = NSKeyedArchiver.archivedData(withRootObject: data)
        self.mydefault.set(objc, forKey: Key)
    }
    
    func GetObjectdata(key: String) -> AnyObject? {
        let objc = self.mydefault.object(forKey: key) as! NSData?
        if objc == nil {
            return nil
        }
        else {
            let info = NSKeyedUnarchiver.unarchiveObject(with: objc! as Data) as AnyObject
            return info
        }
    }
    
    @objc func RefreshToken(tokenStatus: ((_ status: Bool)->Void)?) {
        background {
            let user = self.GetUserInfodata()
            let paramDict = RefreshTokenParamDict.init(refreshToken: (user?.refreshToken)!, devicekey: (user?.deviceKey)!)
            
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_Refreshtoken.value(), Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        if (responseObject["status"] != nil) {
                            let refreshToken: String = (user?.refreshToken!)!
                            let deviceKey: String = (user?.deviceKey!)!
                            let message: String = "Refresh Token update success"
                            let newtoken: String = responseObject["idToken"] as! String
                            let newaccess: String = responseObject["accessToken"] as! String
                            
                            let newdict = [
                                "accessToken"   : newaccess,
                                "idToken"       : newtoken,
                                "refreshToken"  : refreshToken,
                                "device_key"    : deviceKey,
                                "status"        : true as Any,
                                "message"       : message
                            ]

                            self.mydefault.removeObject(forKey: LoginKey)
                            self.mydefault.synchronize()
                            let Newuserinfo = UserinfoRootClass.init(fromDictionary: newdict)
                            if UserInfoData.shared.SaveUserInfodata(info: Newuserinfo) {
                                if newtoken.uppercased() == self.GetUserToken()?.uppercased() {
                                    tokenStatus!(true)
                                }
                                else {
                                    tokenStatus!(false)
                                }
                            }
                            else {
                                tokenStatus!(false)
                            }
                        }
                        else {
                            tokenStatus!(false)
                        }
                    }
                    else {
                        tokenStatus!(false)
                    }
                }
            }) { (msg, code) in
                main {
                    tokenStatus!(false)
                }
            }
        }
    }
    
    @objc func UserLogout() {
        let domain = Bundle.main.bundleIdentifier!
        self.mydefault.removePersistentDomain(forName: domain)
        self.mydefault.synchronize()
        print(Array(self.mydefault.dictionaryRepresentation().keys).count)
        App?.SetHomeVC()
    }
    
}
