//
//  FileManagment.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/12/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import Foundation
import UIKit

class FileManagment: NSObject {
    
    static let shared = FileManagment()
    let appname: String = "GXToken"
    
    func CreateFolderInDocumentDirectory()
    {
        let fileManager = FileManager.default
        let PathWithFolderName = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(self.appname)
        
        print("Document Directory Folder Path :- ",PathWithFolderName)
        
        if !fileManager.fileExists(atPath: PathWithFolderName)
        {
            try! fileManager.createDirectory(atPath: PathWithFolderName, withIntermediateDirectories: true, attributes: nil)
        }
    }
    
    func getDirectoryPath() -> NSURL
    {
        // path is main document directory path
        
        let documentDirectoryPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString)
        let pathWithFolderName = documentDirectoryPath.appendingPathComponent(self.appname)
        let url = NSURL(string: pathWithFolderName) // convert path in url
        
        return url!
    }
    
    func saveImageDocumentDirectory(image: UIImage, imagename: String)
    {
        self.deleteDirectory(imagename: String.init(format: "%@.png", imagename))
        let fileManager = FileManager.default
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(self.appname)
        if !fileManager.fileExists(atPath: path) {
            try! fileManager.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
        let url = NSURL(string: path)
        let imagePath = url!.appendingPathComponent(imagename)
        let urlString: String = String.init(format: "%@.png", imagePath!.absoluteString)
//        let imageData = image.jpegData(compressionQuality: 1.0)
        let imageData = image.pngData()
        fileManager.createFile(atPath: urlString as String, contents: imageData, attributes: nil)
    }
    
    func getImageFromDocumentDirectory(imagename: String) -> UIImage? {
        let fileManager = FileManager.default
        let imagePath = (self.getDirectoryPath() as NSURL).appendingPathComponent("\(imagename).png")
        let urlString: String = imagePath!.absoluteString
        if fileManager.fileExists(atPath: urlString) {
            let image = UIImage(contentsOfFile: urlString)
            return image
        } else {
            // print("No Image")
            return nil
        }
    }
    
    func deleteDirectory(imagename: String) {
        let fileManager = FileManager.default
        let yourProjectImagesPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imagename)
        if fileManager.fileExists(atPath: yourProjectImagesPath) {
            try! fileManager.removeItem(atPath: yourProjectImagesPath)
        }
//        let yourProjectDirectoryPath = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(self.appname)
//        if fileManager.fileExists(atPath: yourProjectDirectoryPath) {
//            try! fileManager.removeItem(atPath: yourProjectDirectoryPath)
//        }
    }
}
