//
//  ParamStruct.swift
//  Lead
//
//  Created by Hiren Joshi on 22/01/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import Foundation

// MARK:- Login And Register Parameters
struct LoginParamDict: Encodable {
    var ids: String
    var password: String
    
    var description: [String : Any] {
        get {
            return ["email" : ids.removeWhiteSpace(), "password": password.removeWhiteSpace()]
        }
    }
}

struct UserdataParamDict: Encodable {
    var email: String
    
    var description: [String : Any] {
        get {
            return ["email" : email.removeWhiteSpace()]
        }
    }
}

struct AuthLoginParamDict: Encodable {
    var ids: String
    var password: String
    var totp_code: String
    
    var description: [String : Any] {
        get {
            return ["email" : ids.removeWhiteSpace(), "password": password.removeWhiteSpace(), "totp_code": self.totp_code]
        }
    }
}

// MARK:- Comman Body Parameters
struct CommanHeader: Encodable {
    var email: String
    var token: String
    
    var description: [String : String] {
        get {
            return ["email" : email.removeWhiteSpace().count == 0 ? CommonSetters().email : email, "token": token.count == 0 ? CommonSetters().token : token]
        }
    }
}

struct RegisterBetsParamDict: Encodable {
    var email: String
    
    var description: [String : Any] {
        get {
            return ["email" : email.removeWhiteSpace()]
        }
    }
}

struct GXUsernameDict: Encodable {
    var email: String
    var username: String
    var image: String
    
    var description: [String : Any] {
        get {
            return [
                "username": self.username,
                "email": self.email,
                "image_url": self.image]
        }
    }
}

struct GXUsernameCheckDict: Encodable {
    var email: String
    var username: String
    var check: Bool = true
    
    var description: [String : Any] {
        get {
            return [
                "username": self.username,
                "email": self.email,
                "username_check": self.check]
        }
    }
}

struct RefreshTokenParamDict: Encodable {
    var refreshToken: String
    var devicekey: String
    
    var description: [String : Any] {
        get {
            return ["refreshToken" : refreshToken, "device_key": devicekey]
        }
    }
}

struct UserBalanceParamDict: Encodable {
    var email: String
    var convert: String
    
    var description: [String : Any] {
        get {
            return ["convert": convert, "email" : email.removeWhiteSpace()]
        }
    }
}

struct OrderHistParamDict: Encodable {
    var basepair: String
    
    var description: [String : Any] {
        get {
            return ["basePair" : basepair]
        }
    }
}

struct LimitSell_BuyParamDict: Encodable {
    var price: String
    var amount: String
    var basePair: String
    
    var description: [String : Any] {
        get {
            return ["price": price,
                    "amount": amount,
                    "basePair": basePair]
        }
    }
}

struct MarketSell_BuyParamDict: Encodable {
    var amount: String
    var basePair: String
    
    var description: [String : Any] {
        get {
            return ["amount": amount,
                    "basePair": basePair]
        }
    }
}

struct ForexConvertParam: Encodable {
    var buy: String
    var from: String
    
    var description: [String : String] {
        get {
            return ["buy" : buy, "from": from]
        }
    }
}

struct WithDrawTo_FromParamDict: Encodable {
    var appcode: String
    var profileid: String
    var coin: String
    
    var description: [String: String] {
        get {
            return ["app_code": appcode,
                    "profile_id": profileid,
                    "coin": coin]
        }
    }
}

struct WithdrawParamDict: Encodable {
    var email: String
    var token: String
    var from: WithDrawTo_FromParamDict
    var to: WithDrawTo_FromParamDict
    var amount: String
    var identifier: String
    var transferfor: String
    var friendid: String
    var destinationid: String
    
    var description: [String : Any] {
        get {
            return ["email": email,
                    "token": token,
                    "from": from.description,
                    "to": to.description,
                    "to_amount": amount,
                    "identifier": identifier,
                    "transfer_for": transferfor,
                    "friend_id": friendid,
                    "destination_id": destinationid]
        }
    }
}

struct DepositParamDict: Encodable {
    var amount: String
    var from: WithDrawTo_FromParamDict
    var to: WithDrawTo_FromParamDict
    var transferfor: String
    
    var description: [String : Any] {
        get {
            return ["amount": amount,
                    "from": from.description,
                    "to": to.description,
                    "transfer_for": transferfor]
        }
    }
}

struct AppBalanceParamDict: Encodable {
    var appcode: String
    var profileid: String
    
    var description: [String : String] {
        get {
            return ["app_code": appcode,
            "profile_id": profileid]
        }
    }
    
}

struct ForgotPassConfirmParam: Encodable {
    var email: String
    var code: String
    var newpass: String
    
    var description: [String: Any] {
        get {
            return ["email": email,
                    "code": code,
                    "newPassword": newpass
            ]
        }
    }
    
}

struct FriendInviteParam: Encodable {
    var userEmail: String? = CommonSetters().email
    var appcode: String = "lx"
    var email: String?
    var mobile: String?
    var apptype: String = "ios"
    var custommessage: String
    
    var description: [String: Any] {
        get {
            return [
                "userEmail": userEmail as Any,
                "app_code": appcode,
                "email": email as Any,
                "mobile": mobile as Any,
                "app_type": apptype,
                "custom_message": custommessage
            ]
        }
    }
    
}
