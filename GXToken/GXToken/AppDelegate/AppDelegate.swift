//
//  AppDelegate.swift
//  GXToken
//
//  Created by Hiren Joshi on 09/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let window: UIWindow = {
        let w = UIWindow()
        w.backgroundColor = .white
        w.makeKeyAndVisible()
        return w
    }()
    var orientationLock = UIInterfaceOrientationMask.portrait
    var BundleID: String?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        self.BundleID = Bundle.main.bundleIdentifier
        
        #if Dev_DEBUG || Dev_RELEASE
        print("Dev")
        #else
        print("Prod")
        #endif
        //        FirebaseApp.configure()
        self.getOnlineLogo()
        self.GetinitialView()
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }
    
}
extension AppDelegate {
    
    func getOnlineLogo() {
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_AppLoadData.value()) { (responsObject) in
            let root = AppLaunchRootClass.init(fromDictionary: responsObject)
            FileManagment.shared.CreateFolderInDocumentDirectory()
            if root.status || root.count >= 1 {
                DispatchQueue.global().async {
                    // Fetch Image Data
                    if ((root.apps.first?.fullcolourlogo?.isEmpty) != nil) {
                        if let data = try? Data(contentsOf: URL.init(string: (root.apps.first?.fullcolourlogo)!)!) {
                            DispatchQueue.main.async {
                                FileManagment.shared.saveImageDocumentDirectory(image: UIImage(data: data)!, imagename: "fullcolourlogo")
                            }
                        }
                        else {
                            FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "fullcolourlogo")!, imagename: "fullcolourlogo")
                        }
                    }
                    else {
                        FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "fullcolourlogo")!, imagename: "fullcolourlogo")
                    }
                    
                    // Fetch Image Data
                    if ((root.apps.first?.fullwhitelogo?.isEmpty) != nil) {
                        if let data = try? Data(contentsOf: URL.init(string: (root.apps.first?.fullwhitelogo)!)!) {
                            DispatchQueue.main.async {
                                FileManagment.shared.saveImageDocumentDirectory(image: UIImage(data: data)!, imagename: "fullwhitelogo")
                            }
                        }
                        else {
                            FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "fullwhitelogo")!, imagename: "fullwhitelogo")
                        }
                    }
                    else {
                        FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "fullwhitelogo")!, imagename: "fullwhitelogo")
                    }
                    
                    // Fetch Image Data
                    if ((root.apps.first?.colouredicon?.isEmpty) != nil) {
                        if let data = try? Data(contentsOf: URL.init(string: (root.apps.first?.colouredicon)!)!) {
                            DispatchQueue.main.async {
                                FileManagment.shared.saveImageDocumentDirectory(image: UIImage(data: data)!, imagename: "colouredicon")
                            }
                        }
                        else {
                            FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "colouredicon")!, imagename: "colouredicon")
                        }
                    }
                    else {
                        FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "colouredicon")!, imagename: "colouredicon")
                    }
                    
                    // Fetch Image Data
                    if ((root.apps.first?.whiteicon?.isEmpty) != nil) {
                        if let data = try? Data(contentsOf: URL.init(string: (root.apps.first?.whiteicon)!)!) {
                            DispatchQueue.main.async {
                                FileManagment.shared.saveImageDocumentDirectory(image: UIImage(data: data)!, imagename: "whiteicon")
                            }
                        }
                        else {
                            FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "whiteicon")!, imagename: "whiteicon")
                        }
                    }
                    else {
                        FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "whiteicon")!, imagename: "whiteicon")
                    }
                }
            }
            else {
                FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "fullcolourlogo")!, imagename: "fullcolourlogo")
                FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "fullwhitelogo")!, imagename: "fullwhitelogo")
                FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "colouredicon")!, imagename: "colouredicon")
                FileManagment.shared.saveImageDocumentDirectory(image: UIImage.init(named: "whiteicon")!, imagename: "whiteicon")
            }
        } onError: { (message, code) in
            
        }
    }
    
    @objc public func GetinitialView() {
        //        if CommonSetters().token {
        self.SetHomeVC()
        //        }
        //        else {
        //            if CommonSetters().token && !CommonSetters().ArcadeUserid.isEmpty {
        //                self.SetHomeVC()
        //            }
        //            else {
        //                self.GXregisterVC()
        //            }
        //        }
    }
    
    @objc public func GXregisterVC() {
        let vc = BetsRegistrationVC.init(nibName: "BetsRegistrationVC", bundle: nil)
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: false)
        self.window.backgroundColor = UIColor.white
    }
    
    @objc public func SetHomeVC(selected: String = "") {
        let vc = MainHomeVC.init(nibName: "MainHomeVC", bundle: nil)
        vc.selected_Coin = selected
        let navigationController = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigationController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: false)
        self.window.backgroundColor = UIColor.white
    }
    
}

