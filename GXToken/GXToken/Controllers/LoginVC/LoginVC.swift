//
//  LoginVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 11/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import CRNotifications
import SVPinView
import Lottie

class LoginVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    
    @IBOutlet var BGLBL: [UILabel]!
    @IBOutlet weak var DismissBTN: UIButton!
    
    @IBOutlet weak var SubView: UIView!
    @IBOutlet var LogoIMG: [UIImageView]!
    
    @IBOutlet weak var UpdatePassLBL: UILabel!
    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var TXTEmail: UITextField!
    
    @IBOutlet weak var PasswordView: UIView!
    @IBOutlet weak var TXTPassword: UITextField!
    
    @IBOutlet weak var ContinueBTN: UIButton!
    @IBOutlet weak var ForgotBTN: UIButton!
    
    @IBOutlet weak var Bottomview: UIView!
    @IBOutlet weak var NewAccountBTN: UIButton!
    
    @IBOutlet weak var AuthenticationView: UIView!
    @IBOutlet weak var AuthTitle_LBL: UILabel!
    @IBOutlet weak var OTPView: UIView!
    @IBOutlet weak var Auth_AccessBTN: UIButton!
    
    @IBOutlet weak var AnimationView: UIView!
    @IBOutlet weak var Animations: UIView!
    
    // MARK:- Variable define
    var pinView: SVPinView!
    var email: String = ""
    var password: String = ""
    var OTP_pin: String = ""
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.setupUI()
    }
    
    // MARK:- User define methods
    
    func setupUI() {
        self.SubView.isHidden = false
        self.AuthenticationView.isHidden = true
        self.AnimationView.isHidden = true
        
        KeyboardAvoiding.avoidingView = self.SubView
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.SubView.backgroundColor = LightWhite
        self.SubView.layer.cornerRadius = 15
        
        self.LogoIMG[0].image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "colouredicon")
        self.LogoIMG[0].tintColor = DarkBlue
        
        self.BGLBL[0].backgroundColor = DarkBlue
        self.BGLBL[1].backgroundColor = DarkBlue
        
        self.UpdatePassLBL.textColor = DarkBlue
        self.UpdatePassLBL.font = Font.MontserratRegularFont(font: 15)
        
        self.TXTEmail.delegate = self
        self.TXTPassword.delegate = self
        
        self.EmailView.backgroundColor = .white
        self.EmailView.clipsToBounds = true
        self.EmailView.layer.cornerRadius = 10
        self.TXTEmail.placeholder = "Email"
        self.TXTEmail.font = Font.MontserratRegularFont(font: 15)
        self.TXTEmail.attributedPlaceholder =
        NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.TXTEmail.textColor = ShadowDarkBlue
        
        self.PasswordView.backgroundColor = .white
        self.PasswordView.clipsToBounds = true
        self.PasswordView.layer.cornerRadius = 10
        self.TXTPassword.placeholder = "Password"
        self.TXTPassword.font = Font.MontserratRegularFont(font: 15)
        self.TXTPassword.attributedPlaceholder =
        NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.TXTPassword.textColor = ShadowDarkBlue
        
        self.ContinueBTN.clipsToBounds = true
        self.ContinueBTN.layer.cornerRadius = 10
        self.ContinueBTN.setTitle("Continue", for: .normal)
        self.ContinueBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.ContinueBTN.setTitleColor(.white, for: .normal)
        self.ContinueBTN.backgroundColor = DarkBlue
        
        self.ContinueBTN.alpha = 0.5
        self.ContinueBTN.isUserInteractionEnabled = false
        
        self.ForgotBTN.setTitle("Forgot Password", for: .normal)
        self.ForgotBTN.titleLabel?.font = Font.MontserratRegularFont(font: 15)
        self.ForgotBTN.setTitleColor(DarkBlue, for: .normal)
        
        self.NewAccountBTN.setTitle("Don’t Have An Account? Click Here", for: .normal)
        self.NewAccountBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.NewAccountBTN.setTitleColor(.white, for: .normal)
        self.Bottomview.backgroundColor = DarkBlue
    }
    
    func setupAuthentication() {
        self.SubView.isHidden = true
        self.AuthenticationView.isHidden = false
        self.AnimationView.isHidden = true
        
        KeyboardAvoiding.avoidingView = self.AuthenticationView
        
        self.BGLBL[0].backgroundColor = ViewBGColor
        self.BGLBL[1].backgroundColor = ViewBGColor
        
        self.pinView = SVPinView.init(frame: CGRect(x: 0, y: 0, width: self.OTPView.frame.width, height: self.OTPView.frame.height))
        self.pinView.pinLength = 6
        self.pinView.interSpace = 5
        self.pinView.textColor = ShadowDarkBlue
        self.pinView.shouldSecureText = true
        self.pinView.style = .box
        self.pinView.borderLineColor = ShadowDarkBlue.withAlphaComponent(0.4)
        self.pinView.activeBorderLineColor = ShadowDarkBlue
        self.pinView.borderLineThickness = 1
        self.pinView.activeBorderLineThickness = 3
        self.pinView.font = UIFont.systemFont(ofSize: 15)
        self.pinView.keyboardType = .phonePad
        self.pinView.placeholder = "000000"
        self.pinView.becomeFirstResponderAtIndex = 0
        self.pinView.didFinishCallback = { pin in
            print("The pin entered is \(pin)")
            self.OTP_pin = pin
            self.Auth_AccessBTN.alpha = 1
            self.Auth_AccessBTN.isUserInteractionEnabled = true
        }
        self.pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done".localized(), style: UIBarButtonItem.Style.done, target: self, action: #selector(dissmisskeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        self.OTPView.addSubview(self.pinView)
        self.AuthTitle_LBL.text = "Enter The Two Factor Authentication Code"
        self.AuthTitle_LBL.font = Font.MontserratRegularFont(font: 12)
        
        self.Auth_AccessBTN.alpha = 0.5
        self.Auth_AccessBTN.isUserInteractionEnabled = false
        
        self.Auth_AccessBTN.setTitle("Continue", for: .normal)
        self.Auth_AccessBTN.titleLabel?.font = Font.MontserratRegularFont(font: 15)
        self.Auth_AccessBTN.backgroundColor = DarkBlue
        self.Auth_AccessBTN.setTitleColor(.white, for: .normal)
        self.Auth_AccessBTN.clipsToBounds = true
        self.Auth_AccessBTN.layer.cornerRadius = 10
    }
    
    func AnimationSetup() {
        main {
            self.SubView.isHidden = true
            self.AuthenticationView.isHidden = true
            self.AnimationView.isHidden = false
            
            self.BGLBL[0].backgroundColor = ViewBGColor
            self.BGLBL[1].backgroundColor = ViewBGColor
            
            let lottie = Lottie.AnimationView(name: "Loading")
            lottie.contentMode = .scaleAspectFit
            lottie.clipsToBounds = true
            lottie.loopMode = .loop
            self.AnimationView.addSubview(lottie)
            self.AnimationView.backgroundColor = ViewBGColor
            lottie.translatesAutoresizingMaskIntoConstraints = false
            lottie.centerXAnchor.constraint(equalTo: self.Animations.centerXAnchor, constant: 0).isActive = true
            lottie.centerYAnchor.constraint(equalTo: self.Animations.centerYAnchor, constant: 0).isActive = true
            lottie.leadingAnchor.constraint(equalTo: self.Animations.leadingAnchor, constant: 0).isActive = true
            lottie.trailingAnchor.constraint(equalTo: self.Animations.trailingAnchor, constant: 0).isActive = true
            lottie.heightAnchor.constraint(equalToConstant: self.Animations.frame.size.height).isActive = true
            lottie.widthAnchor.constraint(equalToConstant: self.Animations.frame.size.width).isActive = true
            lottie.play()
        }
    }
    
    // MARK:- API Calling
    
    func LoginApiCalling() {
        background {
            self.AnimationSetup()
            let paramDict = LoginParamDict.init(ids: self.email, password: self.password)
            
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_Login.value(), Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let userinfo = UserinfoRootClass.init(fromDictionary: responseObject)
                        if UserInfoData.shared.SaveUserInfodata(info: userinfo) {
                            UserInfoData.shared.SaveUserEmail(email: self.TXTEmail.text!.removeWhiteSpace())
                            self.Getuserdata()
                        }
                    }
                    else {
                        let userinfo = UserinfoRootClass.init(fromDictionary: responseObject)
                        if userinfo.mfa ?? false {
                            self.setupAuthentication()
                        }
                        else {
                            let message = responseObject["message"] as AnyObject
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message as! String, dismissDelay: 3, completion: {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                    }
                }
            }) { (msg, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: msg, dismissDelay: 3, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            }
        }
    }
    
    func Getuserdata() {
        background {
            let param = UserdataParamDict.init(email: self.email)
            NetworkingRequests.shared.requestsGET(CommanApiCall.API_GetUserData.value(), Parameters: param.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject) in
                main {
                    let userdata = UserDataRootClass.init(fromDictionary: responseObject)
                    if userdata.status {
                        if UserInfoData.shared.SaveUserdata(info: userdata) {
                            UserInfoData.shared.SaveUserEmail(email: self.TXTEmail.text!.removeWhiteSpace())
                            self.RegisterBets()
                        }
                    }
                    else {
                        let message = responseObject["message"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message as! String, dismissDelay: 3, completion: {
                            
                        })
                    }
                    App?.SetHomeVC()
                }
            }) { (msg, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: msg, dismissDelay: 3, completion: {
                        App?.SetHomeVC()
                    })
                }
            }
        }
    }
    
    func LoginwithAuth(pin: String) {
        background {
            self.AnimationSetup()
            let paramDict = AuthLoginParamDict.init(ids: self.TXTEmail.text!.removeWhiteSpace(), password: self.TXTPassword.text!.removeWhiteSpace(), totp_code: pin)
            
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_Login.value(), Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let userinfo = UserinfoRootClass.init(fromDictionary: responseObject)
                        if UserInfoData.shared.SaveUserInfodata(info: userinfo) {
                            self.Getuserdata()
                        }
                    }
                    else {
                        
                        let message = responseObject["message"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message as! String, dismissDelay: 3, completion: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                }
            }) { (msg, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: msg, dismissDelay: 3, completion: {
                        App?.SetHomeVC()
                    })
                }
            }
        }
    }
    
    func RegisterBets() {
        background {
            let header = CommanHeader.init(email: self.email, token: UserInfoData.shared.GetUserToken()!)
            let paramDict = RegisterBetsParamDict.init(email: self.email)
            
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_UserBIO.value(), Parameters: paramDict.description, Headers: header.description, onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let userinfo = UserBioRootClass.init(fromDictionary: responseObject as NSDictionary)
                        UserInfoData.shared.SaveObjectdata(data: userinfo.payload, forkey: BetsRegister)
                        if userinfo.payload.betsLevel {
                            App?.SetHomeVC()
                        }
                        else {
                            let vc = BetsRegistrationVC.init(nibName: "BetsRegistrationVC", bundle: nil)
                            self.navigationController?.pushViewController(vc, animated: false)
                        }
                    }
                    else {
                        let vc = BetsRegistrationVC.init(nibName: "BetsRegistrationVC", bundle: nil)
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
            }) { (msg, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: msg, dismissDelay: 3, completion: {
                        App?.SetHomeVC()
                    })
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    
    @objc func dissmisskeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedContinue(_ sender: UIButton) {
        if self.TXTEmail.isValidEmail(testStr: self.TXTEmail.text) || self.TXTPassword.text?.count != 0 {
            self.email = self.TXTEmail.text!.removeWhiteSpace()
            self.password = self.TXTPassword.text!
            self.LoginApiCalling()
        }
        else {
            
        }
    }
    
    @IBAction func TappedForgotPass(_ sender: Any) {
        let vc = ForgotPasswordVC.init(nibName: "ForgotPasswordVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func TappedNewAccount(_ sender: Any) {
        
    }
    
    @IBAction func TappedContinueBTN(_ sender: UIButton) {
        if !self.OTP_pin.IsStrEmpty() {
            self.AuthenticationView.isHidden = true
            self.LoginwithAuth(pin: self.OTP_pin)
        }
    }
    
}

extension LoginVC: ForgotPasswordDelegate {
    func backstatus(status: Bool) {
        if status {
            self.UpdatePassLBL.text = "Your Password Has Been Udpated"
        }
    }
}

//MARK:- Textfield Delegates
extension LoginVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.TXTEmail == textField {
            self.TXTEmail.resignFirstResponder()
            self.TXTPassword.becomeFirstResponder()
        }
        else {
            self.view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if AppUtillity.shared.validateEmail(enteredEmail: self.TXTEmail.text!) && self.TXTPassword.text?.count != 0 {
            self.ContinueBTN.alpha = 1
            self.ContinueBTN.isUserInteractionEnabled = true
        }
        else {
            self.ContinueBTN.alpha = 0.5
            self.ContinueBTN.isUserInteractionEnabled = false
        }
        
        return true
    }
    
}
