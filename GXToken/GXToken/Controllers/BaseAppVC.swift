//
//  BaseAppVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 09/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class BaseAppVC: UIViewController, UIGestureRecognizerDelegate {

    lazy var nodatafound: NodatafoundView = {
        () -> NodatafoundView in
        let nodata = NodatafoundView.init()
        nodata.SetupView(title: "No data available!", Notes: "There are no Data available yet!", image: "", enable: true)
        return nodata
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.backBarButtonItem?.tintColor = .white
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = ViewBGColor
        for item in self.view.subviews {
            if item.isKind(of: UIView.self) {
                item.backgroundColor = ViewBGColor
            }
            if item.isKind(of: UILabel.self) {
                (item as? UILabel)?.textColor = TextColor
                (item as? UILabel)?.font = Font.MontserratRegularFont(font: 17)
            }
            if item.isKind(of: UIButton.self) {
                (item as? UIButton)?.setTitleColor(TextColor, for: .normal)
                (item as? UIButton)?.titleLabel?.font = Font.MontserratRegularFont(font: 15)
            }
            if item.isKind(of: UITextField.self) {
                (item as? UITextField)?.font = Font.MontserratRegularFont(font: 15)
                (item as? UITextField)?.textColor = TextColor
                (item as? UITextField)?.attributedPlaceholder =
                    NSAttributedString(string: ((item as? UITextField)?.placeholder)!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.darkGray])
            }
            if item.isKind(of: UIScrollView.self) {
                (item as? UIScrollView)?.backgroundColor = ViewBGColor
            }
            if item.isKind(of: UIImageView.self) {
                (item as? UIImageView)?.backgroundColor = .clear
                (item as? UIImageView)?.tintColor = .clear
            }
        }
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dissmissAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.hidesBottomBarWhenPushed = true
    }
    
    @objc func dissmissAction () {
        self.view.endEditing(true)
    }

}

