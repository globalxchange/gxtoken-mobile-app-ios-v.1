//
//  TradeStep3VC.swift
//  GXToken
//
//  Created by Hiren Joshi on 06/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class TradeStep3VC: BaseAppVC {

    // MARK:- IBOutlets Define
    
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    
    @IBOutlet weak var SegmentView: UIView!
    @IBOutlet weak var CryptoBTN: UIButton!
    @IBOutlet weak var SepratorLBL: UILabel!
    @IBOutlet weak var ForexBTN: UIButton!
    
    @IBOutlet weak var CoinTBL: UITableView!
    
    // MARK:- Variable Define
    
    var BalanceData : UserBalanceConvertedCrypto!
    var FromCome: String!
    var AppsArry: [AppListUserApp]!
    var Userdata : SectionAppBalanceRootClass!
    var SelecctedApps: AppListUserApp!
    var Selecteddata3: AppBalanceCoinsData!
    var SelectedSection: Int = 0
    
    // MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
        self.Nautch.backgroundColor = DarkBlue
        
        self.SetupUI()
        self.nodatafound.frame = self.CoinTBL.frame
        self.nodatafound.didActionBlock = {
            self.Getuserbalance()
        }
        self.Getuserbalance()
    }

    // MARK:- User Define Methods
    
    func SetupUI() {
        
        if self.FromCome.uppercased() == "Add".uppercased() {
            self.ViewTitleLBL.text = "Select The App You Are Withdrawing From ".appending(self.SelecctedApps.appName)
        }
        else {
            self.ViewTitleLBL.text = "Select The App You Are Withdrawing To ".appending(self.SelecctedApps.appName)
        }
        self.ViewTitleLBL.textAlignment = .center
        self.ViewTitleLBL.font = Font.MontserratSemiBoldFont(font: 20)
        self.ViewTitleLBL.textColor = DarkBlue
        
        self.SegmentView.backgroundColor = ViewBGColor
        self.SegmentView.clipsToBounds = true
        self.SegmentView.layer.cornerRadius = 10
        self.SegmentView.layer.borderWidth = 1
        self.SegmentView.layer.borderColor = UIColor.colorWithHexString(hexStr: "EBEBEB").cgColor
        
        self.CryptoBTN.setTitle("Crypto", for: .normal)
        self.CryptoBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.CryptoBTN.setTitleColor(UIColor.white, for: .normal)
        self.CryptoBTN.backgroundColor = DarkBlue
        self.CryptoBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        
        self.ForexBTN.titleLabel?.font = Font.MontserratLightFont(font: 15)
        self.ForexBTN.setTitle("Forex", for: .normal)
        self.ForexBTN.titleLabel?.font = Font.MontserratLightFont(font: 15)
        self.ForexBTN.setTitleColor(DarkBlue, for: .normal)
        
        self.SepratorLBL.backgroundColor = UIColor.colorWithHexString(hexStr: "EBEBEB")
        
        self.CoinTBL.register(UINib.init(nibName: "GXTokenCell", bundle: nil), forCellReuseIdentifier: "GXTokenCell")
        self.CoinTBL.isHidden = false
        self.CoinTBL.backgroundColor = .clear
        
        self.view.addSubview(self.nodatafound)
    }
    
    // MARK:- API Calling
    
    func Getuserbalance() {
        let animation = LottiePopupView.init(frame: CGRect(x: 0, y: self.MainView.frame.origin.y, width: Screen_width, height: self.MainView.frame.height))
        self.MainView.addSubview(animation)
        let param = AppBalanceParamDict.init(appcode: self.SelecctedApps.appCode, profileid: self.SelecctedApps.profileId)
        let header = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
        NetworkingRequests.shared.requestPOST(CommanApiCall.API_AppCoinBalance.value(), Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
            let Data = AppBalanceRootClass.init(fromDictionary: responseObject as NSDictionary)
            if Data.status {
                self.Userdata = SectionAppBalanceRootClass(fromDictionary: [:])
                self.Userdata.Cryptoarray = [AppBalanceCoinsData]()
                self.Userdata.Fiatarray = [AppBalanceCoinsData]()
                
                let crypto = Data.coinsData.filter({ (coins) -> Bool in
                    coins.type.uppercased() == "crypto".uppercased()
                })
                self.Userdata.Cryptoarray.append(contentsOf: crypto)
                
                let fiat = Data.coinsData.filter({ (coins) -> Bool in
                    coins.type.uppercased() == "fiat".uppercased()
                })
                self.Userdata.Fiatarray.append(contentsOf: fiat)
                
                self.nodatafound.isHidden = true
                self.CoinTBL.delegate = self
                self.CoinTBL.dataSource = self
                self.CoinTBL.reloadData()
            }
            else {
                self.nodatafound.isHidden = false
            }
            animation.removeFromSuperview()
        }) { (Message, Code) in
            self.nodatafound.isHidden = false
            animation.removeFromSuperview()
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedCrypto(_ sender: Any) {
        self.SelectedSection = 0
        self.CryptoBTN.setTitleColor(UIColor.white, for: .normal)
        self.CryptoBTN.backgroundColor = DarkBlue
        self.CryptoBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.ForexBTN.titleLabel?.font = Font.MontserratLightFont(font: 15)
        self.ForexBTN.setTitleColor(DarkBlue, for: .normal)
        self.ForexBTN.backgroundColor = UIColor.white
        self.CoinTBL.reloadData()
    }
    
    @IBAction func TappedForex(_ sender: Any) {
        self.SelectedSection = 1
        self.ForexBTN.setTitleColor(UIColor.white, for: .normal)
        self.ForexBTN.backgroundColor = DarkBlue
        self.ForexBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.CryptoBTN.titleLabel?.font = Font.MontserratLightFont(font: 15)
        self.CryptoBTN.setTitleColor(DarkBlue, for: .normal)
        self.CryptoBTN.backgroundColor = UIColor.white
        self.CoinTBL.reloadData()
    }
    
}

extension TradeStep3VC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SelectedSection == 0 ? self.Userdata.Cryptoarray.count : self.Userdata.Fiatarray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GXTokenCell = tableView.dequeueReusableCell(withIdentifier: "GXTokenCell") as! GXTokenCell
        cell.TilesTheme()
        if self.SelectedSection == 0 {
            let data = self.Userdata.Cryptoarray[indexPath.row]
            cell.CoinImage.downloadedFrom(link: data.coinImage, contentMode: .scaleAspectFit, radious: cell.CoinImage.frame.height / 2)
            cell.price.text = String.init(format: "%f", data.coinValue).CoinPriceThumbRules(Coin: data.symbol)
            cell.title.text = data.coinName
            cell.isHidden = false
        }
        else {
            let data = self.Userdata.Fiatarray[indexPath.row]
            cell.CoinImage.downloadedFrom(link: data.coinImage, contentMode: .scaleAspectFit, radious: cell.CoinImage.frame.height / 2)
            cell.price.text = String.init(format: "%f", data.coinValue).CoinPriceThumbRules(Coin: data.symbol)
            cell.title.text = data.coinName
            cell.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TradeStep4VC.init(nibName: "TradeStep4VC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.BalanceData = self.BalanceData
        vc.FromCome = self.FromCome
        vc.SelecctedApps = self.SelecctedApps
        vc.Selecteddata3 = self.SelectedSection == 0 ? self.Userdata.Cryptoarray[indexPath.row] : self.Userdata.Fiatarray[indexPath.row]
        present(vc, animated: true, completion: nil)
    }
    
}
