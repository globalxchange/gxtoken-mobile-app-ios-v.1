//
//  FD_Step3VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class AddTradeQRVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    
    // TODO:- Step 3 QRcode Methods
    @IBOutlet weak var QRView: UIView!
    @IBOutlet weak var QRimage: UIImageView!
    @IBOutlet weak var CodeView: UIView!
    @IBOutlet weak var Codesview: UIView!
    @IBOutlet weak var QRCodelbl: UILabel!
    @IBOutlet weak var QRCopyBTN: UIButton!
    @IBOutlet var AllBottomview: UIView!
    @IBOutlet weak var ProcessBTN: UIButton!
    
    var FromCome: String!
    var FinalCoinAddress: String = ""
    var BalanceData : UserBalanceConvertedCrypto!
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = DarkBlue
        }
        self.Nautch.backgroundColor = DarkBlue
        self.AllBottomview.backgroundColor = DarkBlue
        
        self.cornerColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.SetupStep_3()
        
        self.QRCopyBTN.isUserInteractionEnabled = false
        self.QRCopyBTN.alpha = 0.5
        
        self.ProcessBTN.isUserInteractionEnabled = false
        self.ProcessBTN.alpha = 0.5
        
        self.GetCoinAddressCode(Coin: "") { (status, address) in
            if status {
                self.FinalCoinAddress = address
            }
            else {
                self.FinalCoinAddress = ""
            }
            self.GenerateQRcode(QRValue: self.FinalCoinAddress)
            self.QRCodelbl.text = self.FinalCoinAddress
            
            self.QRCopyBTN.isUserInteractionEnabled = true
            self.QRCopyBTN.alpha = 1
            
            self.ProcessBTN.isUserInteractionEnabled = true
            self.ProcessBTN.alpha = 1
        }
        
    }

    // MARK:- API Calling
    func GetCoinAddressCode(Coin: String, CodeSuccess: ((_ status: Bool, _ Address: String)->Void)?) {
        background {
            
        }
    }
    
    // MARK:- User Define Methods
    func cornerColor(color: UIColor = DarkBlue) {
        for lbl in self.bottomlbl {
            lbl.backgroundColor = color
        }
    }

    func SetupStep_3() {
        
        var title = "Here Is Your COIN Address"
        self.ViewTitleLBL.text = title.replaceLocalized(fromvalue: ["COIN"], tovalue: [""])
        
        self.QRCodelbl.textColor = DarkBlue
        
        self.CodeView.clipsToBounds = true
        self.CodeView.layer.borderColor = DarkBlue.withAlphaComponent(0.2).cgColor
        self.CodeView.layer.borderWidth = 1.0
        self.CodeView.layer.cornerRadius = 3
        
        self.QRCopyBTN.clipsToBounds = true
        self.QRCopyBTN.layer.borderColor = DarkBlue.withAlphaComponent(0.2).cgColor
        self.QRCopyBTN.layer.borderWidth = 1.0
        self.QRCopyBTN.layer.cornerRadius = 3
        
        self.QRView.backgroundColor = .white
        
        self.ProcessBTN.setTitle("Share", for: .normal)
        self.ProcessBTN.tintColor = .clear
        self.ProcessBTN.backgroundColor = .clear
        self.ProcessBTN.setTitleColor(.white, for: .normal)
        self.ProcessBTN.setTitleColor(.white, for: .selected)
    }
    
    func GenerateQRcode(QRValue: String) {
        self.FinalCoinAddress = QRValue
        self.QRCodelbl.text = QRValue
        // Get data from the string
        let data = QRValue.data(using: String.Encoding.ascii)
        // Get a QR CIFilter
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        // Input the data
        qrFilter.setValue(data, forKey: "inputMessage")
        // Get the output image
        guard let qrImage = qrFilter.outputImage else { return }
        // Scale the image
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        // Do some processing to get the UIImage
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return }
        self.QRimage.image = UIImage(cgImage: cgImage)
    }
    
    func ShareQRCode(QRValue: String) {
        //        let secondActivityItem : NSURL = NSURL(string: "http//:urlyouwant")!
        // If you want to put an image
        let image : UIImage = self.QRimage.image!
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [QRValue, image], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = self.ProcessBTN
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = .any
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToFacebook
            ,UIActivity.ActivityType.postToTwitter
            ,UIActivity.ActivityType.postToWeibo
            ,UIActivity.ActivityType.message
            ,UIActivity.ActivityType.mail
            ,UIActivity.ActivityType.print
            ,UIActivity.ActivityType.copyToPasteboard
            ,UIActivity.ActivityType.assignToContact
            ,UIActivity.ActivityType.saveToCameraRoll
            ,UIActivity.ActivityType.addToReadingList
            ,UIActivity.ActivityType.postToFlickr
            ,UIActivity.ActivityType.postToVimeo
            ,UIActivity.ActivityType.postToTencentWeibo
            ,UIActivity.ActivityType.airDrop
            ,UIActivity.ActivityType.openInIBooks
            ,UIActivity.ActivityType.markupAsPDF
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedBTN(_ sender: UIButton) {
        if sender == self.QRCopyBTN {
            if self.FinalCoinAddress.count > 0 {
                let pasteboard = UIPasteboard.general
                pasteboard.string = self.FinalCoinAddress
                if let string = pasteboard.string {
                    print(string)
                    self.QRCodelbl.text = "Copied To Clipboard"
                    self.DismissBTN.tag = 0
                    self.ProcessBTN.setTitleColor(.white, for: .selected)
                    self.ProcessBTN.backgroundColor = .clear
                    self.ProcessBTN.setTitle("Close", for: .normal)
                    self.ProcessBTN.isSelected = true
                }
            }
        }
        else {
            if self.ProcessBTN.isSelected {
                App?.SetHomeVC()
            }
            else {
                AppUtillity.shared.ShareTools(sender: self.ProcessBTN, shareSTR: self.FinalCoinAddress, shareIMG: self.QRimage.image!, vc: self)
//                self.ShareQRCode(QRValue: self.FinalCoinAddress)
            }
        }
    }
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
