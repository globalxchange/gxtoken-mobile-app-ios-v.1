//
//  TradeStep2VC.swift
//  GXToken
//
//  Created by Hiren Joshi on 06/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class TradeStep2VC: BaseAppVC {

    // MARK:- IBOutlets Define
    
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    @IBOutlet weak var FundTBL: UITableView!
    
    @IBOutlet weak var SP_SearchView: UIView!
    @IBOutlet weak var SP_AppBTN: UIButton!
    @IBOutlet weak var SP_Segment: UILabel!
    @IBOutlet weak var SP_Searchbar: UITextField!
    @IBOutlet weak var SP_Search_leading: NSLayoutConstraint!
    
    // MARK:- Variable Define
    
    var BalanceData : UserBalanceConvertedCrypto!
    var FromCome: String!
    var AppsArry: [AppListUserApp]!
    var FilterArry: [AppListUserApp]!
    
    
    // MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
        self.Nautch.backgroundColor = DarkBlue
        
        self.SetupUI()
    }

    // MARK:- User Define Methods
    
    func SetupUI() {
        
        self.GetAppListing()
        
        if self.FromCome.uppercased() == "Add".uppercased() {
            
        }
        else {
            
        }
        
        self.SP_SearchView.backgroundColor = ViewBGColor
        self.SP_SearchView.clipsToBounds = true
        self.SP_SearchView.layer.cornerRadius = 10
        self.SP_SearchView.layer.borderWidth = 1
        self.SP_SearchView.layer.borderColor = UIColor.colorWithHexString(hexStr: "EBEBEB").cgColor
        
        self.SP_Search_leading.constant = 10
        self.SP_Searchbar.placeholder = "Search Name of App"
        self.SP_Searchbar.delegate = self
        self.SP_Searchbar.font = Font.MontserratRegularFont(font: 15)
        self.SP_Searchbar.textColor = DarkBlue
        self.SP_Searchbar.attributedPlaceholder =
        NSAttributedString(string: self.SP_Searchbar.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        self.SP_AppBTN.isHidden = true
        self.SP_Segment.isHidden = true
        
        if self.FromCome.uppercased() == "Add".uppercased() {
            self.ViewTitleLBL.text = "Select The App You Are Withdrawing From"
        }
        else {
            self.ViewTitleLBL.text = "Select The App You Are Withdrawing To"
        }
        self.ViewTitleLBL.textAlignment = .center
        self.ViewTitleLBL.font = Font.MontserratSemiBoldFont(font: 20)
        self.ViewTitleLBL.textColor = DarkBlue
        
        self.FundTBL.register(UINib.init(nibName: "GXTokenCell", bundle: nil), forCellReuseIdentifier: "GXTokenCell")
        self.FundTBL.backgroundColor = .clear
    }
    
    // MARK:- API Calling
    
    func GetAppListing() {
        let animation = LottiePopupView.init(frame: CGRect(x: 0, y: self.MainView.frame.origin.y, width: Screen_width, height: self.MainView.frame.height))
        self.MainView.addSubview(animation)
        let param = UserdataParamDict.init(email: CommonSetters().email)
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_AppListing.value(), Parameters: param.description, Headers: [:], onSuccess: { (responseObject) in
            let applist = AppListRootClass.init(fromDictionary: responseObject as NSDictionary)
            if applist.status {
                self.AppsArry = [AppListUserApp]()
                self.AppsArry = applist.userApps
                self.FilterArry = [AppListUserApp]()
                self.FilterArry = applist.userApps
                let filter = self.AppsArry.filter { (list) -> Bool in
                    list.appCode.range(of: "lx", options: .caseInsensitive) != nil
                }
                UserInfoData.shared.SaveGXToken(data: filter.first!)
                self.FundTBL.delegate = self
                self.FundTBL.dataSource = self
                self.FundTBL.reloadData()
            }
            else {
                
            }
            animation.removeFromSuperview()
        }) { (Message, statuscode) in
            animation.removeFromSuperview()
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedAppBTN(_ sender: Any) {
    }
    
}

extension TradeStep2VC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        let filter = self.AppsArry.filter { (list) -> Bool in
            list.appName.range(of: newString as String, options: .caseInsensitive) != nil
        }
        self.FilterArry.removeAll()
        self.FilterArry = filter.count > 0 ? filter : self.AppsArry
        self.FundTBL.reloadData()
        return true
    }
    
}

extension TradeStep2VC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.FilterArry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GXTokenCell = tableView.dequeueReusableCell(withIdentifier: "GXTokenCell") as! GXTokenCell
        cell.TilesTheme()
        self.ConfigCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func ConfigCell(cell: GXTokenCell, indexPath: IndexPath) {
        let app = self.FilterArry[indexPath.row]
        
        cell.title.text = app.appName
        cell.price.text = ""
        if app.appName.IsStrEmpty() {
            cell.CoinImage.image = UIImage.init(named: "IC_AppIcon")
        }
        else {
            cell.CoinImage.downloadedFrom(link: app.appIcon, contentMode: .scaleAspectFit, radious: cell.CoinImage.frame.height / 2)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = TradeStep3VC.init(nibName: "TradeStep3VC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.BalanceData = self.BalanceData
        vc.FromCome = self.FromCome
        vc.SelecctedApps = self.FilterArry[indexPath.row]
        present(vc, animated: true, completion: nil)
    }
    
}
