//
//  FDOptionCell.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-05-27.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class FDOptionCell: UICollectionViewCell {

    @IBOutlet weak var cardview: UIView!
    @IBOutlet weak var CardIMG: UIImageView!
    @IBOutlet weak var NameLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.cardview.layer.cornerRadius = 10
        self.cardview.layer.borderColor = LightGroup.cgColor
        self.cardview.layer.borderWidth = 1
        self.cardview.layer.shadowColor = LightGroup.cgColor
        self.cardview.layer.shadowRadius = 1
        self.cardview.layer.shadowOpacity = 0.4
        self.cardview.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.cardview.clipsToBounds = true
        
        self.CardIMG.tintColor = DarkBlue
        self.NameLBL.textColor = DarkBlue
    }

}
