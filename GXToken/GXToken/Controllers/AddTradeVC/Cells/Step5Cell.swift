//
//  Step5Cell.swift
//  GXToken
//
//  Created by Hiren Joshi on 08/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class Step5Cell: UITableViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var subview: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.subview.backgroundColor = ViewBGColor
        
        self.titlelbl.font = Font.MontserratRegularFont(font: 15)
        self.titlelbl.textColor = DarkBlue
        
        self.name.font = Font.MontserratRegularFont(font: 15)
        self.name.textColor = DarkBlue
        
        self.price.font = Font.MontserratRegularFont(font: 15)
        self.price.textColor = DarkBlue
        
        self.subview.layer.cornerRadius = 10
        self.subview.layer.borderColor = LightGroup.cgColor
        self.subview.layer.borderWidth = 1
        self.subview.layer.shadowColor = LightGroup.cgColor
        self.subview.layer.shadowRadius = 1
        self.subview.layer.shadowOpacity = 0.4
        self.subview.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.subview.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
