//
//  FD_Step8VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit

class AddTradeCompleteVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    
    // TODO:- Step 8 Congrest View
    @IBOutlet weak var CongratulationView: UIView!
    @IBOutlet weak var CongretTitleLBL: UILabel!
    @IBOutlet weak var CongretCoinView: UIView!
    @IBOutlet weak var CongretCoinIMG: UIImageView!
    @IBOutlet weak var CongretCoinName: UILabel!
    @IBOutlet weak var CongretCoinSeprator: UILabel!
    @IBOutlet weak var CongretCoinDetail: UILabel!
    @IBOutlet weak var Collection: UICollectionView!
    
    // TODO:- Variable Define
    
    var BalanceData : UserBalanceConvertedCrypto!
    var FromCome: String!
    var AppsArry: [AppListUserApp]!
    var Userdata : UserBalanceData!
    var SelecctedApps: AppListUserApp!
    var Selecteddata3: AppBalanceCoinsData!
    var Topupamount1: Double = 0.00
    var Topupamount2: Double = 0.00
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = DarkBlue
        }
        self.Nautch.backgroundColor = DarkBlue
        self.SetupCongrestView_8()
        self.cornerColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK:- User Define Methods
    func cornerColor(color: UIColor = ViewBGColor) {
        for lbl in self.bottomlbl {
            lbl.backgroundColor = color
        }
    }

    func SetupCongrestView_8() {
        if self.FromCome.uppercased() == "ADD".uppercased() {
            self.CongretCoinIMG.downloadedFrom(link: self.Selecteddata3.coinImage, contentMode: .scaleAspectFit, radious: (self.CongretCoinIMG.frame.height / 2))
            self.ViewTitleLBL.text = "Deposit Complete"
            self.CongretCoinName.text = self.Selecteddata3.coinSymbol
            self.CongretCoinDetail.text = String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)
            
            self.CongretTitleLBL.text = String.init(format: "Congratulations Your Have Deposited ") + String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)! + " Into Your Vault"
        }
        else {
            self.ViewTitleLBL.text = "Withdrawal Complete"
            self.CongretCoinIMG.downloadedFrom(link: self.Selecteddata3.coinImage, contentMode: .scaleAspectFit, radious: (self.CongretCoinIMG.frame.height / 2))
            self.CongretCoinName.text = self.Selecteddata3.coinSymbol
            self.CongretCoinDetail.text = String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)
            
            self.CongretTitleLBL.text = String.init(format: "Congratulations Your Have Widthdraw  ") + String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)! + " From Your Vault"
        }
        
        self.Collection.register(UINib.init(nibName: "FDOptionCell", bundle: nil), forCellWithReuseIdentifier: "FDOptionCell")
        self.Collection.translatesAutoresizingMaskIntoConstraints = false
        self.Collection.delegate = self
        self.Collection.dataSource = self
    }
    
    // MARK:- IBAction Methods
    
}

extension AddTradeCompleteVC: UICollectionViewDelegate,UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FDOptionCell",for: indexPath) as? FDOptionCell else { fatalError() }
        if indexPath.row == 0 {
            cell.CardIMG.image = UIImage.init(named: "Cube")
            cell.NameLBL.text = "Place A Trade"
        }
        else {
            cell.CardIMG.image = UIImage.init(named: "BetsGraph")
            cell.NameLBL.text = "Back To Vault"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        App?.SetHomeVC()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: collectionView.frame.height)
    }
    
}
