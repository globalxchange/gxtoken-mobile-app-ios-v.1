//
//  FD_Step7VC.swift
//  CryptoLottery
//
//  Created by Shorupan Pirakaspathy on 2020-06-24.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import CRNotifications
import Lottie

class AddTrafeAnimationVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var DF_MainView: UIView!
    @IBOutlet var DF_bgLBL: [UILabel]!
    
    @IBOutlet weak var DF_MainView_Height: NSLayoutConstraint!
    @IBOutlet weak var DF_NautchLBL: UILabel!

    @IBOutlet weak var S7_Animationview: UIView!
    @IBOutlet weak var S7_Animation: UIView!
    
    // TODO:- Variable Define
    
    var BalanceData : UserBalanceConvertedCrypto!
    var FromCome: String!
    var AppsArry: [AppListUserApp]!
    var Userdata : UserBalanceData!
    var SelecctedApps: AppListUserApp!
    var Selecteddata3: AppBalanceCoinsData!
    var Topupamount1: Double = 0.00
    var Topupamount2: Double = 0.00
    
    // MARK:- For Invite Friends param
    var isMobile: Bool = false
    var email: String = ""
    var mobile: String = ""
    var Countryname: String = ""
    var countrycode: String = ""
    var Message: String = ""
    
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DF_MainView.backgroundColor = ViewBGColor
        self.DF_MainView.layer.cornerRadius = 20
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = ViewBGColor
        }
        self.DF_NautchLBL.backgroundColor = DarkBlue
        self.cornerColor(color: UIColor.white)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.FromCome.uppercased() == "Add".uppercased() {
            self.TopupAmount()
        }
        else if self.FromCome.uppercased() == "Send".uppercased() {
            self.WithdrawAmount()
        }
        else if self.FromCome.uppercased() == "Invite".uppercased() {
            self.Invitefriend()
        }
    }

    // MARK:- API Calling
    func TopupAmount() {
        self.SetupAnimation_7()
        background {
            let fromparam = WithDrawTo_FromParamDict.init(appcode: self.SelecctedApps.appCode, profileid: self.SelecctedApps.profileId, coin: self.Selecteddata3.coinSymbol)
            let toparam = WithDrawTo_FromParamDict.init(appcode: (UserInfoData.shared.GetGXToken()?.appCode)!, profileid: (UserInfoData.shared.GetGXToken()?.profileId)!, coin: self.BalanceData.symbol)
            let param = DepositParamDict.init(amount: String.init(format: "%f", self.Topupamount2).WithoutCoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)!, from: fromparam, to: toparam, transferfor: "transfer cost")
            let header = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_TopupVault.value(), Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        let vc = AddTradeCompleteVC.init(nibName: "AddTradeCompleteVC", bundle: nil)
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.BalanceData = self.BalanceData
                        vc.FromCome = self.FromCome
                        vc.SelecctedApps = self.SelecctedApps
                        vc.Selecteddata3 = self.Selecteddata3
                        vc.Topupamount1 = self.Topupamount1
                        vc.Topupamount2 = self.Topupamount2
                        self.present(vc, animated: true, completion: nil)
                    }
                    else {
                        let Message = responseObject["message"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            App?.SetHomeVC()
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        App?.SetHomeVC()
                    })
                }
            }
        }
    }
    
    func WithdrawAmount() {
        self.SetupAnimation_7()
        background {
            let toparam = WithDrawTo_FromParamDict.init(appcode: self.SelecctedApps.appCode, profileid: self.SelecctedApps.profileId, coin: self.Selecteddata3.coinSymbol)
            let fromparam = WithDrawTo_FromParamDict.init(appcode: (UserInfoData.shared.GetGXToken()?.appCode)!, profileid: (UserInfoData.shared.GetGXToken()?.profileId)!, coin: self.BalanceData.symbol)
            let param = WithdrawParamDict.init(email: CommonSetters().email, token: CommonSetters().token, from: fromparam, to: toparam, amount: String.init(format: "%f", self.Topupamount2).WithoutCoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)!, identifier: "", transferfor: "Transfer cost", friendid: "", destinationid: "")
            let header = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_WithdrawVault.value(), Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        let vc = AddTradeCompleteVC.init(nibName: "AddTradeCompleteVC", bundle: nil)
                        vc.modalPresentationStyle = .overCurrentContext
                        vc.BalanceData = self.BalanceData
                        vc.FromCome = self.FromCome
                        vc.SelecctedApps = self.SelecctedApps
                        vc.Selecteddata3 = self.Selecteddata3
                        vc.Topupamount1 = self.Topupamount1
                        vc.Topupamount2 = self.Topupamount2
                        self.present(vc, animated: true, completion: nil)
                    }
                    else {
                        let Message = responseObject["message"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            App?.SetHomeVC()
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        App?.SetHomeVC()
                    })
                }
            }
        }
    }
    
    func Invitefriend() {
        self.SetupAnimation_7()
        background {
            let param = FriendInviteParam.init(userEmail: CommonSetters().email, appcode: "lx", email: self.email, mobile: self.mobile, apptype: "ios", custommessage: self.Message)
            let header = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_InviteFriend.value(), Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    if status {
                        let sms = InviteMessageVC.init(nibName: "InviteMessageVC", bundle: nil)
                        sms.isCongratulation = true
                        let vc = UINavigationController(rootViewController: sms)
                        vc.setNavigationBarHidden(true, animated: true)
                        vc.modalPresentationStyle = .overCurrentContext
                        self.present(vc, animated: true, completion: nil)
                    }
                    else {
                        let Message = responseObject["message"] as AnyObject
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: Message as! String, dismissDelay: 3, completion: {
                            App?.SetHomeVC()
                        })
                    }
                }
            }) { (Message, Code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.error, title: "ERROR!", message: Message, dismissDelay: 3, completion: {
                        App?.SetHomeVC()
                    })
                }
            }
        }
    }
    
    // MARK:- User Define Methods
    func cornerColor(color: UIColor = UIColor.white) {
        for lbl in self.DF_bgLBL {
            lbl.backgroundColor = color
        }
    }

    func SetupAnimation_7() {
        let lottie = AnimationView(name: "Loading")
        lottie.contentMode = .scaleAspectFit
        lottie.clipsToBounds = true
        lottie.loopMode = .loop
        self.S7_Animation.addSubview(lottie)
        self.S7_Animation.backgroundColor = .clear
        lottie.translatesAutoresizingMaskIntoConstraints = false
        lottie.centerXAnchor.constraint(equalTo: self.S7_Animation.centerXAnchor, constant: 0).isActive = true
        lottie.centerYAnchor.constraint(equalTo: self.S7_Animation.centerYAnchor, constant: 0).isActive = true
        lottie.leadingAnchor.constraint(equalTo: self.S7_Animation.leadingAnchor, constant: 0).isActive = true
        lottie.trailingAnchor.constraint(equalTo: self.S7_Animation.trailingAnchor, constant: 0).isActive = true
        lottie.heightAnchor.constraint(equalToConstant: self.S7_Animation.frame.size.height).isActive = true
        lottie.widthAnchor.constraint(equalToConstant: self.S7_Animation.frame.size.width).isActive = true
        lottie.play()
    }
    
    // MARK:- IBAction Methods
    

}
