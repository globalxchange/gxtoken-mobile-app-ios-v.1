//
//  TradeStep5VC.swift
//  GXToken
//
//  Created by Hiren Joshi on 06/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class TradeStep5VC: BaseAppVC {
    
    // MARK:- IBOutlets Define
    
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    @IBOutlet weak var SelectedTBL: UITableView!
    @IBOutlet weak var TBL_Height: NSLayoutConstraint!
    @IBOutlet weak var EditBTN: UIButton!
    @IBOutlet weak var ConfirmBTN: UIButton!
    
    // MARK:- Variable Define
    var BalanceData : UserBalanceConvertedCrypto!
    var FromCome: String!
    var AppsArry: [AppListUserApp]!
    var Userdata : UserBalanceData!
    var SelecctedApps: AppListUserApp!
    var Selecteddata3: AppBalanceCoinsData!
    var Topupamount1: Double = 0.00
    var Topupamount2: Double = 0.00
    
    
    // MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
        self.Nautch.backgroundColor = DarkBlue
        self.SetupUI()
    }
    
    // MARK:- User Define Methods
    
    func SetupUI() {
        if self.FromCome.uppercased() == "Add".uppercased() {
            self.ViewTitleLBL.text = "Confirm Deposit Details"
        }
        else {
            self.ViewTitleLBL.text = "Confirm Withdrawal Details"
        }
        self.ViewTitleLBL.textAlignment = .center
        self.ViewTitleLBL.font = Font.MontserratSemiBoldFont(font: 20)
        self.ViewTitleLBL.textColor = DarkBlue
        
        self.SelectedTBL.register(UINib.init(nibName: "Step5Cell", bundle: nil), forCellReuseIdentifier: "Step5Cell")
        self.SelectedTBL.delegate = self
        self.SelectedTBL.dataSource = self
        self.SelectedTBL.reloadData()
        self.SelectedTBL.backgroundColor = .clear
        self.TBL_Height.constant = 80 * 3
        
        self.EditBTN.setTitleColor(DarkBlue, for: .normal)
        self.EditBTN.setTitle("Edit", for: .normal)
        self.EditBTN.backgroundColor = .white
        self.EditBTN.layer.shadowColor = UIColor.black.cgColor
        self.EditBTN.layer.shadowRadius = 1
        self.EditBTN.layer.shadowOpacity = 0.4
        self.EditBTN.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        self.ConfirmBTN.setTitleColor(.white, for: .normal)
        self.ConfirmBTN.setTitle("Confirm", for: .normal)
        self.ConfirmBTN.backgroundColor = DarkBlue
        self.ConfirmBTN.layer.shadowColor = UIColor.black.cgColor
        self.ConfirmBTN.layer.shadowRadius = 1
        self.ConfirmBTN.layer.shadowOpacity = 0.4
        self.ConfirmBTN.layer.shadowOffset = CGSize(width: 0, height: 0)
        
    }
    
    // MARK:- API Calling
    
    // MARK:- IBAction Methods
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedEdit(_ sender: Any) {
    }
    
    @IBAction func TappedConfirm(_ sender: Any) {
        let vc = AddTrafeAnimationVC.init(nibName: "AddTrafeAnimationVC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.BalanceData = self.BalanceData
        vc.FromCome = self.FromCome
        vc.SelecctedApps = self.SelecctedApps
        vc.Selecteddata3 = self.Selecteddata3
        vc.Topupamount1 = self.Topupamount1
        vc.Topupamount2 = self.Topupamount2
        present(vc, animated: true, completion: nil)
    }
    
}

extension TradeStep5VC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        view.backgroundColor = ViewBGColor
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: Step5Cell = tableView.dequeueReusableCell(withIdentifier: "Step5Cell") as! Step5Cell
        self.ConfigCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func ConfigCell(cell: Step5Cell, indexPath: IndexPath) {
        if indexPath.section == 0 {
            cell.titlelbl.text = "Your GX USDT Vault Will Be Debited"
            cell.icon.downloadedFrom(link: self.BalanceData.icon, contentMode: .scaleAspectFit, radious: cell.icon.frame.height / 2)
            cell.name.text = self.BalanceData.name
            cell.price.text = String.init(format: "%f", self.Topupamount1).CoinPriceThumbRules(Coin: self.BalanceData.symbol)
        }
        else {
            cell.titlelbl.text = "Your ICP BTC Vault Will Be Credited"
            cell.icon.downloadedFrom(link: self.Selecteddata3.coinImage, contentMode: .scaleAspectFit, radious: cell.icon.frame.height / 2)
            cell.name.text = self.Selecteddata3.coinName
            cell.price.text = String.init(format: "%f", self.Topupamount2).CoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
