//
//  TradeStep1VC.swift
//  GXToken
//
//  Created by Hiren Joshi on 05/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class TradeStep1VC: BaseAppVC {

    // MARK:- IBOutlets Define
    
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    @IBOutlet weak var FundTBL: UITableView!
    
    // MARK:- Variable Define
    
    var BalanceData : UserBalanceConvertedCrypto!
    var FromCome: String!
    
    // MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
        self.Nautch.backgroundColor = DarkBlue
        self.SetupUI()
    }

    // MARK:- User Define Methods
    
    func SetupUI() {
        
        if self.FromCome.uppercased() == "Add".uppercased() {
            
        }
        else {
            
        }
        
        self.SetBalance(detailtitle: "Balance \n", strprice: String.init(format: "%f ", self.BalanceData.value).WithoutCoinPriceThumbRules(Coin: self.BalanceData.symbol)!, strcoin: self.BalanceData.symbol, detailnote1: "\n▲ 4.59%", detailnote2: " (+212.64)")
        
        self.FundTBL.register(UINib.init(nibName: "GXTokenCell", bundle: nil), forCellReuseIdentifier: "GXTokenCell")
        self.FundTBL.delegate = self
        self.FundTBL.dataSource = self
        self.FundTBL.reloadData()
        self.FundTBL.backgroundColor = .clear
    }
    
    func SetBalance(detailtitle: String, strprice: String, strcoin: String, detailnote1: String, detailnote2: String) {
        let mainstring: String = detailtitle + strprice + strcoin + detailnote1 + detailnote2
        let myMutableString = mainstring.Attributestring(attribute: [
            (detailtitle, Font.MontserratRegularFont(font: 18), DarkBlue),
            (strprice, Font.MontserratSemiBoldFont(font: 40.0), DarkBlue),
            (strcoin, Font.MontserratSemiBoldFont(font: 25), DarkBlue),
            (detailnote1, Font.MontserratSemiBoldFont(font: 13), DarkGreen),
            (detailnote2, Font.MontserratRegularFont(font: 13), DarkBlue)
        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [detailtitle ,strprice ,strcoin ,detailnote1 ,detailnote2])))
        self.ViewTitleLBL.attributedText = myMutableString
    }
    
    // MARK:- API Calling
    
    // MARK:- IBAction Methods
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension TradeStep1VC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        view.backgroundColor = ViewBGColor
        
        let title = UILabel.init(frame: CGRect.init(x: 5, y: 15, width: tableView.frame.width, height: 20))
        title.font = Font.MontserratSemiBoldFont(font: 15)
        title.textColor = DarkBlue
        if self.FromCome.uppercased() == "Add".uppercased() {
            title.text = "How Do You Want To Add Funds?"
        }
        else {
            title.text = "How Do You Want To Send Funds?"
        }
        view.addSubview(title)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GXTokenCell = tableView.dequeueReusableCell(withIdentifier: "GXTokenCell") as! GXTokenCell
        self.ConfigCell(cell: cell, indexPath: indexPath)
        cell.TilesTheme()
        return cell
    }
    
    func ConfigCell(cell: GXTokenCell, indexPath: IndexPath) {    
        cell.CoinImage.image = indexPath.row == 0 ? UIImage.init(named: "IC_Vault") : UIImage.init(named: "IC_QRcode")
        cell.price.text = ""
        cell.title.text = indexPath.row == 0 ? "Valut Connect" : String.init(format: "Via %@ Address", self.BalanceData.symbol)
        cell.alpha = indexPath.row == 0 ? 1 : 0.3
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc = TradeStep2VC.init(nibName: "TradeStep2VC", bundle: nil)
            vc.modalPresentationStyle = .overCurrentContext
            vc.BalanceData = self.BalanceData
            vc.FromCome = self.FromCome
            present(vc, animated: true, completion: nil)
        }
        else {
//            let vc = AddTradeQRVC.init(nibName: "AddTradeQRVC", bundle: nil)
//            vc.modalPresentationStyle = .overCurrentContext
//            vc.BalanceData = self.BalanceData
//            vc.FromCome = self.FromCome
//            present(vc, animated: true, completion: nil)
        }
    }
    
}
