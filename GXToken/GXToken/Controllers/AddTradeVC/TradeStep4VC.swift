//
//  TradeStep4VC.swift
//  GXToken
//
//  Created by Hiren Joshi on 06/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class TradeStep4VC: BaseAppVC {

    // MARK:- IBOutlets Define
    
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    
    @IBOutlet weak var AmountView: UIView!
    @IBOutlet weak var Amount1LBL: UILabel!
    @IBOutlet weak var HalfBTN: UIButton!
    @IBOutlet weak var FullBTN: UIButton!
    @IBOutlet weak var Symbol1LBL: UILabel!
    @IBOutlet weak var SepratorLBL: UILabel!
    @IBOutlet weak var Amount2LBL: UILabel!
    @IBOutlet weak var Symbol2LBL: UILabel!
    @IBOutlet weak var DigitView: UIView!
    @IBOutlet var DigitsBTN: [UIButton]!
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var BottomBTN: UIButton!
    
    // MARK:- Variable Define
    
    var BalanceData : UserBalanceConvertedCrypto!
    var FromCome: String!
    var AppsArry: [AppListUserApp]!
    var Userdata : UserBalanceData!
    var SelecctedApps: AppListUserApp!
    var Selecteddata3: AppBalanceCoinsData!
    var Topupamount1: Double = 0.00
    var Topupamount2: Double = 0.00
    var PinArray: NSMutableArray!
    var isFirstAmount: Bool = true
    
    
    // MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
        self.Nautch.backgroundColor = DarkBlue
        if self.FromCome.uppercased() == "Add".uppercased() {
            
        }
        else {
            
        }
        self.SetupUI()
    }

    // MARK:- User Define Methods
    
    func SetupUI() {
        
        self.PinArray = NSMutableArray.init()
        
        self.ViewTitleLBL.text = "Enter Amount"
        self.ViewTitleLBL.textAlignment = .center
        self.ViewTitleLBL.font = Font.MontserratSemiBoldFont(font: 20)
        self.ViewTitleLBL.textColor = DarkBlue
        
        self.AmountView.backgroundColor = ViewBGColor
        
        self.Amount1LBL.text = "0.00"
        self.Amount1LBL.font = Font.MontserratRegularFont(font: 20)
        self.Amount1LBL.textColor = DarkBlue
        
        self.HalfBTN.setTitle("HALF", for: .normal)
        self.HalfBTN.layer.borderColor = ShadowDarkBlue.cgColor
        self.HalfBTN.layer.borderWidth = 0.5
        self.HalfBTN.setTitleColor(.lightGray, for: .normal)
        
        self.FullBTN.setTitle("FULL", for: .normal)
        self.FullBTN.layer.borderColor = ShadowDarkBlue.cgColor
        self.FullBTN.layer.borderWidth = 0.5
        self.FullBTN.setTitleColor(.lightGray, for: .normal)
        
        self.Symbol1LBL.text = self.BalanceData.symbol
        self.Symbol1LBL.font = Font.MontserratSemiBoldFont(font: 20)
        self.Symbol1LBL.textColor = DarkBlue
        
        self.SepratorLBL.backgroundColor = DarkBlue
        
        self.Amount2LBL.text = "0.00"
        self.Amount2LBL.font = Font.MontserratRegularFont(font: 20)
        self.Amount2LBL.textColor = .black
        
        self.Symbol2LBL.text = self.Selecteddata3.coinSymbol
        self.Symbol2LBL.font = Font.MontserratSemiBoldFont(font: 20)
        self.Symbol2LBL.textColor = .black
        
        self.DigitView.backgroundColor = DarkBlue
        for btn in self.DigitsBTN {
            btn.setTitleColor(.white, for: .normal)
            btn.titleLabel?.font = Font.MontserratRegularFont(font: 16)
        }
        
        self.BottomView.backgroundColor = DarkBlue
        self.BottomBTN.setTitleColor(.white, for: .normal)
        self.BottomBTN.titleLabel?.font = Font.MontserratRegularFont(font: 20)
        
    }
    
    func EnterPin(digit: String) {
        self.PinArray.add(digit)
        self.EnteredAmounts()
    }
    
    func EnteredAmounts() {
        for (index, item) in self.PinArray.enumerated() {
            if self.isFirstAmount {
                if index == 0 {
                    self.Amount1LBL.text = ""
                    self.Amount1LBL.text = String.init(format: "%@", item as! CVarArg)
                }
                else {
                    self.Amount1LBL.text = String.init(format: "%@%@", self.Amount1LBL.text!, item as! CVarArg)
                }
            }
            else {
                if index == 0 {
                    self.Amount2LBL.text = ""
                    self.Amount2LBL.text = String.init(format: "%@", item as! CVarArg)
                }
                else {
                    self.Amount2LBL.text = String.init(format: "%@%@", self.Amount2LBL.text!, item as! CVarArg)
                }
            }
        }
        if self.isFirstAmount {
            if !(self.Amount1LBL.text?.contains("."))! {
                if let n = NumberFormatter().number(from: self.Amount1LBL.text! as String) {
                    let f = Double(truncating: n)
                    self.Amount1LBL.text = String.init(format: "%f", f)
                }
            }
            if let n = NumberFormatter().number(from: self.Amount1LBL.text! as String) {
                let f = Double(truncating: n)
                self.Topupamount1 = f
            }
            
            self.ConvertForex(Buy: self.BalanceData.symbol, From: self.Selecteddata3.coinSymbol, Value: self.Amount1LBL.text!)
        }
        else {
            if !(self.Amount2LBL.text?.contains("."))! {
                if let n = NumberFormatter().number(from: self.Amount2LBL.text! as String) {
                    let f = Double(truncating: n)
                    self.Amount2LBL.text = String.init(format: "%f", f)
                }
            }
            if let n = NumberFormatter().number(from: self.Amount2LBL.text! as String) {
                let f = Double(truncating: n)
                self.Topupamount2 = f
            }
            self.ConvertForex(Buy: self.BalanceData.symbol, From: self.Selecteddata3.coinSymbol, Value: self.Amount2LBL.text!)
        }
    }
    
    @objc func Longbackpress() {
        self.PinArray.removeAllObjects()
        self.Amount1LBL.text = "0.00"
        if let n = NumberFormatter().number(from: self.Amount1LBL.text! as String) {
            let f = Double(truncating: n)
            self.Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.BalanceData.symbol)
        }
        
        self.Amount2LBL.text = "0.00"
        if let n = NumberFormatter().number(from: self.Amount2LBL.text! as String) {
            let f = Double(truncating: n)
            self.Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)
        }
    }
    
    func backspace() {
        self.PinArray.removeLastObject()
        if self.PinArray.count == 0 {
            self.Amount1LBL.text = "0.00"
            if let n = NumberFormatter().number(from: self.Amount1LBL.text! as String) {
                let f = Double(truncating: n)
                self.Amount1LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.BalanceData.symbol)
            }
            
            self.Amount2LBL.text = "0.00"
            if let n = NumberFormatter().number(from: self.Amount2LBL.text! as String) {
                let f = Double(truncating: n)
                self.Amount2LBL.text = String.init(format: "%f", f).WithoutCoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)
            }
        }
        else {
            self.EnteredAmounts()
        }
    }
    
    // MARK:- API Calling
    func ConvertForex(Buy: String, From: String, Value: String) {
        self.BottomBTN.isUserInteractionEnabled = false
        self.BottomBTN.alpha = 0.5
        CommanCalling().ForexConversion(Buy: Buy, From: From) { (keyvalue1, keyvalue2) in
            if self.isFirstAmount {
                if let n = NumberFormatter().number(from: Value) {
                    let cgvalue = Double(truncating: n)
                    let convert = cgvalue * Double(keyvalue2)
                    self.Amount1LBL.text! = String.init(format: "%f", cgvalue).WithoutCoinPriceThumbRules(Coin: self.BalanceData.symbol)!
                    self.Topupamount1 = cgvalue
                    self.Amount2LBL.text! = String.init(format: "%f", convert).WithoutCoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)!
                    self.Topupamount2 = convert
                }
            }
            else {
                if let n = NumberFormatter().number(from: Value) {
                    let cgvalue = Double(truncating: n)
                    let convert = cgvalue * Double(keyvalue1)
                    self.Amount2LBL.text! = String.init(format: "%f", cgvalue).WithoutCoinPriceThumbRules(Coin: self.BalanceData.symbol)!
                    self.Topupamount2 = cgvalue
                    self.Amount1LBL.text! = String.init(format: "%f", convert).WithoutCoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)!
                    self.Topupamount1 = convert
                }
            }
            self.BottomBTN.isUserInteractionEnabled = true
            self.BottomBTN.alpha = 1.0
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedHalfBTN(_ sender: Any) {
        self.Amount1LBL.text! = String.init(format: "%f", (self.BalanceData.value * 50) / 100).WithoutCoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)!
        self.ConvertForex(Buy: self.BalanceData.symbol, From: self.Selecteddata3.coinSymbol, Value: self.Amount1LBL.text!)
    }
    
    @IBAction func TappedFullBTN(_ sender: Any) {
        self.Amount1LBL.text! = String.init(format: "%f", self.BalanceData.value).WithoutCoinPriceThumbRules(Coin: self.Selecteddata3.coinSymbol)!
        self.ConvertForex(Buy: self.BalanceData.symbol, From: self.Selecteddata3.coinSymbol, Value: self.Amount1LBL.text!)
    }
    
    @IBAction func TappedActive1LBL(_ sender: Any) {
        self.isFirstAmount = true
        self.Amount1LBL.font = Font.MontserratBoldFont(font: 17)
        self.Amount2LBL.font = Font.MontserratLightFont(font: 17)
        self.Longbackpress()
    }
    
    @IBAction func TappedActive2LBL(_ sender: Any) {
        self.isFirstAmount = false
        self.Amount2LBL.font = Font.MontserratBoldFont(font: 17)
        self.Amount1LBL.font = Font.MontserratLightFont(font: 17)
        self.Longbackpress()
    }
    
    @IBAction func TappedDigitsBTN(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 2:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 3:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 4:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 5:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 6:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 7:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 8:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 9:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 100:
            self.backspace()
            break
            
        case 0:
            self.EnterPin(digit: String.init(format: "%d", sender.tag))
            break
            
        case 200:
            if !self.PinArray.contains(".") {
                if self.PinArray.count == 0 {
                    self.EnterPin(digit: String.init(format: "0"))
                    self.EnterPin(digit: String.init(format: "."))
                }
                else {
                    self.EnterPin(digit: String.init(format: "."))
                }
            }
            break
            
        case 300:
            // Divide
            break
            
            case 400:
                // Multiple
            break
            
            case 500:
                // Minues
            break
            
            case 600:
                // Add
            break
            
        default:
            break
        }
    }
    
    @IBAction func TappedBottom(_ sender: Any) {
        let vc = TradeStep5VC.init(nibName: "TradeStep5VC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.BalanceData = self.BalanceData
        vc.FromCome = self.FromCome
        vc.SelecctedApps = self.SelecctedApps
        vc.Selecteddata3 = self.Selecteddata3
        vc.Topupamount1 = self.Topupamount1
        vc.Topupamount2 = self.Topupamount2
        present(vc, animated: true, completion: nil)
    }
    
}
