//
//  SettingVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 22/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class SettingVC: BaseAppVC {

    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var Main_Height: NSLayoutConstraint!
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    @IBOutlet weak var DismissBTN: UIButton!
    
    @IBOutlet weak var refreshView: UIView!
    @IBOutlet weak var refreshlbl: UILabel!
    @IBOutlet weak var Updatelbl: UILabel!
    @IBOutlet var RefreshBTN: UIButton!
    
    @IBOutlet weak var testflightView: UIView!
    @IBOutlet weak var testflightlbl: UILabel!
    @IBOutlet weak var testflightupdatelbl: UILabel!
    @IBOutlet var testflightBTN: UIButton!
    
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet var CloseBTN: UIButton!
    
    
    var TestFlight_STR: String = ""
    
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        self.MainView.clipsToBounds = true
        for lbl in self.bottomlbl {
            lbl.backgroundColor = DarkBlue
        }
        self.ViewTitleLBL.backgroundColor = DarkBlue
        self.ViewTitleLBL.text = "Terminal Settings"
        self.ViewTitleLBL.textColor = .white
        self.ViewTitleLBL.font = Font.MontserratRegularFont(font: 20)
        self.DismissBTN.backgroundColor = .clear
        self.setupview()
    }

    // MARK:- User Define Methods
    func setupview() {
        self.GetAppListing()
        self.Main_Height.constant = Screen_height / 2
        self.refreshView.backgroundColor = .white
        
        self.refreshlbl.text = "Refresh"
        self.refreshlbl.textColor = DarkBlue
        self.refreshlbl.font = Font.MontserratRegularFont(font: 11)
        
        self.Updatelbl.text = Date().Lastupdate()
        self.Updatelbl.textColor = DarkBlue
        self.Updatelbl.font = Font.MontserratLightFont(font: 11)
        
        self.RefreshBTN.clipsToBounds = true
        self.RefreshBTN.layer.cornerRadius = 10
        self.RefreshBTN.setTitleColor(.white, for: .normal)
        self.RefreshBTN.setTitle("Refresh", for: .normal)
        self.RefreshBTN.titleLabel!.font = Font.MontserratRegularFont(font: 20)
        
        self.testflightlbl.text = "Latest App"
        self.testflightlbl.textColor = DarkBlue
        self.testflightlbl.font = Font.MontserratRegularFont(font: 11)
        
        self.testflightupdatelbl.text = Date().Lastupdate()
        self.testflightupdatelbl.textColor = DarkBlue
        self.testflightupdatelbl.font = Font.MontserratLightFont(font: 11)
        
        self.testflightBTN.clipsToBounds = true
        self.testflightBTN.backgroundColor = ViewBGColor
        self.testflightBTN.layer.cornerRadius = 10
        self.testflightBTN.layer.borderColor = DarkBlue.cgColor
        self.testflightBTN.layer.borderWidth = 1
        self.testflightBTN.setTitleColor(DarkBlue, for: .normal)
        self.testflightBTN.setTitle("", for: .normal)
        self.testflightBTN.titleLabel!.font = Font.MontserratRegularFont(font: 20)
        
        self.BottomView.backgroundColor = DarkBlue
        self.CloseBTN.setTitleColor(.white, for: .normal)
        self.CloseBTN.setTitle("Close Settings", for: .normal)
        self.CloseBTN.titleLabel!.font = Font.MontserratRegularFont(font: 20)
    }

    // MARK:- API Calling
    func GetAppListing() {
        let animation = LottiePopupView.init(frame: CGRect(x: 0, y: self.MainView.frame.origin.y, width: Screen_width, height: self.MainView.frame.height))
        self.MainView.addSubview(animation)
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Apple_Link.value(), Parameters: ["app_code":"lx"], Headers: [:], onSuccess: { (responseObject) in
            let root = TestFlightRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status {
                if root.count != 0 {
                    self.TestFlight_STR = (root.logs.first?.iosAppLink)!
                    let time = (root.logs.first?.timestamp)! as Double
                    self.testflightupdatelbl.text = String.init(format: "%@", (time.getDateStringFromUTC(format: "EEEE, MMM d, yyyy hh:mm aa")))
                }
            }
            animation.removeFromSuperview()
        }) { (Message, statuscode) in
            animation.removeFromSuperview()
        }
    }
    
    // MARK:- IBAction Methods
    @IBAction func TappedClosebtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedRefreshbtn(_ sender: Any) {
        App?.SetHomeVC()
    }
    
    @IBAction func TappedTestFlight(_ sender: Any) {
        let Url = NSURL(string: self.TestFlight_STR)
        if UIApplication.shared.canOpenURL(Url! as URL)
        {
            UIApplication.shared.open(Url! as URL, options: [:], completionHandler: nil)
            
        } else {
            //redirect to safari because the user doesn't have Instagram
            UIApplication.shared.open(NSURL(string: "https://testflight.apple.com/")! as URL, options: [:], completionHandler: nil)
        }
    }
    
}
