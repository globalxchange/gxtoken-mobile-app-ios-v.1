//
//  InviteMessageVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 10/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class InviteMessageVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    
    @IBOutlet var bottomlbl: [UILabel]!
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet var SepratorLBL: [UILabel]!
    @IBOutlet weak var Logo: UIImageView!
    
    @IBOutlet weak var TitleIMG: UIImageView!
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var TXTMessage: UITextField!
    
    @IBOutlet weak var ProcessBTN: UIButton!
    @IBOutlet weak var EditBTN: UIButton!
    
    var isCongratulation: Bool = false
    
    var isMobile: Bool = false
    var email: String = ""
    var mobile: String = ""
    var Countryname: String = ""
    var countrycode: String = ""
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        self.MainView.clipsToBounds = true
        
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
        for lbl in self.SepratorLBL {
            lbl.backgroundColor = ShadowDarkBlue.withAlphaComponent(0.3)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isCongratulation ? self.setupCongrats() : self.Setup()
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.NavLogo_IMG.tintColor = DarkBlue
    }

    // MARK:- API Calling
    
    
    // MARK:- User Define Methods

    func setupCongrats() {
        self.TitleIMG.isHidden = true
        self.DismissBTN.isHidden = true
        
        let title = "Congraulations \n\n"
        let detail = "Your prospect has just received a link to download the latest version of the BrokerApp. Click Below To Learn More About The App"
        let mainstring: String = title + detail
        let myMutableString = mainstring.Attributestring(attribute: [(title, Font.MontserratRegularFont(font: 35.0), ShadowDarkBlue), (detail, Font.MontserratRegularFont(font: 11.0), DarkBlue)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [title, detail])))
        self.TitleLBL.attributedText = myMutableString
        
        self.ProcessBTN.backgroundColor = DarkBlue
        self.ProcessBTN.setTitleColor(.white, for: .normal)
        self.ProcessBTN.setTitle("Register", for: .normal)
        self.ProcessBTN.layer.cornerRadius = 10
        
        self.EditBTN.backgroundColor = DarkBlue
        self.EditBTN.setTitleColor(.white, for: .normal)
        self.EditBTN.setTitle("Close", for: .normal)
        self.EditBTN.layer.cornerRadius = 10
    }
    
    func Setup() {
        
        self.TitleIMG.image = UIImage.init(named: "IC_Invite_Step")
        
        let title = "Create Message \n\n"
        let detail = "Let Your Receipient Know Why They Are Getting Crypto\n\n\n"
        let mainstring: String = title + detail
        let myMutableString = mainstring.Attributestring(attribute: [(title, Font.MontserratRegularFont(font: 30.0), ShadowDarkBlue), (detail, Font.MontserratRegularFont(font: 14.0), ShadowDarkBlue)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [title, detail])))
        self.TitleLBL.attributedText = myMutableString
        
        
        self.TXTMessage.font = Font.MontserratRegularFont(font: 15)
        self.TXTMessage.textColor = ShadowDarkBlue
        self.TXTMessage.attributedPlaceholder =
        NSAttributedString(string: "User Message", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        self.ProcessBTN.backgroundColor = DarkBlue
        self.ProcessBTN.setTitleColor(.white, for: .normal)
        self.ProcessBTN.setTitle("Process", for: .normal)
        self.ProcessBTN.layer.cornerRadius = 10
        
        self.EditBTN.backgroundColor = DarkBlue
        self.EditBTN.setTitleColor(.white, for: .normal)
        self.EditBTN.setTitle("Edit", for: .normal)
        self.EditBTN.layer.cornerRadius = 10
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func TappedActionBTN(_ sender: UIButton) {
        if self.isCongratulation {
            if sender == self.EditBTN {
                App?.SetHomeVC()
            }
            else {
                
            }
        }
        else {
            if sender == self.EditBTN {
                
            }
            else {
                let vc = AddTrafeAnimationVC.init(nibName: "AddTrafeAnimationVC", bundle: nil)
                vc.modalPresentationStyle = .overCurrentContext
                vc.FromCome = "Invite"
                if self.isMobile {
                    vc.mobile = self.mobile
                    vc.Countryname = self.Countryname
                    vc.countrycode = self.countrycode
                }
                else {
                    vc.email = self.email
                }
                vc.isMobile = self.isMobile
                vc.Message = self.TXTMessage.text!
                present(vc, animated: true, completion: nil)
            }
        }
    }
}
