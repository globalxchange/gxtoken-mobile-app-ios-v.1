//
//  VaultsVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 28/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class VaultsVC: BaseAppVC {
    
    // MARK:- IBOutlet Define
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    
    @IBOutlet weak var SubMain: UIView!
    
    @IBOutlet weak var Detailview: UIView!
    @IBOutlet weak var DetailLBL: UILabel!
    
    @IBOutlet weak var SegmentView: UIView!
    @IBOutlet weak var CryptoBTN: UIButton!
    @IBOutlet weak var SepratorLBL: UILabel!
    @IBOutlet weak var ForexBTN: UIButton!
    
    @IBOutlet weak var CoinTBL: UITableView!
    
    // MARK:- Variable Define
    
    var SelectedSection: Int = 0
    var Userdata : UserBalanceData!
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.setupVC()
        self.nodatafound.frame = self.SubMain.frame
        self.nodatafound.didActionBlock = {
            self.Getuserbalance()
        }
        self.Getuserbalance()
    }
    
    // MARK:- User Define methods
    
    func setupVC() {
        self.SubMain.backgroundColor = LightGroup
        
        self.Detailview.backgroundColor = ViewBGColor
        
        self.SegmentView.backgroundColor = ViewBGColor
        self.SegmentView.clipsToBounds = true
        self.SegmentView.layer.cornerRadius = 10
        self.SegmentView.layer.borderWidth = 1
        self.SegmentView.layer.borderColor = UIColor.colorWithHexString(hexStr: "EBEBEB").cgColor
        
        self.CryptoBTN.setTitle("Crypto", for: .normal)
        self.CryptoBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.CryptoBTN.setTitleColor(UIColor.white, for: .normal)
        self.CryptoBTN.backgroundColor = DarkBlue
        self.CryptoBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        
        self.ForexBTN.titleLabel?.font = Font.MontserratLightFont(font: 15)
        self.ForexBTN.setTitle("Forex", for: .normal)
        self.ForexBTN.titleLabel?.font = Font.MontserratLightFont(font: 15)
        self.ForexBTN.setTitleColor(DarkBlue, for: .normal)
        
        self.SepratorLBL.backgroundColor = UIColor.colorWithHexString(hexStr: "EBEBEB")
        
        self.TotalBalace(detailtitle: "Networth", strprice: "\n$1,367.36", strcoin: " USD", detailnote1: "\n▲ 4.59%", detailnote2: " (+212.64)")
        
        self.CoinTBL.register(UINib.init(nibName: "GXTokenCell", bundle: nil), forCellReuseIdentifier: "GXTokenCell")
        self.CoinTBL.isHidden = false
        self.CoinTBL.backgroundColor = .clear
        
        self.view.addSubview(self.nodatafound)
    }
    
    func TotalBalace(detailtitle: String, strprice: String, strcoin: String, detailnote1: String, detailnote2: String) {
        let mainstring: String = detailtitle + strprice + strcoin + detailnote1 + detailnote2
        let myMutableString = mainstring.Attributestring(attribute: [
            (detailtitle, Font.MontserratRegularFont(font: 18), DarkBlue),
            (strprice, Font.MontserratSemiBoldFont(font: 40.0), DarkBlue),
            (strcoin, Font.MontserratSemiBoldFont(font: 25), DarkBlue),
            (detailnote1, Font.MontserratSemiBoldFont(font: 13), DarkGreen),
            (detailnote2, Font.MontserratRegularFont(font: 13), DarkBlue)
        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [detailtitle ,strprice ,strcoin ,detailnote1 ,detailnote2])))
        self.DetailLBL.attributedText = myMutableString
    }
    
    // MARK:- API Calling
    
    func Getuserbalance() {
        ApiLoader.shared.showHUD()
        let param = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_VaultBalance.value(), Parameters: [:], Headers: param.description, onSuccess: { (responseObject) in
            let Data = UserBalanceRootClass.init(fromDictionary: responseObject as NSDictionary)
            if Data.status {
                self.Userdata = Data.data
                self.TotalBalace(detailtitle: "Networth \n", strprice: String.init(format: "%f", self.Userdata.totalCrypto).WithoutCoinPriceThumbRules(Coin: "USD")!, strcoin: " USD", detailnote1: "\n▲ 4.59%", detailnote2: " (+212.64)")
                self.SubMain.isHidden = false
                self.nodatafound.isHidden = true
                self.CoinTBL.delegate = self
                self.CoinTBL.dataSource = self
                self.CoinTBL.reloadData()
            }
            else {
                self.SubMain.isHidden = true
                self.nodatafound.isHidden = false
            }
            ApiLoader.shared.stopHUD()
        }) { (Message, Code) in
            self.SubMain.isHidden = true
            self.nodatafound.isHidden = false
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func MenuBTN(_ sender: UIButton) {
        let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
        Menu.OptionSelection = "vaults"
        self.navigationController?.pushViewController(Menu, animated: true)
    }
    
    @IBAction func TappedCrypto(_ sender: Any) {
        self.SelectedSection = 0
        self.CryptoBTN.setTitleColor(UIColor.white, for: .normal)
        self.CryptoBTN.backgroundColor = DarkBlue
        self.CryptoBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.ForexBTN.titleLabel?.font = Font.MontserratLightFont(font: 15)
        self.ForexBTN.setTitleColor(DarkBlue, for: .normal)
        self.ForexBTN.backgroundColor = UIColor.white
        self.CoinTBL.reloadData()
    }
    
    @IBAction func TappedForex(_ sender: Any) {
        self.SelectedSection = 1
        self.ForexBTN.setTitleColor(UIColor.white, for: .normal)
        self.ForexBTN.backgroundColor = DarkBlue
        self.ForexBTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.CryptoBTN.titleLabel?.font = Font.MontserratLightFont(font: 15)
        self.CryptoBTN.setTitleColor(DarkBlue, for: .normal)
        self.CryptoBTN.backgroundColor = UIColor.white
        self.CoinTBL.reloadData()
    }
    
}

extension VaultsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.SelectedSection == 0 ? self.Userdata.crypto.count : self.Userdata.fiat.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GXTokenCell = tableView.dequeueReusableCell(withIdentifier: "GXTokenCell") as! GXTokenCell
        cell.TilesTheme()
        if self.SelectedSection == 0 {
            let data = self.Userdata.crypto[indexPath.row]
            cell.CoinImage.downloadedFrom(link: data.icon, contentMode: .scaleAspectFit, radious: cell.CoinImage.frame.height / 2)
            cell.price.text = String.init(format: "%f", data.value).CoinPriceThumbRules(Coin: data.symbol)
            cell.title.text = data.name
            cell.isHidden = false
        }
        else {
            let data = self.Userdata.fiat[indexPath.row]
            cell.CoinImage.downloadedFrom(link: data.icon, contentMode: .scaleAspectFit, radious: cell.CoinImage.frame.height / 2)
            cell.price.text = String.init(format: "%f", data.value).CoinPriceThumbRules(Coin: "USD")
            cell.title.text = data.name
            cell.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = VaultsDetailVC.init(nibName: "VaultsDetailVC", bundle: nil)
        if self.SelectedSection == 0 {
            let data = self.Userdata.crypto[indexPath.row]
            vc.Data = data
        }
        else {
            let data = self.Userdata.fiat[indexPath.row]
            vc.Data = data
        }
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion: nil)
    }
    
}
