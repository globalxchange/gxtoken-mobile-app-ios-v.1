//
//  FullGraphVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 23/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import Charts


class FullGraphVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    @IBOutlet weak var CloseBTN: UIButton!
    @IBOutlet weak var LeftTrackBTN: UIButton!
    @IBOutlet weak var RightTrackBTN: UIButton!
    
    @IBOutlet weak var TrackInfoView: UIView!
    @IBOutlet weak var LeftTrackInfoLBL: UILabel!
    @IBOutlet weak var RightTrackInfoLBL: UILabel!
    
    @IBOutlet weak var ChartView: LineChartView!
    @IBOutlet weak var TopBG: UILabel!
    
    @IBOutlet weak var PriceView: UIView!
    @IBOutlet weak var Price_height: NSLayoutConstraint!
    @IBOutlet weak var Price_width: NSLayoutConstraint!
    @IBOutlet weak var CircleBG: UIImageView!
    @IBOutlet weak var PriceValueLBL: UILabel!
    
    // MARK:- Variable define
    var chartObj: HomeChartsData!
    
    // MARK:- Viewc Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupview()
    }
    
    // MARK:- User define methods
    
    func setupview() {
        
        self.ChartView.backgroundColor = ViewBGColor
        self.TopBG.backgroundColor = ViewBGColor
        
        self.view.backgroundColor = ViewBGColor
        self.CloseBTN.tintColor = DarkBlue
        let tracktitle = String.init(format: "GXT | %@", self.chartObj.basePair.RemoveCoinPrefix())
        self.LeftTrackBTN.setTitle(tracktitle, for: .normal)
        self.LeftTrackBTN.titleLabel?.font = Font.MontserratBoldFont(font: 20)
        self.LeftTrackBTN.setTitleColor(DarkBlue, for: .normal)
        
        let string1: String = "$1.36"
        let string2: String = " (5.65%)" as String
        let mainstring: String = string1 + string2
        let myMutableString = mainstring.Attributestring(attribute: [(string1, Font.MontserratRegularFont(font: 20.0), DarkBlue), (string2, Font.MontserratBoldFont(font: 15.0), DarkGreen)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1, string2])))
        self.RightTrackBTN.setAttributedTitle(myMutableString, for: .normal)
        
        self.TrackInfoView.backgroundColor = DarkBlue
        
        self.LeftTrackInfoLBL.text = "24 Hr Volume"
        self.LeftTrackInfoLBL.font = Font.MontserratBoldFont(font: 10)
        self.LeftTrackInfoLBL.textColor = .white
        
        self.RightTrackInfoLBL.text = "$24,367.32"
        self.RightTrackInfoLBL.font = Font.MontserratBoldFont(font: 10)
        self.RightTrackInfoLBL.textColor = .white
        
        self.PriceView.backgroundColor = .clear
        self.PriceView.clipsToBounds = true
        self.Price_height.constant = Screen_width / 2
        self.Price_width.constant = Screen_width / 2
        self.CircleBG.image = UIImage.init(named: "BalanceBG")
        let pstring1: String = "5.54321"
        let pstring2: String = "\nMarket Price" as String
        let pmainstring: String = pstring1 + pstring2
        let pmyMutableString = mainstring.Attributestring(attribute: [(pstring1, Font.MontserratRegularFont(font: 30.0), DarkBlue), (pstring2, Font.MontserratBoldFont(font: 15.0), .black)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: pmainstring, subStrings: [pstring1, pstring2])))
        self.PriceValueLBL.attributedText = pmyMutableString
        
        self.setupchart()
    }
    
    func setupchart() {
        self.ChartView.data = self.setDataCount(chart: self.chartObj.chart)

        self.ChartView.chartDescription?.enabled = false
        self.ChartView.dragEnabled = true
        self.ChartView.setScaleEnabled(false)
        self.ChartView.pinchZoomEnabled = false
        self.ChartView.xAxis.gridColor = .white

        self.ChartView.leftAxis.removeAllLimitLines()
        self.ChartView.leftAxis.enabled = false
        self.ChartView.rightAxis.enabled = false
        self.ChartView.gridBackgroundColor = .clear
        self.ChartView.drawGridBackgroundEnabled = false

        self.ChartView.legend.form = .none

        for set in self.ChartView.data!.dataSets as! [LineChartDataSet] {
            set.mode = (set.mode == .cubicBezier) ? .horizontalBezier : .cubicBezier
        }
        self.ChartView.setNeedsDisplay()

        self.ChartView.animate(xAxisDuration: 1)
    }
    
    func setDataCount(chart: [HomeChartsChart]) -> ChartData {
        var values = [ChartDataEntry]()
        
        for obj in chart {
            if obj.stockin != 0.1 && obj.stockout != 0.1 {
                let value = ChartDataEntry(x: obj.timeStamp == nil ? 0 : obj.timeStamp, y: obj.stockin == nil ? 0 :obj.stockin)
                values.append(value)
            }
        }
        
        let set1 = LineChartDataSet(entries: values, label: "")
        set1.drawIconsEnabled = false
        
        set1.lineDashLengths = [0, 0]
        set1.highlightLineDashLengths = [0, 0]
        set1.setColor(UIColor.clear)
        set1.setCircleColor(UIColor.clear)
        set1.lineWidth = 0
        set1.circleRadius = 0
        set1.drawCircleHoleEnabled = false
        set1.valueFont = Font.MontserratBoldFont(font: 12)
        set1.valueTextColor = UIColor.clear
        set1.formLineDashLengths = [0, 0]
        set1.formLineWidth = 0
        set1.formSize = 0
        
        let gradientColors = [DarkBlue.withAlphaComponent(0.1).cgColor,
                              DarkBlue.withAlphaComponent(0.8).cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        let data = LineChartData(dataSet: set1)
        
        return data
    }
    
    // MARK:- API Calling
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedClose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedTrackBTN(_ sender: UIButton) {
        if sender == self.LeftTrackBTN {
            
        }
        else {
            
        }
    }
    
}
