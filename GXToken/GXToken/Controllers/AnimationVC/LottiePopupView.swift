//
//  LottiePopupView.swift
//  GXToken
//
//  Created by Hiren Joshi on 15/12/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import Foundation
import Lottie
import UIKit


class LottiePopupView: UIView {
    
    @IBOutlet weak var Mainview: UIView!
    
    private var lottie: AnimationView!

    public var maskType: LottieHUDMaskType = .solid
    
    public struct LottieHUDConfig {
        static var shadow: CGFloat = 0.4
        static var animationDuration: TimeInterval = 0.3
    }
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.init(x: 0, y: 0, width: frame.width, height: frame.height))
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        let view = UINib(nibName: "LottiePopupView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.backgroundColor = .clear
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        main {
            self.Mainview.backgroundColor = ViewBGColor
            
            if self.maskType == .transparent {
                self.Mainview.backgroundColor = UIColor.black.withAlphaComponent(LottieHUDConfig.shadow)
            } else {
                self.Mainview.backgroundColor = UIColor.white
            }
            
            self.lottie = AnimationView(name: "Loading")
            self.lottie.loopMode = .loop
            self.lottie.contentMode = .scaleAspectFit
            self.lottie.clipsToBounds = true
            self.lottie.layer.cornerRadius = 10
            
            self.Mainview.addSubview(self.lottie)
            self.lottie.translatesAutoresizingMaskIntoConstraints = false
            self.lottie.centerXAnchor.constraint(equalTo: self.Mainview.centerXAnchor, constant: 0).isActive = true
            self.lottie.centerYAnchor.constraint(equalTo: self.Mainview.centerYAnchor, constant: 0).isActive = true
            self.lottie.heightAnchor.constraint(equalToConstant: 200).isActive = true
            self.lottie.widthAnchor.constraint(equalToConstant: 200).isActive = true
            
            UIView.animate(withDuration: LottieHUDConfig.animationDuration, delay: 0.0, options: .curveEaseIn, animations: {
                self.Mainview.alpha = 1.0
            }, completion: nil)
            self.lottie.play(completion: { _ in
                self.clearHUD()
            })
        }
    }
    
    public func clearHUD() {
        main {
            UIView.animate(withDuration: LottieHUDConfig.animationDuration, delay: 0, options: .curveEaseIn, animations: {
                
            }) { finished in
                UIApplication.shared.keyWindow!.isUserInteractionEnabled = true
                self.Mainview.removeFromSuperview()
                self.lottie.stop()
            }
        }
    }
    
}
