//
//  AnimationVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 07/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import CRNotifications
import Lottie


class AnimationVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet var bgLBL: [UILabel]!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    
    @IBOutlet weak var MainView_Height: NSLayoutConstraint!

    @IBOutlet weak var Animationview: UIView!
    @IBOutlet weak var Animation: UIView!
    
    // TODO:- Variable Define
    lazy var nodata: UIImageView = {
        () -> UIImageView in
        let image = UIImageView.init(frame: CGRect.init(x: 10, y: 10, width: self.Animation.frame.width - 10, height: self.Animation.frame.height - 10))
        image.image = UIImage.init(named: "LoginFail")
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    var AppCode: String! = ""
    var ComeFrom: String! = ""
    open var AnimationDismissAction: (() -> ())?
    
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        self.MainView.clipsToBounds = true
        self.cornerColor(color: UIColor.white)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.SetupAnimation()
        if self.ComeFrom.uppercased() == "Pulse_App_Install".uppercased() {
            self.apicall()
        }
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.NavLogo_IMG.tintColor = DarkBlue
    }
    
    // MARK:- API call
    // MARK:-
    func apicall() {
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Apple_Link.value(), Parameters: ["app_code": self.AppCode as Any], Headers: [:], onSuccess: { (responseObject) in
            let root = TestFlightRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status {
                if root.count != 0 {
                    let TestFlight_STR = (root.logs.first?.iosAppLink)!
                    let android = (root.logs.first?.androidAppLink)!
                    let install = InstallAppVC.init(nibName: "InstallAppVC", bundle: nil)
                    install.ios_link = TestFlight_STR
                    install.android_link = android
                    install.InstallDismissAction = {
                        self.dismiss(animated: false, completion: nil)
                    }
                    let vc = UINavigationController(rootViewController: install)
                    vc.setNavigationBarHidden(true, animated: true)
                    vc.modalPresentationStyle = .overCurrentContext
                    self.present(vc, animated: true, completion: nil)
                }
                else {
                    self.Animation.addSubview(self.nodata)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }) { (Message, statuscode) in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // MARK:- User Define Methods
    func cornerColor(color: UIColor = UIColor.white) {
        for lbl in self.bgLBL {
            lbl.backgroundColor = color
        }
    }

    func SetupAnimation() {
        let lottie = AnimationView(name: "Loading")
        lottie.contentMode = .scaleAspectFit
        lottie.clipsToBounds = true
        lottie.loopMode = .loop
        self.Animation.addSubview(lottie)
        self.Animation.backgroundColor = .clear
        lottie.translatesAutoresizingMaskIntoConstraints = false
        lottie.centerXAnchor.constraint(equalTo: self.Animation.centerXAnchor, constant: 0).isActive = true
        lottie.centerYAnchor.constraint(equalTo: self.Animation.centerYAnchor, constant: 0).isActive = true
        lottie.leadingAnchor.constraint(equalTo: self.Animation.leadingAnchor, constant: 0).isActive = true
        lottie.trailingAnchor.constraint(equalTo: self.Animation.trailingAnchor, constant: 0).isActive = true
        lottie.heightAnchor.constraint(equalToConstant: self.Animation.frame.size.height).isActive = true
        lottie.widthAnchor.constraint(equalToConstant: self.Animation.frame.size.width).isActive = true
        lottie.play()
    }
    
    // MARK:- IBAction Methods
    

}
