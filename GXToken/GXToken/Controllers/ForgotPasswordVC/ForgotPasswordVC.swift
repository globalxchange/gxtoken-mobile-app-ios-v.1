//
//  ForgotPasswordVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 24/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import CRNotifications
import SVPinView
import Lottie

protocol ForgotPasswordDelegate {
    func backstatus(status: Bool)
}

class ForgotPasswordVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    
    // TODO:- Comman Controls
    @IBOutlet var BGLBL: [UILabel]!
    
    @IBOutlet var LogoIMG: [UIImageView]!
    
    @IBOutlet var Bottomview: [UIView]! // 0 = EmailView, 1 = Authenticationview
    @IBOutlet var GobackBTN: [UIButton]!
    @IBOutlet var ContinueBTN: [UIButton]! // 0 = Emailview, 1 = Authentication, 2 = ChangePassword
    
    // TODO:- Email address view
    @IBOutlet weak var MainEmailView: UIView!
    @IBOutlet weak var EmailHint_LBL: UILabel!
    @IBOutlet weak var EmailView: UIView!
    @IBOutlet weak var TXTEmail: UITextField!
    
    // TODO:- Email Verify View
    @IBOutlet weak var AuthenticationView: UIView!
    @IBOutlet weak var AuthTitle_LBL: UILabel!
    @IBOutlet weak var OTPView: UIView!
    
    // TODO:- ChangePassword View
    @IBOutlet weak var ChangePassView: UIView!
    @IBOutlet weak var PassView: UIView!
    @IBOutlet weak var TXTPass: UITextField!
    
    @IBOutlet weak var ConfPassView: UIView!
    @IBOutlet weak var TXTConfPass: UITextField!
    
    @IBOutlet weak var PassHint_BTN: UIButton!
    @IBOutlet weak var PassHint_LBL: UILabel!
    
    // TODO:- Animation View
    @IBOutlet weak var AnimationView: UIView!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var width: NSLayoutConstraint!
    @IBOutlet weak var Animations: UIView!
    
    // MARK:- Variable define
    var pinView: SVPinView!
    var email: String = ""
    var password: String = ""
    var OTP_pin: String = ""
    
    let char = "8 CHARACTERS"
    let upper = "\n1 UPPER CASE"
    let num = "\n1 NUMBER"
    let special = "\n1 SPECIAL CHARACTER"
    
    var delegate: ForgotPasswordDelegate? = nil
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.EmailUI()
    }
    
    // MARK:- User define methods
    
    func EmailUI() {
        self.MainEmailView.isHidden = false
        self.AuthenticationView.isHidden = true
        self.ChangePassView.isHidden = true
        self.AnimationView.isHidden = true
        
        KeyboardAvoiding.avoidingView = self.MainEmailView
        
        self.MainEmailView.backgroundColor = LightWhite
        self.MainEmailView.layer.cornerRadius = 15
        
        self.LogoIMG[0].image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.LogoIMG[0].tintColor = DarkBlue
        
        self.BGLBL[0].backgroundColor = DarkBlue
        self.BGLBL[1].backgroundColor = DarkBlue
        
        self.EmailHint_LBL.text = "Type In Your Email To Reset Your Password"
        self.EmailHint_LBL.font = Font.MontserratRegularFont(font: 12)
        self.EmailHint_LBL.textColor = DarkBlue
        
        self.EmailView.backgroundColor = .white
        self.EmailView.clipsToBounds = true
        self.EmailView.layer.cornerRadius = 10
        self.TXTEmail.placeholder = "Current Email"
        self.TXTEmail.delegate = self
        self.TXTEmail.font = Font.MontserratRegularFont(font: 15)
        self.TXTEmail.attributedPlaceholder =
        NSAttributedString(string: "Current Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.TXTEmail.textColor = ShadowDarkBlue
        
        self.ContinueBTN[0].clipsToBounds = true
        self.ContinueBTN[0].layer.cornerRadius = 10
        self.ContinueBTN[0].setTitle("Continue", for: .normal)
        self.ContinueBTN[0].titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.ContinueBTN[0].setTitleColor(.white, for: .normal)
        self.ContinueBTN[0].backgroundColor = DarkBlue
        
        self.ContinueBTN[0].alpha = 0.5
        self.ContinueBTN[0].isUserInteractionEnabled = false
        
        self.GobackBTN[0].setTitle("Go Back", for: .normal)
        self.GobackBTN[0].titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.GobackBTN[0].setTitleColor(.white, for: .normal)
        self.Bottomview[0].backgroundColor = DarkBlue
    }
    
    func setupAuthentication() {
        self.MainEmailView.isHidden = true
        self.AuthenticationView.isHidden = false
        self.ChangePassView.isHidden = true
        self.AnimationView.isHidden = true
        
        self.AuthenticationView.backgroundColor = LightWhite
        self.AuthenticationView.layer.cornerRadius = 15
        
        KeyboardAvoiding.avoidingView = self.AuthenticationView
        
        self.LogoIMG[0].image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.LogoIMG[0].tintColor = DarkBlue
        
        self.BGLBL[0].backgroundColor = DarkBlue
        self.BGLBL[1].backgroundColor = DarkBlue
        
        let str1 = "Enter The Six Digit Code That Was Just Sent To "
        let str2 = " *Enter The Users Email”"
        let mainstring: String = str1 + str2
        let myMutableString = mainstring.Attributestring(attribute: [
            (str1, Font.MontserratRegularFont(font: 12), DarkBlue),
            (str2, Font.MontserratSemiBoldFont(font: 12.0), DarkBlue)
        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1 ,str2])))
        self.AuthTitle_LBL.attributedText = myMutableString
        
        self.ContinueBTN[1].alpha = 0.5
        self.ContinueBTN[1].isUserInteractionEnabled = false
        
        self.pinView = SVPinView.init(frame: CGRect(x: 0, y: 0, width: self.OTPView.frame.width, height: self.OTPView.frame.height))
        self.pinView.pinLength = 6
        self.pinView.interSpace = 5
        self.pinView.textColor = ShadowDarkBlue
        self.pinView.shouldSecureText = true
        self.pinView.style = .box
        self.pinView.borderLineColor = ShadowDarkBlue.withAlphaComponent(0.4)
        self.pinView.activeBorderLineColor = ShadowDarkBlue
        self.pinView.borderLineThickness = 1
        self.pinView.activeBorderLineThickness = 3
        self.pinView.font = UIFont.systemFont(ofSize: 15)
        self.pinView.keyboardType = .phonePad
        self.pinView.placeholder = "000000"
        self.pinView.becomeFirstResponderAtIndex = 0
        self.pinView.didFinishCallback = { pin in
            print("The pin entered is \(pin)")
            self.OTP_pin = pin
            self.ContinueBTN[1].alpha = 1
            self.ContinueBTN[1].isUserInteractionEnabled = true
        }
        self.pinView.pinInputAccessoryView = { () -> UIView in
            let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
            doneToolbar.barStyle = UIBarStyle.default
            let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let done: UIBarButtonItem  = UIBarButtonItem(title: "Done".localized(), style: UIBarButtonItem.Style.done, target: self, action: #selector(dissmisskeyboard))
            
            var items = [UIBarButtonItem]()
            items.append(flexSpace)
            items.append(done)
            
            doneToolbar.items = items
            doneToolbar.sizeToFit()
            return doneToolbar
        }()
        self.OTPView.addSubview(self.pinView)
        
        self.ContinueBTN[1].setTitle("Continue", for: .normal)
        self.ContinueBTN[1].titleLabel?.font = Font.MontserratRegularFont(font: 15)
        self.ContinueBTN[1].backgroundColor = DarkBlue
        self.ContinueBTN[1].setTitleColor(.white, for: .normal)
        self.ContinueBTN[1].clipsToBounds = true
        self.ContinueBTN[1].layer.cornerRadius = 10
        
        self.GobackBTN[1].setTitle("Go Back", for: .normal)
        self.GobackBTN[1].titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.GobackBTN[1].setTitleColor(.white, for: .normal)
        self.Bottomview[1].backgroundColor = DarkBlue
    }
    
    func AnimationSetup() {
        self.MainEmailView.isHidden = true
        self.AuthenticationView.isHidden = true
        self.ChangePassView.isHidden = true
        self.AnimationView.isHidden = false
        
        self.AnimationView.backgroundColor = LightWhite
        self.AnimationView.layer.cornerRadius = 15
        
        self.height.constant = Screen_width
        self.width.constant = Screen_width
        
        self.BGLBL[0].backgroundColor = ViewBGColor
        self.BGLBL[1].backgroundColor = ViewBGColor
        
        let lottie = Lottie.AnimationView(name: "Loading")
        lottie.contentMode = .scaleAspectFit
        lottie.clipsToBounds = true
        lottie.loopMode = .loop
        self.AnimationView.addSubview(lottie)
        self.AnimationView.backgroundColor = ViewBGColor
        lottie.translatesAutoresizingMaskIntoConstraints = false
        lottie.centerXAnchor.constraint(equalTo: self.Animations.centerXAnchor, constant: 0).isActive = true
        lottie.centerYAnchor.constraint(equalTo: self.Animations.centerYAnchor, constant: 0).isActive = true
        lottie.leadingAnchor.constraint(equalTo: self.Animations.leadingAnchor, constant: 0).isActive = true
        lottie.trailingAnchor.constraint(equalTo: self.Animations.trailingAnchor, constant: 0).isActive = true
        lottie.heightAnchor.constraint(equalToConstant: self.Animations.frame.size.height).isActive = true
        lottie.widthAnchor.constraint(equalToConstant: self.Animations.frame.size.width).isActive = true
        lottie.play()
    }
    
    func ChangePassUI() {
        self.MainEmailView.isHidden = true
        self.AuthenticationView.isHidden = true
        self.ChangePassView.isHidden = false
        self.AnimationView.isHidden = true
        
        self.ChangePassView.backgroundColor = LightWhite
        self.ChangePassView.layer.cornerRadius = 15
        
        KeyboardAvoiding.avoidingView = self.ChangePassView
        
        self.LogoIMG[0].image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.LogoIMG[0].tintColor = DarkBlue
        
        self.BGLBL[0].backgroundColor = ViewBGColor
        self.BGLBL[1].backgroundColor = ViewBGColor
        
        self.PassView.backgroundColor = .white
        self.PassView.clipsToBounds = true
        self.PassView.layer.cornerRadius = 10
        self.TXTPass.placeholder = "Enter New Password"
        self.TXTPass.font = Font.MontserratRegularFont(font: 15)
        self.TXTPass.attributedPlaceholder =
        NSAttributedString(string: "Enter New Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.TXTPass.textColor = ShadowDarkBlue
        
        self.ConfPassView.backgroundColor = .white
        self.ConfPassView.clipsToBounds = true
        self.ConfPassView.layer.cornerRadius = 10
        self.TXTConfPass.placeholder = "Confirm New Password"
        self.TXTConfPass.font = Font.MontserratRegularFont(font: 15)
        self.TXTConfPass.attributedPlaceholder =
        NSAttributedString(string: "Confirm New Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.TXTConfPass.textColor = ShadowDarkBlue
        
        self.TXTPass.delegate = self
        self.TXTConfPass.delegate = self
        
        self.PassHint_BTN.setTitle("See Password Checklist", for: .normal)
        self.PassHint_BTN.titleLabel?.font = Font.MontserratBoldFont(font: 15)
        self.PassHint_BTN.setTitleColor(DarkBlue, for: .normal)
        
        self.PassHint_LBL.text = ""
        self.PassHint_LBL.isHidden = true
        
        self.ContinueBTN[2].alpha = 0.5
        self.ContinueBTN[2].isUserInteractionEnabled = false
        
        self.ContinueBTN[2].setTitle("Continue", for: .normal)
        self.ContinueBTN[2].titleLabel?.font = Font.MontserratRegularFont(font: 15)
        self.ContinueBTN[2].backgroundColor = DarkBlue
        self.ContinueBTN[2].setTitleColor(.white, for: .normal)
        self.ContinueBTN[2].clipsToBounds = true
        self.ContinueBTN[2].layer.cornerRadius = 10
    }
    
    // MARK:- API Calling
    
    func API_RequestForgotPass(email: String) {
        background {
            let paramDict = RegisterBetsParamDict.init(email: email)
            
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_ForgotPassRequest.value(), Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let status = responseObject["status"] as! Bool
                        let message = responseObject["message"] as! String
                        if status {
                            self.setupAuthentication()
                        }
                        else {
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message, dismissDelay: 3, completion: {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                    }
                    else {
                        let message = responseObject["message"] as! String
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message, dismissDelay: 3, completion: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                }
            }) { (msg, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: msg, dismissDelay: 3, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            }
        }
    }
    
    func API_ConfirmPassword() {
        background {
            let paramDict = ForgotPassConfirmParam.init(email: self.TXTEmail.text!, code: self.OTP_pin, newpass: self.TXTConfPass.text!)
            
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_ForgotPassConfirm.value(), Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let status = responseObject["status"] as! Bool
                        let message = responseObject["message"] as! String
                        if status {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                self.dismiss(animated: true) {
                                    self.delegate?.backstatus(status: true)
                                }
                            }
                        }
                        else {
                            CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message, dismissDelay: 3, completion: {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                    }
                    else {
                        let message = responseObject["message"] as! String
                        CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message, dismissDelay: 3, completion: {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                }
            }) { (msg, code) in
                main {
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: msg, dismissDelay: 3, completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    
    @objc func dissmisskeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func dismiss(_ sender: UIButton) {
        if sender.tag == 0 {
            self.dismiss(animated: true, completion: nil)
        }
        else {
            self.EmailUI()
        }
    }
    
    @IBAction func TappedContinue(_ sender: UIButton) {
        if sender.tag == 0 {
            self.AnimationSetup()
            self.API_RequestForgotPass(email: self.TXTEmail.text!)
        }
        else if sender.tag == 1 {
            self.AnimationSetup()
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.ChangePassUI()
            }
        }
        else {
            self.AnimationSetup()
            self.API_ConfirmPassword()
        }
    }
    
    @IBAction func TappedPassHint(_ sender: Any) {
        self.PassHint_BTN.isHidden = true
        self.PassHint_LBL.isHidden = false
        
        let mainstring: String = char + upper + num + special
        let myMutableString = mainstring.Attributestring(attribute: [
            (char, UIFont.systemFont(ofSize: 15.0), self.TXTPass.text!.count >= 8 ? DarkBlue : UIColor.lightGray),
            (upper, UIFont.systemFont(ofSize: 15.0), self.IsContainUppercase(textvalue: self.TXTPass.text!) ? DarkBlue : UIColor.lightGray),
            (num, UIFont.systemFont(ofSize: 15.0), self.IsContainDigit(textvalue: self.TXTPass.text!) ? DarkBlue : UIColor.lightGray),
            (special, UIFont.systemFont(ofSize: 15.0), self.IsContainSpecialCharacter(textvalue: self.TXTPass.text!)  ? DarkBlue : UIColor.lightGray)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [char, upper, num, special])))
        self.PassHint_LBL.attributedText = myMutableString
    }
    
}

//MARK:- Textfield Delegates
extension ForgotPasswordVC: UITextFieldDelegate {
    
    func IsContainCharacter(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    
    func IsContainUppercase(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    
    func IsContainDigit(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "0123456789")
        if textvalue.rangeOfCharacter(from: characterset) != nil {
            return true
        }
        return false
    }
    
    func IsContainSpecialCharacter(textvalue: String) -> Bool {
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        if textvalue.rangeOfCharacter(from: characterset.inverted) != nil {
            return true
        }
        return false
    }
    
    func isValidPassword(testStr:String?) -> Bool {
        guard testStr != nil else { return false }
     
        // at least one uppercase,
        // at least one digit
        // at least one special character
        // 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{8,}")
        return passwordTest.evaluate(with: testStr)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.TXTEmail == textField {
            self.view.endEditing(true)
        }
        else if self.TXTPass == textField {
            self.TXTPass.resignFirstResponder()
            self.TXTConfPass.becomeFirstResponder()
        }
        else {
            self.view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if self.TXTEmail == textField {
            if AppUtillity.shared.validateEmail(enteredEmail: self.TXTEmail.text!) {
                self.ContinueBTN[0].alpha = 1
                self.ContinueBTN[0].isUserInteractionEnabled = true
            }
            else {
                self.ContinueBTN[0].alpha = 0.5
                self.ContinueBTN[0].isUserInteractionEnabled = false
            }
        }
        if textField == self.TXTPass || textField == self.TXTConfPass {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            
            let mainstring: String = char + upper + num + special
            let myMutableString = mainstring.Attributestring(attribute: [
                (char, UIFont.systemFont(ofSize: 15.0), self.TXTPass.text!.count >= 8 ? DarkBlue : UIColor.lightGray),
                (upper, UIFont.systemFont(ofSize: 15.0), self.IsContainUppercase(textvalue: newString as String) ? DarkBlue : UIColor.lightGray),
                (num, UIFont.systemFont(ofSize: 15.0), self.IsContainDigit(textvalue: newString as String) ? DarkBlue : UIColor.lightGray),
                (special, UIFont.systemFont(ofSize: 15.0), self.IsContainSpecialCharacter(textvalue: newString as String)  ? DarkBlue : UIColor.lightGray)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [char, upper, num, special])))
            self.PassHint_LBL.attributedText = myMutableString
            
            if textField == self.TXTConfPass {
                if self.TXTPass.text!.count >= 8 && self.IsContainUppercase(textvalue: self.TXTPass.text!) && self.IsContainDigit(textvalue: self.TXTPass.text!) && self.IsContainSpecialCharacter(textvalue: self.TXTPass.text!){
                    self.ContinueBTN[2].alpha = 1
                    self.ContinueBTN[2].isUserInteractionEnabled = true
                }
                else {
                    self.ContinueBTN[2].alpha = 0.5
                    self.ContinueBTN[2].isUserInteractionEnabled = false
                }
            }
        }
        
        return true
    }
    
}
