//
//  NodatafoundView.swift
//
//  Created by Hiren Joshi on 21/08/19.
//  Copyright © 2019 Hiren Joshi. All rights reserved.
//

import UIKit

@objc public class NodatafoundView: UIView {

    @IBOutlet var nodata: UIView!
    @IBOutlet var TitleLBL: UILabel!
    @IBOutlet var Img: UIImageView!
    @IBOutlet var DetailLBL: UILabel!
    @IBOutlet var RefreshBTN: UIButton!
    
    var didActionBlock: (()->())?
    // other outlets
    
    override init(frame: CGRect) { // for using CustomView in code
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) { // for using CustomView in IB
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        let view = UINib(nibName: "NodatafoundView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.backgroundColor = .clear
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
        nodata.frame = bounds
        nodata.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.backgroundColor = ViewBGColor
        nodata.backgroundColor = ViewBGColor

        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(RefreshBTN(_:)))
        self.nodata.addGestureRecognizer(gesture)
        self.addGestureRecognizer(gesture)
        
        self.RefreshBTN.setTitle("Refresh Now", for: .normal)
        self.RefreshBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
        self.RefreshBTN.backgroundColor = DarkBlue
        self.RefreshBTN.setTitleColor(UIColor.white, for: .normal)
        self.RefreshBTN.clipsToBounds = true
        self.RefreshBTN.layer.cornerRadius = 10.0
    }
    
    @objc public func SetupView(title: String, Notes: String, image: String, enable refresh: Bool) {
        self.TitleLBL.text = title
        self.TitleLBL.textColor = TextDark
        self.TitleLBL.font = Font.MontserratSemiBoldFont(font: 25)
        self.DetailLBL.text = Notes
        self.DetailLBL.textColor = TextDark
        if image.count == 0 {
            self.Img.image = UIImage.init(named: "NodataIMG")
        }
        else{
            self.Img.image = UIImage.init(named: image)
        }
        self.RefreshBTN.isHidden = refresh
    }
    
    @IBAction func RefreshBTN(_ sender: Any) {
        if self.didActionBlock != nil {
            self.didActionBlock!()
        }
    }
    
}
