//
//  CountryCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 11/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var flag: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.flag.layer.cornerRadius = flag.frame.height / 2
        
        self.name.font = Font.MontserratRegularFont(font: 15)
        self.name.textColor = ShadowDarkBlue
        
        self.code.font = Font.MontserratRegularFont(font: 15)
        self.code.textColor = ShadowDarkBlue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
