//
//  InviteOptionVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 09/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
import libPhoneNumber_iOS
//import SDWebImageSVGCoder

class InviteOptionVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet var NavLogo_IMG: [UIImageView]!
    
    @IBOutlet var bottomlbl: [UILabel]!
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet var SepratorLBL: [UILabel]!
    @IBOutlet weak var Logo: UIImageView!
    
    @IBOutlet weak var TitleIMG: UIImageView!
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet var MethodView: UIView!
    @IBOutlet var MethodLBL: UILabel!
    @IBOutlet var subMethod: UIView!
    @IBOutlet var TXTEmail: UITextField!
    @IBOutlet var MethodIMG: UIImageView!
    
    @IBOutlet weak var NumberView: UIView!
    @IBOutlet weak var NumberTitle: UILabel!
    @IBOutlet weak var CountrySelectionBTN: UIButton!
    
    @IBOutlet weak var NumberInputVIew: UIView!
    @IBOutlet weak var CountryVIew: UIView!
    @IBOutlet weak var CountryLogo: UIImageView!
    @IBOutlet weak var PhoneVIew: UIView!
    @IBOutlet weak var TXTPhone: UITextField!
    
    @IBOutlet weak var ProcessBTN: UIButton!
    
    @IBOutlet var CountryList_View: UIView!
    @IBOutlet weak var SubCountryListView: UIView!
    @IBOutlet weak var CountrySearchBar: UISearchBar!
    @IBOutlet weak var CountryListTBL: TPKeyboardAvoidingTableView!
    
    
    var isMobile: Bool = false
    var CountryArry: [CountryRootClass] = []
    var FilterCountryArry: [CountryRootClass] = []
    var selectedCountry: CountryRootClass!
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        self.MainView.clipsToBounds = true
        
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCountryList()
        self.NavLogo_IMG[0].image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.NavLogo_IMG[0].tintColor = DarkBlue
        self.NavLogo_IMG[1].image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.NavLogo_IMG[1].tintColor = DarkBlue
    }

    // MARK:- API Calling
    
    func getCountryList() {
        NetworkingRequests.shared.requestCountryGet(CommanApiCall.API_CoutryList.value()) { (responseObject) in
            for item in responseObject {
                let data = CountryRootClass.init(fromDictionary: item as NSDictionary)
                self.CountryArry.append(data)
                self.FilterCountryArry.append(data)
            }
            self.selectedCountry = self.CountryArry.filter({ (data) -> Bool in
                return data.name.uppercased() == "CANADA".uppercased()
            }).first
            self.Setup()
        } onError: { (message, code) in
            
        }
    }
    
    // MARK:- User Define Methods

    func AddCountryListView() {
        self.view.addSubview(self.CountryList_View)
        self.CountryList_View.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.SubCountryListView.backgroundColor = ViewBGColor
        self.CountrySearchBar.delegate = self
        self.CountryListTBL.register(UINib.init(nibName: "CountryCell", bundle: nil), forCellReuseIdentifier: "CountryCell")
        self.CountryListTBL.delegate = self
        self.CountryListTBL.dataSource = self
        self.CountryListTBL.reloadData()
    }
    
    func Setup() {
        
        KeyboardAvoiding.avoidingView = self.view
        
        self.DismissBTN.isHidden = false
        for lbl in self.SepratorLBL {
            lbl.backgroundColor = ShadowDarkBlue.withAlphaComponent(0.3)
        }
        
        self.TitleIMG.image = UIImage.init(named: "IC_Invite_Step")
        self.NumberView.isHidden = true
        
        if self.isMobile {
            self.MethodIMG.image = UIImage.init(named: "IC_Text")
            self.MethodLBL.text = "What Is Your Recipient’s Phone Number"
            self.TXTEmail.attributedPlaceholder =
            NSAttributedString(string: "Enter Phone Number", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        } else {
            self.MethodIMG.image = UIImage.init(named: "IC_Gmail")
            self.MethodLBL.text = "What Is Your Recipient’s Email"
            self.TXTEmail.attributedPlaceholder =
            NSAttributedString(string: "Enter Email", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        }
        
        let title = "You Have To Enter The Entire Phone Number Including The Area Code. "
        let click = "Click Here "
        let other = "To Get Area Code From Any Country"
        let mainstring: String = title + click + other
        let myMutableString = mainstring.Attributestring(attribute: [(title, Font.MontserratRegularFont(font: 14), UIColor.lightGray), (click, Font.MontserratRegularFont(font: 14.0), UIColor.black), (other, Font.MontserratRegularFont(font: 14), UIColor.lightGray)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [title, click, other])))
        self.NumberTitle.attributedText = myMutableString
        
        self.CountryVIew.backgroundColor = ViewBGColor
        self.CountryVIew.clipsToBounds = true
        self.CountryVIew.layer.cornerRadius = 5
        self.CountryVIew.layer.borderWidth = 0.3
        self.CountryVIew.layer.borderColor = ShadowDarkBlue.withAlphaComponent(0.3).cgColor
        
        self.CountryLogo.downloadedFrom(link: self.selectedCountry.flag, contentMode: .scaleToFill, radious: 5)
        
        self.PhoneVIew.backgroundColor = ViewBGColor
        self.PhoneVIew.clipsToBounds = true
        self.PhoneVIew.layer.cornerRadius = 5
        self.PhoneVIew.layer.borderWidth = 0.3
        self.PhoneVIew.layer.borderColor = ShadowDarkBlue.withAlphaComponent(0.3).cgColor
        
        self.TXTPhone.font = Font.MontserratRegularFont(font: 15)
        self.TXTPhone.textColor = ShadowDarkBlue
        self.TXTPhone.attributedPlaceholder =
        NSAttributedString(string: "000 000 0000", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        self.TXTPhone.delegate = self
        
        self.TitleLBL.text = "Destination"
        self.TitleLBL.font = Font.MontserratRegularFont(font: 30.0)
        self.TitleLBL.textColor = ShadowDarkBlue
        
        self.MethodView.backgroundColor = ViewBGColor
        
        self.subMethod.backgroundColor = ViewBGColor
        self.subMethod.clipsToBounds = true
        self.subMethod.layer.cornerRadius = 5
        self.subMethod.layer.borderWidth = 0.3
        self.subMethod.layer.borderColor = ShadowDarkBlue.withAlphaComponent(0.3).cgColor
        
        self.MethodLBL.font = Font.MontserratRegularFont(font: 12)
        self.MethodLBL.textColor = .lightGray
        
        self.TXTEmail.font = Font.MontserratRegularFont(font: 15)
        self.TXTEmail.textColor = ShadowDarkBlue
        self.TXTEmail.delegate = self
        
        self.ProcessBTN.backgroundColor = DarkBlue
        self.ProcessBTN.setTitleColor(.white, for: .normal)
        self.ProcessBTN.setTitle("Process", for: .normal)
        self.ProcessBTN.layer.cornerRadius = 10
        self.ProcessBTN.alpha = 0.5
        self.ProcessBTN.isUserInteractionEnabled = false
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func ProcessBTN(_ sender: Any) {
        let sms = InviteMessageVC.init(nibName: "InviteMessageVC", bundle: nil)
        if self.isMobile {
            sms.mobile = self.TXTPhone.text!
            sms.Countryname = self.selectedCountry.name
            sms.countrycode = (self.selectedCountry.currencies.first?.code)!
        }
        else {
            sms.email = self.TXTEmail.text!
        }
        sms.isMobile = self.isMobile
        let vc = UINavigationController(rootViewController: sms)
        vc.setNavigationBarHidden(true, animated: true)
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func TappedSelectCountry(_ sender: Any) {
        self.AddCountryListView()
    }
    
}

//MARK:- Textfield Delegates
extension InviteOptionVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.isMobile {
            self.TitleIMG.isHidden = true
            self.TitleLBL.isHidden = true
            self.MethodView.isHidden = true
            self.NumberView.isHidden = false
        }
        else {
            self.TitleIMG.isHidden = false
            self.TitleLBL.isHidden = false
            self.MethodView.isHidden = false
            self.NumberView.isHidden = true
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
            return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.TXTEmail == textField {
            self.TXTEmail.resignFirstResponder()
            self.view.endEditing(true)
        }
        else {
            self.TitleIMG.isHidden = true
            self.TitleLBL.isHidden = true
            self.MethodView.isHidden = true
            self.NumberView.isHidden = false
            self.TXTPhone.resignFirstResponder()
            self.view.endEditing(true)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if self.isMobile {
            let phoneUtil = NBPhoneNumberUtil()
            
            do {
                let phoneNumber: NBPhoneNumber = try phoneUtil.parse(newString as String, defaultRegion: self.selectedCountry.currencies.first?.code)
                let formattedString: String = try phoneUtil.format(phoneNumber, numberFormat: .INTERNATIONAL)
                
                NSLog("[%@]", formattedString)
                if phoneUtil.isValidNumber(phoneNumber) {
                    self.ProcessBTN.alpha = 1
                    self.ProcessBTN.isUserInteractionEnabled = true
                }
                else {
                    self.ProcessBTN.alpha = 0.5
                    self.ProcessBTN.isUserInteractionEnabled = false
                }
            }
            catch let error as NSError {
                print(error.localizedDescription)
                self.ProcessBTN.alpha = 0.5
                self.ProcessBTN.isUserInteractionEnabled = false
            }
        }
        else {
            if AppUtillity.shared.validateEmail(enteredEmail: newString as String) {
                self.ProcessBTN.alpha = 1
                self.ProcessBTN.isUserInteractionEnabled = true
            }
            else {
                self.ProcessBTN.alpha = 0.5
                self.ProcessBTN.isUserInteractionEnabled = false
            }
        }
        
        return true
    }
    
}

extension InviteOptionVC: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.CountrySearchBar.showsCancelButton = true
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentString: NSString = searchBar.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: text) as NSString
        self.FilterCountryArry.removeAll()
        self.FilterCountryArry = self.CountryArry.filter({ (country) -> Bool in
            return country.name.uppercased().contains(newString.uppercased)
        })
        self.CountryListTBL.reloadData()
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.FilterCountryArry.removeAll()
        self.FilterCountryArry = self.CountryArry.filter({ (country) -> Bool in
            return country.name.uppercased() == searchBar.text!.uppercased()
        })
        self.CountryListTBL.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.CountrySearchBar.showsCancelButton = false
        self.FilterCountryArry.removeAll()
        self.FilterCountryArry = self.CountryArry
        self.CountrySearchBar.text = ""
        self.CountryListTBL.reloadData()
    }
    
}

extension InviteOptionVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.FilterCountryArry.count
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CountryCell = tableView.dequeueReusableCell(withIdentifier: "CountryCell") as! CountryCell
        let data = self.FilterCountryArry[indexPath.row]
        cell.flag.downloadedFrom(link: data.flag, contentMode: .scaleAspectFit, radious: cell.flag.frame.height / 2)
        cell.name.text = data.name
        cell.code.text = String.init(format: "+%@", data.numericCode)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCountry = self.FilterCountryArry[indexPath.row]
        self.CountryLogo.downloadedFrom(link: self.selectedCountry.flag, contentMode: .scaleToFill, radious: 5)
        self.CountryList_View.removeFromSuperview()
    }
    
}
