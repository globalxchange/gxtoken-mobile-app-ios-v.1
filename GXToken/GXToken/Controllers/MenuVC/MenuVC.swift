//
//  MenuVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 23/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class MenuVC: BaseAppVC {

    // MARK:- Outlet Controls
    @IBOutlet weak var CloseBTN: UIButton!
    
    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var ProfileIMG: UIImageView!
    @IBOutlet weak var ProfileName: UILabel!
    
    @IBOutlet weak var OptionsStack: UIStackView!
    @IBOutlet weak var TradeBTN: UIButton!
    @IBOutlet weak var VaultsBTN: UIButton!
    @IBOutlet weak var PulseBTN: UIButton!
    
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet weak var LoginBTN: UIButton!
    
    var OptionSelection: String = ""
    
    // MARK:- Viewc Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupview()
    }
    
    // MARK:- User define methods
    
    func setupview() {
        self.view.backgroundColor = ViewBGColor
    
        self.ProfileView.backgroundColor = ViewBGColor
        
        self.CloseBTN.tintColor = DarkBlue
        
        self.TradeBTN.setTitle("Trade", for: .normal)
        self.TradeBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
        self.TradeBTN.setTitleColor(DarkBlue, for: .normal)
        
        self.VaultsBTN.setTitle("Vaults", for: .normal)
        self.VaultsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
        self.VaultsBTN.setTitleColor(DarkBlue, for: .normal)
        
        if App?.BundleID?.uppercased() == "com.nvest.GXTokenTerminal".uppercased() {
            self.PulseBTN.isHidden = false
        }
        else {
            self.PulseBTN.isHidden = true
        }
        self.PulseBTN.setTitle("Pulse", for: .normal)
        self.PulseBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
        self.PulseBTN.setTitleColor(DarkBlue, for: .normal)
        
        if self.OptionSelection.uppercased() == "PULSE" {
            self.TradeBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
            self.VaultsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
            self.PulseBTN.titleLabel?.font = Font.MontserratBoldFont(font: 25)
        }
        else if self.OptionSelection.uppercased() == "TRADE" {
            self.PulseBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
            self.VaultsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
            self.TradeBTN.titleLabel?.font = Font.MontserratBoldFont(font: 25)
        } else {
            self.TradeBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
            self.PulseBTN.titleLabel?.font = Font.MontserratRegularFont(font: 25)
            self.VaultsBTN.titleLabel?.font = Font.MontserratBoldFont(font: 25)
        }
        
        if CommonSetters().isLogin {
            self.LoginBTN.setTitle("Login", for: .normal)
            self.ProfileIMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "colouredicon")
            self.ProfileName.text = ""
        }
        else {
            self.LoginBTN.setTitle("Logout", for: .normal)
            
            let userdata = UserInfoData.shared.GetUserdata()
            self.ProfileIMG.clipsToBounds = true
            self.ProfileIMG.layer.cornerRadius = self.ProfileIMG.frame.height / 2
            self.ProfileIMG.layer.borderColor = DarkBlue.cgColor
            self.ProfileIMG.layer.borderWidth = 1
            self.ProfileIMG.downloadedFrom(link: userdata?.profileImg, contentMode: .scaleAspectFit, radious: ProfileIMG.frame.height / 2)
            
            let title = "$0.00\n"
            let detail = "Invite Friends"
            let mainstring: String = title + detail
            let myMutableString = mainstring.Attributestring(attribute: [(title, Font.MontserratRegularFont(font: 30.0), DarkBlue), (detail, Font.MontserratRegularFont(font: 16.0), DarkBlue)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [title, detail])))
            self.ProfileName.attributedText = myMutableString
        }
        self.LoginBTN.titleLabel?.font = Font.MontserratRegularFont(font: 16)
        self.LoginBTN.setTitleColor(DarkBlue, for: .normal)
        self.LoginBTN.clipsToBounds = true
        self.LoginBTN.layer.borderColor = DarkBlue.cgColor
        self.LoginBTN.layer.borderWidth = 1
    }
    
    // MARK:- API Calling
    
    // MARK:- IBAction Methods

    @IBAction func TappedClose(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func TappedTrade(_ sender: UIButton) {
        let vc = MainHomeVC.init(nibName: "MainHomeVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func TappedVaults(_ sender: UIButton) {
        if CommonSetters().isLogin {
            let login = LoginVC.init(nibName: "LoginVC", bundle: nil)
            let vc = UINavigationController(rootViewController: login)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
        else {
            let vc = VaultsVC.init(nibName: "VaultsVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func TappedPulse(_ sender: UIButton) {
//        if CommonSetters().isLogin {
//            let login = LoginVC.init(nibName: "LoginVC", bundle: nil)
//            let vc = UINavigationController(rootViewController: login)
//            vc.setNavigationBarHidden(true, animated: true)
//            vc.modalPresentationStyle = .overCurrentContext
//            present(vc, animated: true, completion: nil)
//        }
//        else {
            let vc = loadViewController("Main", "PulseTabbarVC")
            self.navigationController?.pushViewController(vc, animated: true)
//        }
    }
    
    @IBAction func TappedLogin(_ sender: UIButton) {
        if CommonSetters().isLogin {
            let login = LoginVC.init(nibName: "LoginVC", bundle: nil)
            let vc = UINavigationController(rootViewController: login)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
        else {
            UserInfoData.shared.UserLogout()
        }
    } 
    
    @IBAction func TappedInviteBTN(_ sender: UIButton) {
        if CommonSetters().isLogin {
//            DO that things which no need to login user
        }
        else {
            let vc = InviteFriendsVC.init(nibName: "InviteFriendsVC", bundle: nil)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}
