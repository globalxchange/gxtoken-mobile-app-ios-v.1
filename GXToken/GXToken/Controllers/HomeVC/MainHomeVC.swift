//
//  MainHomeVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 09/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class MainHomeVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    
    @IBOutlet weak var SubMainView: UIView!
    @IBOutlet weak var GraphView: UIView!
    @IBOutlet var carousel: iCarousel!
    
    @IBOutlet weak var DetailsView: UIView!
    @IBOutlet weak var SubDetailsView: UIStackView!
    @IBOutlet weak var MarketBTN: UIButton!
    @IBOutlet weak var DetailLimitLBL: UILabel!
    @IBOutlet weak var StakeBTN: UIButton!
    
    @IBOutlet weak var GXTAmount: UILabel!
    @IBOutlet weak var TXTGXT: UITextField!
    
    @IBOutlet weak var LimitPriceview: UIView!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var Total: UILabel!
    @IBOutlet weak var TotalPriceview: UIView!
    @IBOutlet weak var ValuePriceLBL: UILabel!
    @IBOutlet weak var Seprator: UILabel!
    @IBOutlet weak var ValueTotalLBL: UILabel!
    
    @IBOutlet weak var MarketPriceview: UIView!
    @IBOutlet weak var MarketTotal: UILabel!
    @IBOutlet weak var MarketTotalPriceview: UIView!
    @IBOutlet weak var MarketValueTotalLBL: UILabel!
    
    @IBOutlet weak var StakePriceview: UIView!
    @IBOutlet weak var StakeTotal: UILabel!
    @IBOutlet weak var StakeTotalPriceview: UIView!
    @IBOutlet weak var StakeValueTotalLBL: UILabel!
    @IBOutlet weak var StakeTotal_Leading: NSLayoutConstraint!
    @IBOutlet weak var StakeDownIMG: UIImageView!
    @IBOutlet weak var StakeAppIMG: UIImageView!
    @IBOutlet weak var StakeOptionBTN: UIButton!
    
    @IBOutlet var ComingSoonView: UIView!
    @IBOutlet weak var StrakeCommingView: UIView!
    @IBOutlet weak var CMLogo_IMG: UIImageView!
    @IBOutlet weak var CMtopview: UIView!
    @IBOutlet weak var CMTitle: UILabel!
    @IBOutlet weak var CMBackBTN: UIButton!
    
    @IBOutlet weak var SellView: UIView!
    @IBOutlet weak var SellBTN: UIButton!
    @IBOutlet weak var BuyView: UIView!
    @IBOutlet weak var BuyBTN: UIButton!
    
    @IBOutlet var StakeOptionView: UIView!
    @IBOutlet var SubStakeOption: UIView!
    @IBOutlet weak var SubStakeOption_height: NSLayoutConstraint!
    
    @IBOutlet weak var SP_StratchBTN: UIButton!
    @IBOutlet weak var SP_Tittle: UILabel!
    @IBOutlet weak var SP_SearchView: UIView!
    @IBOutlet weak var SP_AppBTN: UIButton!
    @IBOutlet weak var SP_Segment: UILabel!
    @IBOutlet weak var SP_Searchbar: UITextField!
    @IBOutlet weak var SP_Search_leading: NSLayoutConstraint!
    
    @IBOutlet weak var SP_TokenTBL: UITableView!
    @IBOutlet weak var SP_AppCollection: UICollectionView!
    
    
    // MARK:- Variable define
    
    var ChartArry : [HomeChartsData]!
    var AppsArry: [CatAppsApp]!
    var filter_AppsArry: [CatAppsApp]!
    var Selected_App: CatAppsApp!
    var selected_Coin: String! = ""
    var SelectedOption: Int = 0
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.CMLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.TXTGXT.delegate = self
        self.nodatafound.frame = self.SubMainView.frame
        self.nodatafound.didActionBlock = {
            self.getgraphdata()
        }
        self.getgraphdata()
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(dissmissAction))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    // MARK:- User define methods
    
    @objc override func dissmissAction () {
        self.view.endEditing(true)
    }
    
    func setupUI() {
        KeyboardAvoiding.avoidingView = self.view
        
        self.SubMainView.isHidden = false
        
        self.carousel.type = .rotary
        self.carousel.delegate = self
        self.carousel.dataSource = self
        self.carousel.reloadData()
        
        if !self.selected_Coin.isEmpty {
            let index = self.ChartArry.enumerated().filter { (index, data) -> Bool in
                var symbol = data.basePair
                return symbol?.replaceLocalized(fromvalue: ["GXT"], tovalue: [""]).uppercased() == selected_Coin.uppercased()
            }.map({ $0.offset }).first
            self.carousel.currentItemIndex = index!
        }
        
        self.MarketBTN.isSelected = true
        self.MarketGraphLogo(visible: false)
        self.MarketBTN.setTitle("Limit", for: .selected)
        self.StakeBTN.setTitle("Stake", for: .normal)
        self.StakeBTN.isSelected = false
        self.selectedOrderFlow(order: "Market")
        
        self.MarketBTN.titleLabel?.font = Font.MontserratRegularFont(font: 15)
        self.MarketBTN.setTitleColor(DarkBlue, for: .normal)
        
        self.StakeBTN.titleLabel?.font = Font.MontserratRegularFont(font: 15)
        self.StakeBTN.setTitleColor(DarkBlue, for: .normal)
        
        self.GXTAmount.text = "Amount of GXT"
        self.GXTAmount.font = Font.MontserratRegularFont(font: 16)
        self.GXTAmount.textColor = DarkBlue
        
        self.TXTGXT.font = Font.bestFittingFont(for: "0.00", in: self.TXTGXT.bounds, fontDescriptor: self.TXTGXT.font!.fontDescriptor)
//        self.TXTGXT.font = Font.MontserratRegularFont(font: 30)
        self.TXTGXT.textColor = TextColor
        self.TXTGXT.attributedPlaceholder =
        NSAttributedString(string: "0.00", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        self.Price.text = "Price"
        self.Price.font = Font.MontserratRegularFont(font: 16)
        self.Price.textColor = DarkBlue
        
        self.Total.text = "Total"
        self.Total.font = Font.MontserratRegularFont(font: 16)
        self.Total.textColor = DarkBlue
        
        self.TotalPriceview.clipsToBounds = true
        self.TotalPriceview.layer.borderWidth = 1
        self.TotalPriceview.layer.borderColor = LightBlue.cgColor
        self.Seprator.backgroundColor = LightBlue
        
        self.ValuePriceLBL.text = "0.00"
        self.ValuePriceLBL.font = Font.MontserratRegularFont(font: 23)
        self.ValuePriceLBL.textColor = DarkBlue
        
        self.ValueTotalLBL.text = "0.00"
        self.ValueTotalLBL.font = Font.MontserratRegularFont(font: 23)
        self.ValueTotalLBL.textColor = DarkBlue
        
        self.MarketTotal.text = "Total"
        self.MarketTotal.font = Font.MontserratRegularFont(font: 16)
        self.MarketTotal.textColor = DarkBlue
        
        self.MarketTotalPriceview.clipsToBounds = true
        self.MarketTotalPriceview.layer.borderWidth = 1
        self.MarketTotalPriceview.layer.borderColor = LightBlue.cgColor
        
        self.MarketValueTotalLBL.text = "0.00"
        self.MarketValueTotalLBL.font = Font.MontserratRegularFont(font: 23)
        self.MarketValueTotalLBL.textColor = DarkBlue
        
        self.StakeTotal.text = ""
        self.StakeTotal.font = Font.MontserratRegularFont(font: 16)
        self.StakeTotal.textColor = DarkBlue
        
        self.StakeValueTotalLBL.text = "Select Staking Contract"
        self.StakeValueTotalLBL.font = Font.MontserratRegularFont(font: 16)
        self.StakeValueTotalLBL.textColor = DarkBlue
        
        self.StakeAppIMG.isHidden = true
        self.StakeTotal_Leading.constant = 10
        
        self.ComingSoonView.backgroundColor = ViewBGColor
        self.StrakeCommingView.backgroundColor = ViewBGColor
        
        self.StakeTotalPriceview.clipsToBounds = true
        self.StakeTotalPriceview.layer.borderWidth = 1
        self.StakeTotalPriceview.layer.borderColor = LightBlue.cgColor
        self.StakeDownIMG.tintColor = DarkBlue
        
        self.SellBTN.setTitle("SELL ↓", for: .normal)
        self.SellBTN.titleLabel?.font = Font.MontserratBoldFont(font: 20)
        self.SellBTN.setTitleColor(.white, for: .normal)
        self.SellBTN.backgroundColor = .clear
        self.SellView.backgroundColor = DarkRed
        self.SellBTN.clipsToBounds = true
        self.SellBTN.layer.cornerRadius = 10
        
        self.BuyBTN.setTitle("BUY ↑", for: .normal)
        self.BuyBTN.titleLabel?.font = Font.MontserratBoldFont(font: 20)
        self.BuyBTN.setTitleColor(.white, for: .normal)
        self.BuyBTN.backgroundColor = .clear
        self.BuyView.backgroundColor = DarkBlue
        self.BuyBTN.clipsToBounds = true
        self.BuyBTN.layer.cornerRadius = 10
        
        self.GetAppListing()
    }
    
    func selectedOrderFlow(order: String) {
        if order.uppercased() == "Limit".uppercased() {
            self.SelectedOption = 0
        }
        else if order.uppercased() == "Market".uppercased() {
            self.SelectedOption = 1
        }
        else {
            self.SelectedOption = 2
        }
        self.DetailLimitLBL.font = Font.MontserratBoldFont(font: 25)
        self.DetailLimitLBL.textColor = DarkBlue
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.thick.rawValue]
        let underlineAttributedString = NSAttributedString(string: order, attributes: underlineAttribute)
        self.DetailLimitLBL.attributedText = underlineAttributedString
    }
    
    func MarketGraphLogo(visible: Bool = true) {
        self.LimitPriceview.isHidden = !visible
        self.MarketPriceview.isHidden = visible
        self.StakePriceview.isHidden = true
    }
    
    func StakeGraphLogo(visible: Bool = true) {
        self.LimitPriceview.isHidden = !visible
        self.MarketPriceview.isHidden = true
        self.StakePriceview.isHidden = visible
    }
    
    func setupStakeOptionPopup() {
        
        self.StakeOptionView.frame = CGRect.init(x: 0, y: 0, width: Screen_width, height: Screen_height)
        self.view.addSubview(self.StakeOptionView)
        self.StakeOptionView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        self.SubStakeOption.backgroundColor = ViewBGColor
        self.SubStakeOption.clipsToBounds = true
        self.SubStakeOption.layer.cornerRadius = 10.0
        self.SubStakeOption_height.constant = Screen_height / 1.5
        
        self.SP_Tittle.text = "Select App"
        self.SP_Tittle.font = Font.MontserratBoldFont(font: 30)
        self.SP_Tittle.textColor = DarkBlue
        
        self.SP_SearchView.backgroundColor = ViewBGColor
        self.SP_SearchView.clipsToBounds = true
        self.SP_SearchView.layer.cornerRadius = 10
        self.SP_SearchView.layer.borderWidth = 1
        self.SP_SearchView.layer.borderColor = UIColor.colorWithHexString(hexStr: "EBEBEB").cgColor
        
        self.SP_Search_leading.constant = 10
        self.SP_Searchbar.placeholder = "Select App"
        self.SP_Searchbar.delegate = self
        self.SP_Searchbar.font = Font.MontserratRegularFont(font: 15)
        self.SP_Searchbar.textColor = DarkBlue
        self.SP_Searchbar.attributedPlaceholder =
        NSAttributedString(string: self.SP_Searchbar.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        self.SP_AppBTN.isHidden = true
        self.SP_Segment.isHidden = true
        
        self.SP_Segment.backgroundColor = UIColor.colorWithHexString(hexStr: "EBEBEB")
        
        self.SP_AppCollection.register(UINib.init(nibName: "ApplicationCell", bundle: nil), forCellWithReuseIdentifier: "ApplicationCell")
        self.SP_AppCollection.isHidden = false
        
        self.SP_AppCollection.delegate = self
        self.SP_AppCollection.dataSource = self
        self.SP_AppCollection.reloadData()
        self.SP_AppCollection.backgroundColor = ViewBGColor
        
        self.SP_TokenTBL.register(UINib.init(nibName: "GXTokenCell", bundle: nil), forCellReuseIdentifier: "GXTokenCell")
        
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.SP_TokenTBL.frame.width, height: 30))
        footer.backgroundColor = ViewBGColor
        self.SP_TokenTBL.tableFooterView = footer
        
        self.SP_TokenTBL.isHidden = true
    }
    
    func UpdateUIwithSelectedChart(index: Int) {
        let obj = self.ChartArry[index]
        self.Price.text = String.init(format: "Price %@", obj.basePair.RemoveCoinPrefix())
        self.Total.text = String.init(format: "Total %@", obj.basePair.RemoveCoinPrefix())
        self.ValuePriceLBL.text = String.init(format: "%f", obj.price).WithoutCoinPriceThumbRules(Coin: obj.basePair.RemoveCoinPrefix())
        if let n = NumberFormatter().number(from: self.TXTGXT.text! as String) {
            let f = Double(truncating: n)
            let totalprice = obj.price * f
            self.ValueTotalLBL.text = String.init(format: "%f", totalprice).WithoutCoinPriceThumbRules(Coin: obj.basePair.RemoveCoinPrefix())
            self.MarketValueTotalLBL.text = String.init(format: "%f", totalprice).WithoutCoinPriceThumbRules(Coin: obj.basePair.RemoveCoinPrefix())
        }
        self.MarketTotal.text = String.init(format: "Total %@", obj.basePair.RemoveCoinPrefix())
        self.StakeTotal.text = ""
    }
    
    // MARK:- API Calling
    
    func getgraphdata() {
        ApiLoader.shared.showHUD()
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_HomeChart.value(), onSuccess: { (responseObject) in
            let chartData = HomeChartsRootClass.init(fromDictionary: responseObject as NSDictionary)
            if chartData.status {
                self.ChartArry = [HomeChartsData]()
                self.ChartArry = chartData.data
                self.setupUI()
                self.SubMainView.isHidden = false
            }
            else {
                self.SubMainView.isHidden = true
                self.view.addSubview(self.nodatafound)
            }
            ApiLoader.shared.stopHUD()
        }) { (Message, statuscode) in
            self.SubMainView.isHidden = true
            self.view.addSubview(self.nodatafound)
            ApiLoader.shared.stopHUD()
        }
    }
    
    func GetAppListing() {
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Cat_Applist.value(), onSuccess: { (responseObject) in
            let root = CatAppsRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status && root.count >= 1 {
                self.AppsArry = root.apps
                self.filter_AppsArry = self.AppsArry
                self.SP_AppCollection.reloadData()
            }
            else {
                
            }
            ApiLoader.shared.stopHUD()
        }) { (Message, statuscode) in
            ApiLoader.shared.stopHUD()
        }
    }
    
    func TradeLimit(IsSell: Bool, price: String, amount: String, basePair: String) {
        background {
            let param = LimitSell_BuyParamDict.init(price: price, amount: amount, basePair: basePair)
            let header = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
            NetworkingRequests.shared.requestPOST(IsSell ? CommanApiCall.API_LimitSell.value() : CommanApiCall.API_LimitBuy.value(), Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    let Data = TradeRootClass.init(fromDictionary: responseObject as NSDictionary)
                    if Data.status {
                        
                    }
                }
            }) { (Message, code) in
                main {
                    
                }
            }
        }
    }
    
    func TradeMarket(IsSell: Bool, amount: String, basePair: String) {
        background {
            let param = MarketSell_BuyParamDict.init(amount: amount, basePair: basePair)
            let header = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
            NetworkingRequests.shared.requestPOST(IsSell ? CommanApiCall.API_MarketSell.value() : CommanApiCall.API_MarketBuy.value(), Parameters: param.description, Headers: header.description, onSuccess: { (responseObject, status) in
                main {
                    let Data = TradeRootClass.init(fromDictionary: responseObject as NSDictionary)
                    if Data.status {
                        
                    }
                }
            }) { (Message, code) in
                main {
                    
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func MenuBTN(_ sender: UIButton) {
        let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
        Menu.OptionSelection = "trade"
        self.navigationController?.pushViewController(Menu, animated: true)
    }
    
    @IBAction func SettingBTN(_ sender: UIButton) {
        let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
        let vc = UINavigationController(rootViewController: setting)
        vc.setNavigationBarHidden(true, animated: true)
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func TappedMarket(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            self.MarketBTN.setTitle("Market", for: .normal)
            self.selectedOrderFlow(order: "Limit")
            self.MarketGraphLogo(visible: true)
        }
        else {
            sender.isSelected = true
            self.MarketGraphLogo(visible: false)
            self.MarketBTN.setTitle("Limit", for: .selected)
            self.StakeBTN.setTitle("Stake", for: .normal)
            self.StakeBTN.isSelected = false
            self.selectedOrderFlow(order: "Market")
        }
    }
    
    @IBAction func TappedStake(_ sender: UIButton) {
        if App?.BundleID?.uppercased() == "com.nvest.GXTokenTerminal".uppercased() {
            self.CMtopview.backgroundColor = DarkBlue
            self.CMTitle.text = "Staking Interface Coming Soon For Supported Assets"
            self.CMTitle.font = Font.MontserratRegularFont(font: 15)
            self.CMTitle.textColor = LightBlue
            
            self.CMBackBTN.setTitle("Go Back", for: .normal)
            self.CMBackBTN.setTitleColor(LightBlue, for: .normal)
            self.CMBackBTN.layer.borderColor = ShadowDarkBlue.cgColor
            self.CMBackBTN.layer.borderWidth = 0.5
            
            self.ComingSoonView.frame = CGRect(x: 0, y: 0, width: Screen_width, height: Screen_height)
            self.view.addSubview(self.ComingSoonView)
            self.ComingSoonView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
        else {
            if sender.isSelected {
                sender.isSelected = false
                self.StakeBTN.setTitle("Stake", for: .normal)
                self.selectedOrderFlow(order: "Limit")
                self.StakeGraphLogo(visible: true)
            }
            else {
                sender.isSelected = true
                self.StakeGraphLogo(visible: false)
                self.StakeBTN.setTitle("Limit", for: .selected)
                self.MarketBTN.setTitle("Market", for: .normal)
                self.MarketBTN.isSelected = false
                self.selectedOrderFlow(order: "Stake")
            }
        }
    }
    
    @IBAction func TappedSell(_ sender: Any) {
        if CommonSetters().isLogin {
            let login = LoginVC.init(nibName: "LoginVC", bundle: nil)
            let vc = UINavigationController(rootViewController: login)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
        else {
            if self.TXTGXT.text?.count != 0 {
                let CaroseulData = self.ChartArry[self.carousel.currentItemIndex]
                if self.SelectedOption == 0 {
                    // "Limit"
                    // self.ValueTotalLBL.text!
                    self.TradeLimit(IsSell: true, price: String.init(format: "%f", CaroseulData.price), amount: self.TXTGXT.text!, basePair: CaroseulData.basePair)
                }
                else if self.SelectedOption == 1 {
                    // "Market"
                    // self.MarketValueTotalLBL.text
                    self.TradeMarket(IsSell: true, amount: self.TXTGXT.text!, basePair: CaroseulData.basePair)
                }
                else {
                    // "Strake"
                }
            }
        }
    }
    
    @IBAction func TappedBuy(_ sender: Any) {
        if CommonSetters().isLogin {
            let login = LoginVC.init(nibName: "LoginVC", bundle: nil)
            let vc = UINavigationController(rootViewController: login)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
        else {
            if self.TXTGXT.text?.count != 0 {
                let CaroseulData = self.ChartArry[self.carousel.currentItemIndex]
                if self.SelectedOption == 0 {
                    // "Limit"
                    // self.ValueTotalLBL.text!
                    self.TradeLimit(IsSell: false, price: String.init(format: "%f", CaroseulData.price), amount: self.TXTGXT.text!, basePair: CaroseulData.basePair)
                }
                else if self.SelectedOption == 1 {
                    // "Market"
                    // self.MarketValueTotalLBL.text
                    self.TradeMarket(IsSell: false, amount: self.TXTGXT.text!, basePair: CaroseulData.basePair)
                }
                else {
                    // "Strake"
                }
            }
        }
    }
    
    
    @IBAction func TappedCMBack(_ sender: Any) {
        self.ComingSoonView.removeFromSuperview()
    }
    
    @IBAction func TappedStakeOption(_ sender: UIButton) {
        if sender == self.StakeOptionBTN {
            self.setupStakeOptionPopup()
        }
        else if sender == self.SP_StratchBTN {
            self.StakeOptionView.removeFromSuperview()
        }
        else if sender == self.SP_AppBTN {
            
        }
        else {
            
        }
    }
    
}

// MARK:- UITableViewDelegates
extension MainHomeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GXTokenCell = tableView.dequeueReusableCell(withIdentifier: "GXTokenCell") as! GXTokenCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.SP_TokenTBL {
            self.StakeOptionView.removeFromSuperview()
            self.StakeAppIMG.isHidden = false
            self.StakeTotal_Leading.constant = 50
            self.StakeValueTotalLBL.text = String.init(format: "%@ - %@", self.Selected_App.appName, "Coin")
            self.StakeTotal.text = "Select Staking Contract"
        }
    }
    
}

// MARK:- UICollectionViewDelegate
extension MainHomeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filter_AppsArry.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ApplicationCell", for: indexPath) as! ApplicationCell
        
        let app = self.filter_AppsArry[indexPath.row]
        
        cell.title.text = app.appName
        cell.icon.downloadedFrom(link: app.appIcon, contentMode: .scaleAspectFit, radious: 10)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.SP_AppBTN.isHidden = false
        self.SP_Segment.isHidden = false
        self.SP_Search_leading.constant = 70
        self.SP_AppCollection.isHidden = true
        
        self.Selected_App = self.filter_AppsArry[indexPath.row]
        
        self.SP_Tittle.text = "Select Contract"
        self.SP_Searchbar.placeholder = "Search BrokerApp Contract"
        self.SP_Searchbar.attributedPlaceholder =
            NSAttributedString(string: self.SP_Searchbar.placeholder!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray])
        
        self.SP_TokenTBL.isHidden = false
        self.SP_TokenTBL.delegate = self
        self.SP_TokenTBL.dataSource = self
        self.SP_TokenTBL.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 3, height: 100)
    }
    
}

// MARK:- iCarouseldelegates
extension MainHomeVC: iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.ChartArry.count
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return 1
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var itemView: CarouselCoinView
        
        if let view = view {
            itemView = view as! CarouselCoinView
        } else {
            itemView = CarouselCoinView(frame: CGRect(x: 0, y: 0, width: carousel.frame.height, height: carousel.frame.height))
        }
        itemView.setupView(obj: self.ChartArry[index], Index: index)
        itemView.StrachactionHandler = { (selectedTag) in
            let strach = FullGraphVC.init(nibName: "FullGraphVC", bundle: nil)
            strach.chartObj = self.ChartArry[selectedTag]
            let vc = UINavigationController(rootViewController: strach)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
        itemView.CoinactionHandler = { (selectedTag) in
            
        }
        self.UpdateUIwithSelectedChart(index: carousel.currentItemIndex)
        return itemView
    }

    private func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        self.UpdateUIwithSelectedChart(index: carousel.currentItemIndex)
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        self.UpdateUIwithSelectedChart(index: carousel.currentItemIndex)
    }
    
}

// MARK:- UITextFieldDelegates
extension MainHomeVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        if textField == self.TXTGXT {
            //        self.UpdateUIwithSelectedChart(index: self.carousel.currentItemIndex)
            if let n = NumberFormatter().number(from: newString as String) {
                let obj = self.ChartArry[self.carousel.currentItemIndex]
                let f = Double(truncating: n)
                let totalprice = obj.price * f
                self.ValueTotalLBL.text = String.init(format: "%f", totalprice).WithoutCoinPriceThumbRules(Coin: obj.basePair.RemoveCoinPrefix())
                self.MarketValueTotalLBL.text = String.init(format: "%f", totalprice).WithoutCoinPriceThumbRules(Coin: obj.basePair.RemoveCoinPrefix())
            }
        }
        else {
            
        }
        return true
    }
    
}
