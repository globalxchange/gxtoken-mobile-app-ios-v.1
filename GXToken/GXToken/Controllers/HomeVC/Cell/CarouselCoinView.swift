//
//  CarouselCoinView.swift
//  CryptoGame
//
//  Created by Shorupan Pirakaspathy on 2020-04-29.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import Charts
import SwiftCharts

class CarouselCoinView: UIView {
    
    @IBOutlet weak var Carousel: UIView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var StrachBTN: UIButton!
    @IBOutlet weak var Icon: UIImageView!
    @IBOutlet weak var CoinBTN: UIButton!
    @IBOutlet weak var PriceLBL: UILabel!
    @IBOutlet weak var RateLBL: UILabel!
    @IBOutlet weak var ChartView: LineChartView!
    @IBOutlet weak var TopBG: UILabel!
    
    @objc public var StrachactionHandler: ((Int) -> Void)?
    @objc public var CoinactionHandler: ((Int) -> Void)?
    
    fileprivate var chart: Chart? // arc
    var CoinIMG: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        
        let view = UINib(nibName: "CarouselCoinView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.backgroundColor = .clear
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
        self.Carousel.backgroundColor = .clear
        
        self.subView.backgroundColor = ViewBGColor
        self.subView.layer.borderColor = UIColor.black.cgColor
        self.subView.layer.shadowColor = UIColor.black.cgColor
        self.subView.layer.shadowRadius = 1
        self.subView.layer.shadowOpacity = 0.4
        self.subView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        self.ChartView.backgroundColor = ViewBGColor
        self.TopBG.backgroundColor = ViewBGColor
        
        self.Icon.clipsToBounds = true
        self.Icon.layer.cornerRadius = self.Icon.frame.height / 2
        
        self.CoinBTN.titleLabel?.font = Font.MontserratBoldFont(font: 20)
        self.CoinBTN.setTitleColor(DarkBlue, for: .normal)
        self.CoinBTN.isUserInteractionEnabled = false
        
        self.PriceLBL.font = Font.MontserratRegularFont(font: 16)
        self.PriceLBL.textColor = DarkBlue
        
        self.RateLBL.font = Font.MontserratRegularFont(font: 12)
        self.RateLBL.textColor = DarkRed
    }
    
    func setupView(obj: HomeChartsData, Index: Int) {
        if obj.imageURL.IsStrEmpty() {
            self.Icon.image = UIImage.init(named: "IC_Application")
        }
        else {
            self.Icon.downloadedFrom(link: obj.imageURL, contentMode: .scaleAspectFit, radious: self.Icon.frame.height / 2)
        }
        self.CoinBTN.tag = Index
        self.StrachBTN.tag = Index
        self.CoinBTN.setTitle(obj.basePair, for: .normal)
        self.PriceLBL.text = String.init(format: "%f", obj.price).CoinPriceThumbRules(Coin: obj.basePair.RemoveCoinPrefix())
        self.RateLBL.text = String.init(format: "%0.2f (%0.2f%@)", obj.diff, obj.perChange, "%")
        if obj.chart.count != 0 {
            self.setupchart(data: obj.chart)
        }
    }
    
    func setupchart(data: [HomeChartsChart]) {
        self.ChartView.data = self.setDataCount(chart: data)

        self.ChartView.chartDescription?.enabled = false
        self.ChartView.dragEnabled = true
        self.ChartView.setScaleEnabled(false)
        self.ChartView.pinchZoomEnabled = false
        self.ChartView.xAxis.gridColor = .white

        self.ChartView.leftAxis.removeAllLimitLines()
        self.ChartView.leftAxis.enabled = false
        self.ChartView.rightAxis.enabled = false
        self.ChartView.gridBackgroundColor = .clear
        self.ChartView.drawGridBackgroundEnabled = false

        self.ChartView.legend.form = .none

        for set in self.ChartView.data!.dataSets as! [LineChartDataSet] {
            set.mode = (set.mode == .cubicBezier) ? .horizontalBezier : .cubicBezier
        }
        self.ChartView.setNeedsDisplay()

        self.ChartView.animate(xAxisDuration: 1)
    }
    
    func setDataCount(chart: [HomeChartsChart]) -> ChartData {
        var values = [ChartDataEntry]()
        
        for obj in chart {
            if obj.stockin != 0.1 && obj.stockout != 0.1 {
                let value = ChartDataEntry(x: obj.timeStamp == nil ? 0 : obj.timeStamp, y: obj.stockin == nil ? 0 :obj.stockin)
                values.append(value)
            }
        }
        
        let set1 = LineChartDataSet(entries: values, label: "")
        set1.drawIconsEnabled = false
        
        set1.lineDashLengths = [0, 0]
        set1.highlightLineDashLengths = [0, 0]
        set1.setColor(UIColor.clear)
        set1.setCircleColor(UIColor.clear)
        set1.lineWidth = 0
        set1.circleRadius = 0
        set1.drawCircleHoleEnabled = false
        set1.valueFont = Font.MontserratBoldFont(font: 12)
        set1.valueTextColor = UIColor.clear
        set1.formLineDashLengths = [0, 0]
        set1.formLineWidth = 0
        set1.formSize = 0
        
        let gradientColors = [DarkBlue.withAlphaComponent(0.1).cgColor,
                              DarkBlue.withAlphaComponent(0.8).cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        let data = LineChartData(dataSet: set1)
        
        return data
    }
    
    @IBAction func TappedStrach(_ sender: UIButton) {
        self.StrachactionHandler?(sender.tag)
    }
    
    @IBAction func TappedCoin(_ sender: UIButton) {
        self.CoinactionHandler?(sender.tag)
    }
    
    
}

class Env {
    static var iPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
}

struct ChartDefaultsSetting {
    
    static var chartSettings: ChartSettings {
        if Env.iPad {
            return iPadChartSettings
        } else {
            return iPhoneChartSettings
        }
    }

    static var chartSettingsWithPanZoom: ChartSettings {
        if Env.iPad {
            return iPadChartSettingsWithPanZoom
        } else {
            return iPhoneChartSettingsWithPanZoom
        }
    }
    
    fileprivate static var iPadChartSettings: ChartSettings {
        var chartSettings = ChartSettings()
        chartSettings.leading = 20
        chartSettings.top = 20
        chartSettings.trailing = 20
        chartSettings.bottom = 20
        chartSettings.labelsToAxisSpacingX = 10
        chartSettings.labelsToAxisSpacingY = 10
        chartSettings.axisTitleLabelsToLabelsSpacing = 5
        chartSettings.axisStrokeWidth = 1
        chartSettings.spacingBetweenAxesX = 15
        chartSettings.spacingBetweenAxesY = 15
        chartSettings.labelsSpacing = 0
        return chartSettings
    }
    
    fileprivate static var iPhoneChartSettings: ChartSettings {
        var chartSettings = ChartSettings()
        chartSettings.leading = 0
        chartSettings.top = 0
        chartSettings.trailing = 0
        chartSettings.bottom = 0
        chartSettings.labelsToAxisSpacingX = 0
        chartSettings.labelsToAxisSpacingY = 0
        chartSettings.axisTitleLabelsToLabelsSpacing = 0
        chartSettings.axisStrokeWidth = 0.2
        chartSettings.spacingBetweenAxesX = 0
        chartSettings.spacingBetweenAxesY = 0
        chartSettings.labelsSpacing = 0
        return chartSettings
    }

    fileprivate static var iPadChartSettingsWithPanZoom: ChartSettings {
        var chartSettings = iPadChartSettings
        chartSettings.zoomPan.panEnabled = true
        chartSettings.zoomPan.zoomEnabled = true
        return chartSettings
    }

    fileprivate static var iPhoneChartSettingsWithPanZoom: ChartSettings {
        var chartSettings = iPhoneChartSettings
        chartSettings.zoomPan.panEnabled = true
        chartSettings.zoomPan.zoomEnabled = true
        return chartSettings
    }
    
    static func chartFrame(_ containerBounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: containerBounds.size.width, height: containerBounds.size.height)
    }
    
    static var labelSettings: ChartLabelSettings {
        return ChartLabelSettings(font: ChartDefaultsSetting.labelFont)
    }
    
    static var labelFont: UIFont {
        return ChartDefaultsSetting.fontWithSize(Env.iPad ? 14 : 11)
    }
    
    static var labelFontSmall: UIFont {
        return ChartDefaultsSetting.fontWithSize(Env.iPad ? 12 : 10)
    }
    
    static func fontWithSize(_ size: CGFloat) -> UIFont {
        return Font.MontserratRegularFont(font: size)
    }
    
    static var guidelinesWidth: CGFloat {
        return Env.iPad ? 0.5 : 0.1
    }
    
    static var minBarSpacing: CGFloat {
        return Env.iPad ? 10 : 5
    }
}
