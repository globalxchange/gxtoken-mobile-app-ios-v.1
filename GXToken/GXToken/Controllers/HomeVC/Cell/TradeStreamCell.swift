//
//  GXTokenCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 12/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class GXTokenCell: UITableViewCell {

    @IBOutlet weak var CoinImage: UIImageView!
    @IBOutlet weak var Subview: UIView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var seprator: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.Subview.backgroundColor = ViewBGColor
        
        self.title.font = Font.MontserratRegularFont(font: 15)
        self.title.textColor = DarkBlue
        
        self.price.font = Font.MontserratRegularFont(font: 15)
        self.price.textColor = DarkBlue
        
        self.seprator.backgroundColor = SepratorColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func TilesTheme() {
        self.contentView.backgroundColor = .clear
        self.backgroundColor = .clear
        self.Subview.backgroundColor = ViewBGColor
        self.Subview.layer.cornerRadius = 10
        self.Subview.layer.borderColor = LightGroup.cgColor
        self.Subview.layer.borderWidth = 1
        self.Subview.layer.borderColor = LightGroup.cgColor
        self.Subview.layer.shadowColor = LightGroup.cgColor
        self.Subview.layer.shadowRadius = 1
        self.Subview.layer.shadowOpacity = 0.4
        self.Subview.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.Subview.clipsToBounds = true
    }
    
}
