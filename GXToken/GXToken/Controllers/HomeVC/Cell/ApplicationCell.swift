//
//  ApplicationCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 13/08/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class ApplicationCell: UICollectionViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.title.font = Font.MontserratRegularFont(font: 10)
        self.title.textColor = ShadowBlue
        
        self.view.backgroundColor = ViewBGColor
        self.icon.tintColor = ShadowDarkBlue
    }

}
