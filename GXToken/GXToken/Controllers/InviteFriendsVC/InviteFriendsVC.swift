//
//  InviteFriendsVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 09/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class InviteFriendsVC: BaseAppVC {

    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    @IBOutlet weak var SettingBTN: UIButton!
    
    @IBOutlet weak var MainSubview: UIView!
    
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var ListTBL: UITableView!
    
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.SetupUI()
    }

    // MARK:- User define methods
    
    func SetupUI() {
        self.MainSubview.backgroundColor = ViewBGColor
        
        self.ListTBL.register(UINib.init(nibName: "InviteCell", bundle: nil), forCellReuseIdentifier: "InviteCell")
        
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.ListTBL.frame.width, height: 30))
        footer.backgroundColor = ViewBGColor
        self.ListTBL.tableFooterView = footer
        self.ListTBL.delegate = self
        self.ListTBL.dataSource = self
        self.ListTBL.reloadData()
        
        self.TitleLBL.textAlignment = .center
        let title = "Your Network Is Your Net-Worth\n\n"
        let detail = "Pick One Of The Following Options"
        let mainstring: String = title + detail
        let myMutableString = mainstring.Attributestring(attribute: [(title, Font.MontserratRegularFont(font: 25.0), DarkBlue), (detail, Font.MontserratRegularFont(font: 14.0), DarkBlue)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [title, detail])))
        self.TitleLBL.attributedText = myMutableString
        
    }
    // MARK:- API Calling

    // MARK:- IBAction Methods
    
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        if sender == self.MenuBTN {
//            let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
//            Menu.OptionSelection = "pulse"
//            self.navigationController?.pushViewController(Menu, animated: true)
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
            let vc = UINavigationController(rootViewController: setting)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }

}

extension InviteFriendsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        view.backgroundColor = ViewBGColor
        
        let TitleLBL = UILabel.init(frame: CGRect.init(x: 30, y: 0, width: view.frame.width, height: 30))
        
        if section == 0 {
            TitleLBL.textColor = DarkBlue
            TitleLBL.font = Font.MontserratRegularFont(font: 15)
            TitleLBL.text = "Mobile First"
        }
        else {
            TitleLBL.textColor = DarkBlue
            TitleLBL.font = Font.MontserratRegularFont(font: 15)
            TitleLBL.text = "Web"
        }
        view.addSubview(TitleLBL)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: InviteCell = tableView.dequeueReusableCell(withIdentifier: "InviteCell") as! InviteCell
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.setupView(img: "IC_Apple_Invite", str1: "Directly To\n", str2: "Their Iphone", detail: "This will get them to download the GXToken Terminal onto their Iphone and register within your network from the app.")
            }
            else {
                cell.setupView(img: "IC_Android_Invite", str1: "Directly To\n", str2: "Their Android", detail: "This will get them to download the GXToken Terminal  onto their Android and register within your network from the app.")
            }
        }
        else {
            if indexPath.row == 0 {
                cell.setupView(img: "IC_Desktop_Invite", str1: "Browser Based\n", str2: "Desktop Signup", detail: "Get them to register within your network by completing a simple form in any internet browser from their laptop or desktop.")
            }
            else {
                cell.setupView(img: "IC_Mobile_Invite", str1: "Browser Based\n", str2: "Mobile Signup", detail: "Get them to register within your network by completing a simple form in any internet browser from their phone.")
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let method = InvitationMethodVC.init(nibName: "InvitationMethodVC", bundle: nil)
            let vc = UINavigationController(rootViewController: method)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
        else {
            
        }
    }
    
}
