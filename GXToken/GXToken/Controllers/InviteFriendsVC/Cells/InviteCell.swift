//
//  InviteCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 09/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class InviteCell: UITableViewCell {

    @IBOutlet weak var View: UIView!
    @IBOutlet weak var Pic_IMG: UIImageView!
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var DetailLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = ViewBGColor
        self.View.backgroundColor = ViewBGColor
        
        self.View.clipsToBounds = true
        self.View.layer.cornerRadius = 10
        self.View.layer.borderColor = DarkBlue.withAlphaComponent(0.5).cgColor
        self.View.layer.borderWidth = 0.5
        
        self.DetailLBL.font = Font.MontserratRegularFont(font: 12)
        self.DetailLBL.textColor = DarkBlue
    }
    
    func setupView(img: String, str1: String, str2: String, detail: String) {
        self.Pic_IMG.image = UIImage.init(named: img)
        
        let mainstring: String = str1 + str2
        let myMutableString = mainstring.Attributestring(attribute: [(str1, Font.MontserratRegularFont(font: 13), DarkBlue), (str2, Font.MontserratRegularFont(font: 18), DarkBlue)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1, str2])))
        self.TitleLBL.attributedText = myMutableString
        
        self.DetailLBL.text = detail
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
