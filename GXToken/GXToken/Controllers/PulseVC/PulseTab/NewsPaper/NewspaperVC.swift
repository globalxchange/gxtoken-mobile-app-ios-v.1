//
//  NewspaperVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class NewspaperVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    @IBOutlet weak var SettingBTN: UIButton!
    
    @IBOutlet weak var MainSubview: UIView!
    @IBOutlet weak var ListTBL: UITableView!
    
    // MARK:- Variable Defines
    
    var Article_arry : [PulseArticleData] = []
    var Header_arry : [PulseArticleData] = []
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.nodatafound.frame = self.MainSubview.frame
        self.nodatafound.didActionBlock = {
            self.GetHeaderList()
        }
        self.MainSubview.addSubview(self.nodatafound)
        self.GetHeaderList()
        self.SetupUI()
    }

    // MARK:- User define methods
    
    func SetupUI() {
        self.MainSubview.backgroundColor = ViewBGColor
        self.ListTBL.backgroundColor = ViewBGColor
        
        self.ListTBL.register(UINib.init(nibName: "PulseHeaderCell", bundle: nil), forCellReuseIdentifier: "PulseHeaderCell")
        self.ListTBL.register(UINib.init(nibName: "TechnologyCell", bundle: nil), forCellReuseIdentifier: "TechnologyCell")
        
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.ListTBL.frame.width, height: 30))
        footer.backgroundColor = ViewBGColor
        self.ListTBL.tableFooterView = footer
    }
    
    // MARK:- API Calling
    func GetHeaderList() {
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Article_Header.value()) { (responseObject) in
            let root = PulseArticleRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status {
                self.Header_arry = root.data
                self.GetNewsList()
            }
            else {
                self.ListTBL.isHidden = true
                self.nodatafound.isHidden = false
            }
        } onError: { (message, code) in
            self.ListTBL.isHidden = true
            self.nodatafound.isHidden = false
        }
    }
    
    func GetNewsList() {
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Article_Navbar.value()) { (responseObject) in
            let root = PulseArticleRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status {
                self.Article_arry = root.data
                
                self.ListTBL.isHidden = false
                self.ListTBL.delegate = self
                self.ListTBL.dataSource = self
                self.ListTBL.reloadData()
                self.nodatafound.isHidden = true
            }
            else {
                self.ListTBL.isHidden = true
                self.nodatafound.isHidden = false
            }
        } onError: { (message, code) in
            self.ListTBL.isHidden = true
            self.nodatafound.isHidden = false
        }
    }

    // MARK:- IBAction Methods
    
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        if sender == self.MenuBTN {
            let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
            Menu.OptionSelection = "pulse"
            self.navigationController?.pushViewController(Menu, animated: true)
        }
        else {
            let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
            let vc = UINavigationController(rootViewController: setting)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }
}

extension NewspaperVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.Header_arry.count == 0 ? 1 : self.Header_arry.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HeaderDetailCell", for: indexPath) as! HeaderDetailCell
        
        if self.Header_arry.count == 0 {
            cell.titleLBL.text = "It Seems That We Have Reached The End Of The News Cycle"
            cell.TypeLBL.text = "By: GX"
            cell.DateLBL.text = "New News Coming Soon"
            cell.pulseIMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
            cell.pulseIMG.tintColor = DarkBlue
        }
        else {
            let data = self.Header_arry[indexPath.row]
            cell.titleLBL.text = data.title
            cell.TypeLBL.text = "Paragraph.info"
            cell.DateLBL.text = data.createdAt
            cell.pulseIMG.downloadedFrom(link: data.icon, contentMode: .scaleAspectFill, radious: 10)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = NewsPaperDetailVC.init(nibName: "NewsPaperDetailVC", bundle: nil)
        vc.Detaildata = self.Header_arry[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 1.4, height: collectionView.frame.height)
    }
    
}

extension NewspaperVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : self.Article_arry.count == 0 ? 1 : self.Article_arry.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        view.backgroundColor = ViewBGColor
        
        let TitleLBL = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: view.frame.width, height: 30))
        
        if section == 0 {
            TitleLBL.textColor = DarkBlue
            TitleLBL.font = Font.MontserratRegularFont(font: 30)
            TitleLBL.text = "The Pulse Of GXT"
        }
        else {
            TitleLBL.textColor = DarkBlue
            TitleLBL.font = Font.MontserratRegularFont(font: 20)
            TitleLBL.text = self.Article_arry.count == 0 ? "Latest News" : "Technology"
        }
        view.addSubview(TitleLBL)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: PulseHeaderCell = tableView.dequeueReusableCell(withIdentifier: "PulseHeaderCell") as! PulseHeaderCell
            
            cell.ListTBL.register(UINib.init(nibName: "HeaderDetailCell", bundle: nil), forCellWithReuseIdentifier: "HeaderDetailCell")
            
            cell.ListTBL.delegate = self
            cell.ListTBL.dataSource = self
            cell.ListTBL.reloadData()
            
            cell.Height.constant = self.MainSubview.frame.height * 0.30
            return cell
        }
        else {
            let cell: TechnologyCell = tableView.dequeueReusableCell(withIdentifier: "TechnologyCell") as! TechnologyCell
            
            if self.Article_arry.count == 0 {
                cell.titleLBL.text = "It Seems That We Have Reached The End Of The News Cycle"
                cell.TypeLBL.text = "By: GX"
                cell.DateLBL.text = "New News Coming Soon"
                cell.technologyIMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "colouredicon")
                cell.technologyIMG.tintColor = .white
                cell.technologyIMG.backgroundColor = DarkBlue
            }
            else {
                let data = self.Article_arry[indexPath.row]
                cell.titleLBL.text = data.title
                cell.TypeLBL.text = "The Guardian"
                cell.DateLBL.text = data.createdAt
                cell.technologyIMG.downloadedFrom(link: data.icon, contentMode: .scaleAspectFill, radious: 10)
                cell.technologyIMG.tintColor = DarkBlue
                cell.technologyIMG.backgroundColor = .white
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            
        }
        else {
            
        }
    }
    
}
