//
//  NewsPaperDetailVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class NewsPaperDetailVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    
    @IBOutlet weak var Scrollview: UIScrollView!
    
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var bannerIMG: UIImageView!
    @IBOutlet weak var NameLBL: UILabel!
    @IBOutlet weak var detailLBL: UILabel!
    @IBOutlet weak var Line: UILabel!
    @IBOutlet weak var DescriptionLBL: UILabel!
    @IBOutlet weak var BottomIMG: UIImageView!
    
    // MARK:- Variable define
    var Detaildata = PulseArticleData.init(fromDictionary: NSDictionary.init())
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.SetupUI()
    }

    // MARK:- User define methods
    
    func SetupUI() {
        self.TitleLBL.font = Font.MontserratRegularFont(font: 22)
        self.TitleLBL.textColor = DarkBlue
        self.TitleLBL.text = self.Detaildata.title
        
        self.bannerIMG.downloadedFrom(link: self.Detaildata.icon, contentMode: .scaleAspectFit, radious: 0)
        
        self.NameLBL.font = Font.MontserratRegularFont(font: 14)
        self.NameLBL.textColor = .black
        self.NameLBL.text = "By Jessie Evans"
        
        self.detailLBL.font = Font.MontserratRegularFont(font: 14)
        self.detailLBL.textColor = .darkGray
        self.detailLBL.text = "April 12, 2020 - 09:45"
        
        self.Line.backgroundColor = .lightGray
        
//        let string1 = "How well do they work? \n\n"
//        let string2 = "Six weeks ago few of us had heard of contact tracing. But on Friday two of the world's most powerful companies announced a collaboration on technology to work out where patients might have picked up coronavirus, or who else they may have infected. "
//
//        let mainstring: String = string1 + string2
//        let myMutableString = mainstring.Attributestring(attribute: [
//            (string1, Font.MontserratRegularFont(font: 18), DarkBlue),
//            (string2, Font.MontserratSemiBoldFont(font: 14.0), .black)
//        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1 ,string2])))
        self.DescriptionLBL.text = self.Detaildata.desc
        
        self.BottomIMG.downloadedFrom(link: self.Detaildata.media, contentMode: .scaleAspectFill, radious: 10)
        
        
    }
    
    // MARK:- API Calling

    // MARK:- IBAction Methods
    
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}
