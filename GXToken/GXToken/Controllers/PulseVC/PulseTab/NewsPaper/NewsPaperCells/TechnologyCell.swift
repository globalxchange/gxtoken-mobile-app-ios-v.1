//
//  TechnologyCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 30/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class TechnologyCell: UITableViewCell {

    @IBOutlet weak var technologyIMG: UIImageView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var TypeLBL: UILabel!
    @IBOutlet weak var DateLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.technologyIMG.clipsToBounds = true
        self.technologyIMG.layer.cornerRadius = 10
        
        self.titleLBL.textColor = ShadowDarkBlue
        self.titleLBL.font = Font.MontserratRegularFont(font: 23)
        
        self.TypeLBL.textColor = ShadowDarkBlue
        self.TypeLBL.font = Font.MontserratRegularFont(font: 12)
        
        self.DateLBL.textColor = .lightGray
        self.DateLBL.font = Font.MontserratRegularFont(font: 12)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
