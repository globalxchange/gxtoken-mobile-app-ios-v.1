//
//  HeaderDetailCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 30/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class HeaderDetailCell: UICollectionViewCell {

    @IBOutlet weak var pulseIMG: UIImageView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var TypeLBL: UILabel!
    @IBOutlet weak var DateLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.pulseIMG.clipsToBounds = true
        self.pulseIMG.layer.cornerRadius = 10
        
        self.titleLBL.textColor = ShadowDarkBlue
        self.titleLBL.font = Font.MontserratRegularFont(font: 22)
        
        self.TypeLBL.textColor = ShadowDarkBlue
        self.TypeLBL.font = Font.MontserratRegularFont(font: 12)
        
        self.DateLBL.textColor = .lightGray
        self.DateLBL.font = Font.MontserratRegularFont(font: 12)
    }

}
