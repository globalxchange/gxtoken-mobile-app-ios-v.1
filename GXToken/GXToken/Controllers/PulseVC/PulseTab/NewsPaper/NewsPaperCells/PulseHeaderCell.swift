//
//  PulseHeaderCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 30/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class PulseHeaderCell: UITableViewCell {

    @IBOutlet weak var ListTBL: UICollectionView!
    @IBOutlet weak var Height: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.ListTBL.backgroundColor = ViewBGColor
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
