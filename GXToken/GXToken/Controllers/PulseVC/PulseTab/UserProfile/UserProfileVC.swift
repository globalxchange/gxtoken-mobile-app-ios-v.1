//
//  UserProfileVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class UserProfileVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    @IBOutlet weak var SettingBTN: UIButton!
    
    @IBOutlet weak var MainSubview: UIView!
    
    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var Pro_backIMG: UIImageView!
    @IBOutlet weak var ProinfoView: UIView!
    @IBOutlet weak var ProfileIMG: UIImageView!
    @IBOutlet weak var ProfileTitlelbl: UILabel!
    @IBOutlet var SepratorLBL: [UILabel]!
    
    @IBOutlet weak var PictureView: UIView!
    @IBOutlet weak var ViewsBTN: UIButton!
    @IBOutlet weak var ViewallBTN: UIButton!
    @IBOutlet weak var PictureList: UICollectionView!
    
    @IBOutlet weak var ListTBL: UITableView!
    @IBOutlet weak var ComposeBTN: UIButton!
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.SetupUI()
    }

    // MARK:- User define methods
    
    func SetupUI() {
        self.MainSubview.backgroundColor = ViewBGColor
        self.ListTBL.backgroundColor = ViewBGColor
        self.PictureList.backgroundColor = ViewBGColor
        
        self.SepratorLBL[0].backgroundColor = SepratorColor
        self.SepratorLBL[1].backgroundColor = SepratorColor
        
        self.ProfileView.backgroundColor = ViewBGColor
        self.Pro_backIMG.image = UIImage.init(named: "ProfileBack")
        self.ProinfoView.backgroundColor = ViewBGColor
        self.ProfileIMG.clipsToBounds = true
        self.ProfileIMG.layer.cornerRadius = self.ProfileIMG.frame.height / 2
        self.ProfileIMG.layer.borderWidth = 2
        self.ProfileIMG.layer.borderColor = ViewBGColor.cgColor
        
        let string1 = "Gregory Edwards \n"
        let string2 = " UI / UX Designer | Photographer | DC Comics"
        
        let mainstring: String = string1 + string2
        let myMutableString = mainstring.Attributestring(attribute: [
            (string1, Font.MontserratRegularFont(font: 20), .black),
            (string2, Font.MontserratSemiBoldFont(font: 12.0), LightGray)
        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1 ,string2])))
        self.ProfileTitlelbl.attributedText = myMutableString
        
        self.PictureView.backgroundColor = ViewBGColor
        self.ViewsBTN.setTitle("1234 views", for: .normal)
        self.ViewsBTN.setTitleColor(LightGray, for: .normal)
        self.ViewsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 12)
        
        self.ViewallBTN.setTitle("View All", for: .normal)
        self.ViewallBTN.setTitleColor(LightGray, for: .normal)
        self.ViewallBTN.titleLabel?.font = Font.MontserratRegularFont(font: 12)
        
        self.PictureList.register(UINib.init(nibName: "VideoFCell", bundle: nil), forCellWithReuseIdentifier: "VideoFCell")
        self.PictureList.isHidden = false
        
        self.PictureList.delegate = self
        self.PictureList.dataSource = self
        self.PictureList.reloadData()
        self.PictureList.backgroundColor = ViewBGColor
        
        self.ListTBL.register(UINib.init(nibName: "UserprofileListCell", bundle: nil), forCellReuseIdentifier: "UserprofileListCell")
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.ListTBL.frame.width, height: 40))
        footer.backgroundColor = ViewBGColor
        self.ListTBL.tableFooterView = footer
        self.ListTBL.delegate = self
        self.ListTBL.dataSource = self
        self.ListTBL.reloadData()
        
        self.ComposeBTN.backgroundColor = DarkBlue
        self.ComposeBTN.clipsToBounds = true
        self.ComposeBTN.layer.cornerRadius = self.ComposeBTN.frame.height / 2
    }
    
    // MARK:- API Calling

    // MARK:- IBAction Methods
    
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        if sender == self.MenuBTN {
            let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
            Menu.OptionSelection = "pulse"
            self.navigationController?.pushViewController(Menu, animated: true)
        }
        else {
            let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
            let vc = UINavigationController(rootViewController: setting)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }

    @IBAction func TappedPicture(_ sender: UIButton) {
    }
    
    @IBAction func TappedCompose(_ sender: Any) {
        let vc = ComposeVC.init(nibName: "ComposeVC", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension UserProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoFCell", for: indexPath) as! VideoFCell
        let img = indexPath.row == 0 ? "Vector" : indexPath.row == 1 ? "Vector2" : indexPath.row == 2 ? "Vector3" : indexPath.row == 3 ? "Vector" : "Vector2"
        let title = indexPath.row == 0 ? "Clark Kent" : indexPath.row == 1 ? "Bruce Wayne" : indexPath.row == 2 ? "Arthur Curry" : indexPath.row == 3 ? "Princess Dayana" : "Other"
        cell.pictureIMG.image = UIImage.init(named: img)
        cell.titleLBL.text = title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 1.4, height: collectionView.frame.height)
    }
    
}

extension UserProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UserprofileListCell = tableView.dequeueReusableCell(withIdentifier: "UserprofileListCell") as! UserprofileListCell
        cell.SetupProfileName(string1: "Gregory Edwards \n", string2: "26 Jun 2019 at12:15 PM")
        cell.NoteLBL.text = "Another day, another milestone achieved!"
        cell.likedetaillbl.text = "Norma Murphy & 99 others"
        cell.MenuactionHandler = { (tag) in
            
        }
        cell.LikeactionHandler = { (tag) in
            
        }
        cell.CommentactionHandler = { (tag) in
            
        }
        cell.ShareactionHandler = { (tag) in
            AppUtillity.shared.ShareTools(sender: cell.ShareBTN, vc: self)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
