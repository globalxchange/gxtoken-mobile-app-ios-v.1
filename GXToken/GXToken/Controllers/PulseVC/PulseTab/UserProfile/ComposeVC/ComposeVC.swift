//
//  ComposeVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 24/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class ComposeVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBack: UIButton!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    @IBOutlet weak var SettingBTN: UIButton!
    
    @IBOutlet weak var MainSubview: UIView!
    
    @IBOutlet weak var Optionview: UIView!
    @IBOutlet weak var OptionHeight: NSLayoutConstraint!
    @IBOutlet weak var PostsBTN: UIButton!
    @IBOutlet weak var ArticleBTN: UIButton!
    @IBOutlet weak var VideoBTN: UIButton!
    @IBOutlet weak var StreamBTN: UIButton!
    
    @IBOutlet weak var ComposeAreaView: UIView!
    @IBOutlet weak var AttachmentTBL: UICollectionView!
    
    @IBOutlet weak var TXTCompose: UITextView!
    
    @IBOutlet weak var BottomView: UIView!
    @IBOutlet var seprator: [UILabel]!
    @IBOutlet weak var PreviewBTN: UIButton!
    @IBOutlet weak var BottomOptionTBL: UICollectionView!
    
    @IBOutlet weak var PublishVIew: UIView!
    @IBOutlet weak var PublishBackBTN: UIButton!
    @IBOutlet weak var PublishBTN: UIButton!
    
    @IBOutlet weak var UploadOptionView: UIView!
    @IBOutlet weak var uploadoption_height: NSLayoutConstraint!
    @IBOutlet weak var AttachmentOptionTBL: UITableView!
    
    // MARK:- Variable Define
    var placeholderLabel : UILabel!
    var bottom_arry : [CatAppsApp] = []
    var SelectedBottom: CatAppsApp!
    var UploadingArry: [Any] = []
    var previewMode: Bool = false
    var UploadViewHeight: CGFloat = 0.0
    
    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.GetbottomList()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillShow),
            name: UIResponder.keyboardWillShowNotification,
            object: nil
        )
    }
    
    // MARK:- User define methods
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.UploadViewHeight = keyboardHeight
        }
    }
    
    func SetupUI() {
        self.MainSubview.backgroundColor = ViewBGColor
        self.Optionview.backgroundColor = ViewBGColor
        self.AttachmentTBL.backgroundColor = ViewBGColor
        
        self.Optionview.isHidden = false
        self.ComposeAreaView.isHidden = false
        
        self.PostsBTN.setTitle("Posts", for: .normal)
        self.PostsBTN.setTitleColor(DarkBlue, for: .normal)
        self.PostsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
        
        self.ArticleBTN.setTitle("Articles", for: .normal)
        self.ArticleBTN.setTitleColor(DarkBlue, for: .normal)
        self.ArticleBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
        
        self.VideoBTN.setTitle("Videos", for: .normal)
        self.VideoBTN.setTitleColor(DarkBlue, for: .normal)
        self.VideoBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
        
        self.StreamBTN.setTitle("Stream", for: .normal)
        self.StreamBTN.setTitleColor(DarkBlue, for: .normal)
        self.StreamBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
        
        self.selectedOption()
        
        self.AttachmentTBL.register(UINib.init(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: "AttachmentCell")
        self.AttachmentTBL.delegate = self
        self.AttachmentTBL.dataSource = self
        self.AttachmentTBL.reloadData()
        
        self.ComposeAreaView.backgroundColor = ViewBGColor
        
        self.TXTCompose.delegate = self
        self.TXTCompose.backgroundColor = ViewBGColor
        self.placeholderLabel = UILabel()
        self.placeholderLabel.lineBreakMode = .byWordWrapping
        self.placeholderLabel.numberOfLines = 2
        
        let string1 = "Enter Title \n"
        let string2 = "Enter The Date"
        
        let mainstring: String = string1 + string2
        let myMutableString = mainstring.Attributestring(attribute: [
            (string1, Font.MontserratRegularFont(font: 25), UIColor.colorWithHexString(hexStr: "#B7BECD")),
            (string2, Font.MontserratSemiBoldFont(font: 9.0), UIColor.colorWithHexString(hexStr: "#737374"))
        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1 ,string2])))
        self.placeholderLabel.attributedText = myMutableString
        self.placeholderLabel.sizeToFit()
        self.TXTCompose.addSubview(self.placeholderLabel)
        self.TXTCompose.font = Font.MontserratRegularFont(font: 25)
        self.placeholderLabel.frame.origin = CGPoint(x: 5, y: (self.TXTCompose.font?.pointSize)! / 2)
        self.placeholderLabel.textColor = UIColor.lightGray
        self.placeholderLabel.isHidden = !self.TXTCompose.text.isEmpty
        
        self.BottomView.backgroundColor = ViewBGColor
        
        self.seprator[0].backgroundColor = SepratorColor
        self.seprator[1].backgroundColor = SepratorColor
        self.seprator[2].backgroundColor = SepratorColor
        
        self.PreviewBTN.setTitle("Preview", for: .normal)
        self.PreviewBTN.setTitleColor(.black, for: .normal)
        self.PreviewBTN.titleLabel?.font = Font.MontserratSemiBoldFont(font: 17)
        self.PreviewBTN.setBackgroundImage(UIImage.init(named: ""), for: .normal)
        self.PreviewBTN.tintColor = .clear
        
        self.BottomOptionTBL.backgroundColor = ViewBGColor
        self.BottomOptionTBL.register(UINib.init(nibName: "BottomOptionCell", bundle: nil), forCellWithReuseIdentifier: "BottomOptionCell")
        
        self.PublishVIew.backgroundColor = DarkBlue
        self.PublishVIew.isHidden = true
        
        self.UploadOptionView.backgroundColor = ViewBGColor
        self.uploadoption_height.constant = isXseries ? 34 : 0
        self.UploadOptionView.isHidden = true
        
        self.AttachmentOptionTBL.register(UINib.init(nibName: "AttachmentOptionCell", bundle: nil), forCellReuseIdentifier: "AttachmentOptionCell")
        self.AttachmentOptionTBL.delegate = self
        self.AttachmentOptionTBL.dataSource = self
        self.AttachmentOptionTBL.reloadData()
    }
    
    func selectedOption(index: Int = 1) {
        switch index {
        case 0:
            self.PostsBTN.titleLabel?.font = Font.MontserratBoldFont(font: 17)
            self.ArticleBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.VideoBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.StreamBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            
        case 1:
            self.PostsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.ArticleBTN.titleLabel?.font = Font.MontserratBoldFont(font: 17)
            self.VideoBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.StreamBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            KeyboardAvoiding.avoidingView = self.BottomView
            self.TXTCompose.becomeFirstResponder()
            
        case 2:
            self.PostsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.ArticleBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.VideoBTN.titleLabel?.font = Font.MontserratBoldFont(font: 17)
            self.StreamBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            
        case 3:
            self.PostsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.ArticleBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.VideoBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.StreamBTN.titleLabel?.font = Font.MontserratBoldFont(font: 17)
            
        default:
            self.PostsBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.ArticleBTN.titleLabel?.font = Font.MontserratBoldFont(font: 17)
            self.VideoBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
            self.StreamBTN.titleLabel?.font = Font.MontserratRegularFont(font: 17)
        }
    }
    
    func previewmode(status: Bool) {
        if status {
            self.view.layoutIfNeeded() // force any pending operations to finish
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.OptionHeight.constant = 0
                self.view.layoutIfNeeded()
            })
            self.PreviewBTN.setTitleColor(.clear, for: .normal)
            self.PreviewBTN.setBackgroundImage(UIImage.init(named: "IC_Keyborad"), for: .normal)
            self.PreviewBTN.isSelected = true
            self.PublishVIew.isHidden = false
            self.BottomView.isHidden = true
            self.TXTCompose.isEditable = false
            self.previewMode = true
            self.TXTCompose.text = "Introducing Defi\nSat July 4th 10:21 PM\n\nSEF which stands for “Save Exchange Fees Coin” is the official cryptocurrency of the InstaCrypto platform. Keeping a balance of SEF allows you to avoid all fees charged by the platform\n\nNote: SEF Does Not Eleviate You From Fees Charged By The Bankers..\n\nSEF which stands for “Save Exchange Fees Coin” is the official cryptocurrency of the InstaCrypto platform. Keeping a balance of SEF allows you to avoid all fees charged by the platform\n\nNote: SEF Does Not Eleviate You From Fees Charged By The Bankers..\n\nSEF which stands for “Save Exchange Fees Coin” is the official cryptocurrency of the InstaCrypto platform. Keeping a balance of SEF allows you to avoid all fees charged by the platform\n\nNote: SEF Does Not Eleviate You From Fees Charged By The Bankers..\n\n"
            self.TXTCompose.resignFirstResponder()
        }
        else {
            self.view.layoutIfNeeded() // force any pending operations to finish
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.OptionHeight.constant = 50
                self.uploadoption_height.constant = isXseries ? 34 : 0
                self.UploadOptionView.isHidden = true
                self.view.layoutIfNeeded()
            })
            self.PreviewBTN.setTitleColor(.black, for: .normal)
            self.PreviewBTN.setBackgroundImage(UIImage.init(named: ""), for: .normal)
            self.PreviewBTN.isSelected = false
            self.PublishVIew.isHidden = true
            self.BottomView.isHidden = false
            self.TXTCompose.isEditable = true
            self.previewMode = false
            self.TXTCompose.becomeFirstResponder()
        }
        self.placeholderLabel.isHidden = !self.TXTCompose.text.isEmpty
        self.AttachmentTBL.reloadData()
    }
    
    // MARK:- API Calling
    func GetbottomList() {
        ApiLoader.shared.showHUD()
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Cat_Applist.value()) { (responseObject) in
            let root = CatAppsRootClass.init(fromDictionary: responseObject as NSDictionary)
            self.SetupUI()
            if root.status && root.count >= 1 {
                self.bottom_arry = root.apps
                self.BottomOptionTBL.delegate = self
                self.BottomOptionTBL.dataSource = self
                self.BottomOptionTBL.reloadData()
                self.BottomView.isHidden = false
            }
            else {
                self.BottomView.isHidden = true
            }
            ApiLoader.shared.stopHUD()
        } onError: { (message, code) in
            self.nodatafound.frame = CGRect.init(x: self.MainSubview.frame.origin.x, y: self.MainSubview.frame.origin.y, width: self.MainSubview.frame.width, height: self.MainSubview.frame.height)
            self.nodatafound.didActionBlock = {
                self.GetbottomList()
            }
            self.MainSubview.addSubview(self.nodatafound)
            ApiLoader.shared.stopHUD()
        }
    }
    
    // MARK:- IBAction Methods
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        if sender == self.MenuBTN {
            let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
            Menu.OptionSelection = "pulse"
            self.navigationController?.pushViewController(Menu, animated: true)
        }
        else if sender == self.MenuBack {
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
            let vc = UINavigationController(rootViewController: setting)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func TappedToolsoption(_ sender: UIButton) {
        if sender == self.PostsBTN {
            self.selectedOption(index: 0)
        }
        else if sender == self.ArticleBTN {
            self.selectedOption(index: 1)
        }
        else if sender == self.VideoBTN {
            self.selectedOption(index: 2)
        }
        else if sender == self.StreamBTN {
            self.selectedOption(index: 3)
        }
        else if sender == self.PreviewBTN {
            if sender.isSelected {
                self.previewmode(status: false)
            }
            else {
                self.previewmode(status: true)
            }
        }
        else {
            
        }
    }
    
    @IBAction func TappedPublish(_ sender: UIButton) {
        if self.PublishBackBTN == sender {
            self.previewmode(status: false)
        }
        else {
            let pulish = PublishVC.init(nibName: "PublishVC", bundle: nil)
            let vc = UINavigationController(rootViewController: pulish)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }
    
}

extension ComposeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.AttachmentTBL {
            return self.previewMode == true ? self.UploadingArry.count : self.PreviewBTN.isSelected == true ? (self.UploadingArry.count == 0 ? self.UploadingArry.count + 1 : self.UploadingArry.count) : self.UploadingArry.count + 1
        }
        else {
            return self.bottom_arry.count
        }
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.AttachmentTBL {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AttachmentCell", for: indexPath) as! AttachmentCell
            if indexPath.row == self.UploadingArry.count || self.UploadingArry.count == 0 {
                cell.uploadIMG.image = UIImage.init(named: "IC_Upload")
                cell.uploadIMG.isHidden = false
                cell.cellimg.isHidden = true
                cell.backgroundColor = LightGray
            }
            else {
                let item = self.UploadingArry[indexPath.row] as! AttachmentObject
                if item.type == .Image {
                    cell.cellimg.image = item.item as? UIImage
                }
                else if item.type == .Video {
                    cell.cellimg.image = item.thumbnail
                }
                else if item.type == .Files {
                    cell.cellimg.image = item.thumbnail
                }
                else if item.type == .Spreadsheet {
                    cell.cellimg.image = item.thumbnail
                }
                else if item.type == .slideshow {
                    cell.cellimg.image = item.thumbnail
                }
                else {
                    cell.cellimg.image = item.thumbnail
                }
                cell.uploadIMG.isHidden = true
                cell.cellimg.isHidden = false
                cell.backgroundColor = .clear
            }
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BottomOptionCell", for: indexPath) as! BottomOptionCell
            
            let app = self.bottom_arry[indexPath.row]
            cell.cellimg.downloadedFrom(link: app.appIcon, contentMode: .scaleAspectFill, radious: 10)
            cell.alpha = self.SelectedBottom == nil ? 1.0 : self.SelectedBottom.appCode == app.appCode ? 1.0 : 0.3
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.AttachmentTBL {
            if self.previewMode {
                
            }
            else {
                if indexPath.row == self.UploadingArry.count {
                    self.TXTCompose.resignFirstResponder()
                    self.view.layoutIfNeeded() // force any pending operations to finish
                    UIView.animate(withDuration: 0.2, animations: { () -> Void in
                        self.uploadoption_height.constant = self.UploadViewHeight
                        self.PreviewBTN.setTitleColor(.clear, for: .normal)
                        self.PreviewBTN.setBackgroundImage(UIImage.init(named: "IC_Keyborad"), for: .normal)
                        self.PreviewBTN.isSelected = true
                        self.UploadOptionView.isHidden = false
                        self.previewMode = false
                        self.AttachmentOptionTBL.reloadData()
                        self.AttachmentTBL.reloadData()
                        self.view.layoutIfNeeded()
                    })
                }
                else {
                    
                }
            }
        }
        else {
            self.SelectedBottom = self.bottom_arry[indexPath.row]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.AttachmentTBL {
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
        else {
            return CGSize(width: 50, height: 50)
        }
    }
    
}

extension ComposeVC: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.view.layoutIfNeeded() // force any pending operations to finish
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.OptionHeight.constant = 50
            self.uploadoption_height.constant = isXseries ? 34 : 0
            self.UploadOptionView.isHidden = true
            self.view.layoutIfNeeded()
        })
        self.PreviewBTN.setTitleColor(.black, for: .normal)
        self.PreviewBTN.setBackgroundImage(UIImage.init(named: ""), for: .normal)
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.placeholderLabel.isHidden = !self.TXTCompose.text.isEmpty
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if(text == "\n") {
//            self.TXTCompose.resignFirstResponder()
//            return false
//        }
        return true
    }
}

extension ComposeVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AttachmentOptionCell = tableView.dequeueReusableCell(withIdentifier: "AttachmentOptionCell") as! AttachmentOptionCell
        switch indexPath.row {
        case 0:
            cell.view.backgroundColor = UIColor.colorWithHexString(hexStr: "FFF2E4")
            cell.icon.image = UIImage.init(named: "IC_PDF")
            cell.name.text = "PDF"
            
        case 1:
            cell.view.backgroundColor = UIColor.colorWithHexString(hexStr: "E7F8FF")
            cell.icon.image = UIImage.init(named: "IC_PhotoVideo")
            cell.name.text = "Photo"
            
        case 2:
            cell.view.backgroundColor = UIColor.colorWithHexString(hexStr: "E7F8FF")
            cell.icon.image = UIImage.init(named: "IC_PhotoVideo")
            cell.name.text = "Video"
            
        case 3:
            cell.view.backgroundColor = UIColor.colorWithHexString(hexStr: "E3FFFB")
            cell.icon.image = UIImage.init(named: "IC_SpreadSheet")
            cell.name.text = "SpreadSheet"
            
        case 4:
            cell.view.backgroundColor = UIColor.colorWithHexString(hexStr: "F7EAFF")
            cell.icon.image = UIImage.init(named: "IC_Slideshow")
            cell.name.text = "Slideshow"
            
        case 5:
            cell.view.backgroundColor = UIColor.colorWithHexString(hexStr: "FFEEEB")
            cell.icon.image = UIImage.init(named: "IC_Other")
            cell.name.text = "Other"
            
        default:
            cell.view.backgroundColor = UIColor.colorWithHexString(hexStr: "FFF2E4")
            cell.icon.image = UIImage.init(named: "IC_PDF")
            cell.name.text = "PDF"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        background {
            if indexPath.row == 1 {
                AttachmentHandler.shared.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self)
                AttachmentHandler.shared.imagePickedBlock = { (image) in
                    main {
                        let obj = AttachmentObject.init(type: .Image, item: image, thumbnail: image)
                        self.UploadingArry.append(obj)
                        self.AttachmentTBL.reloadData()
                        self.AttachmentTBL.scrollToItem(at: IndexPath.init(row: self.UploadingArry.count - 1, section: 0), at: .right, animated: true)
                    }
                }
            }
            else if indexPath.row == 2 {
                AttachmentHandler.shared.authorisationStatus(attachmentTypeEnum: .video, vc: self)
                AttachmentHandler.shared.videoPickedBlock = { (videoURL, thumbnail) in
                    main {
                        let obj = AttachmentObject.init(type: .Video, item: videoURL, thumbnail: thumbnail)
                        self.UploadingArry.append(obj)
                        self.AttachmentTBL.reloadData()
                    }
                }
            }
            else {
                AttachmentHandler.shared.documentPicker()
                AttachmentHandler.shared.filePickedBlock = { (fileURL, thumbnail) in
                    main {
                        let obj = AttachmentObject.init(type: indexPath.row == 0 ? .Files : indexPath.row == 3 ? .Spreadsheet : indexPath.row == 4 ? .slideshow : .other, item: fileURL, thumbnail: thumbnail)
                        self.UploadingArry.append(obj)
                        self.AttachmentTBL.reloadData()
                    }
                }
            }
        }
    }
    
}

enum AttachmentType {
    case Image
    case Video
    case Files
    case other
    case Spreadsheet
    case slideshow
}

struct AttachmentObject {
    var type: AttachmentType
    var item: Any
    var thumbnail: UIImage
}
