//
//  AttachmentCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 25/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class AttachmentCell: UICollectionViewCell {

    @IBOutlet weak var cellimg: UIImageView!
    @IBOutlet weak var uploadIMG: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
    }

}
