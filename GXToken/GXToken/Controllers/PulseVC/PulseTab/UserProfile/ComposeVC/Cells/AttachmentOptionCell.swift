//
//  AttachmentOptionCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 25/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class AttachmentOptionCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.view.backgroundColor = ViewBGColor
        self.view.clipsToBounds = true
        self.view.layer.cornerRadius = 15.0
        
        self.name.font = Font.MontserratRegularFont(font: 18)
        self.name.textColor = TextDark
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
