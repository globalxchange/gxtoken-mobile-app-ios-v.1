//
//  BottomOptionCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 24/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class BottomOptionCell: UICollectionViewCell {

    @IBOutlet weak var cellimg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
    }

}
