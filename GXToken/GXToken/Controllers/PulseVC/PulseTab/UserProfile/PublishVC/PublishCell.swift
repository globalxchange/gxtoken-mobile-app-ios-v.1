//
//  PublishCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 26/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class PublishCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var notes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        view.backgroundColor = ViewBGColor
        view.clipsToBounds = true
        view.layer.cornerRadius = 10
        view.layer.borderColor = SepratorColor?.cgColor
        
        icon.backgroundColor = DarkBlue
        
        name.textColor = DarkBlue
        name.font = Font.MontserratRegularFont(font: 18)
        
        notes.textColor = DarkBlue
        notes.font = Font.MontserratRegularFont(font: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
