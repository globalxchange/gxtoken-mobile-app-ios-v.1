//
//  PublishVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 26/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class PublishVC: BaseAppVC {

    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var main_height: NSLayoutConstraint!
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet var bottomlbl: [UILabel]!
    @IBOutlet weak var ViewTitleLBL: UILabel!
    @IBOutlet weak var DismissBTN: UIButton!
    
    @IBOutlet weak var OptionTBL: UITableView!
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = DarkBlue
        }
        self.Nautch.backgroundColor = DarkBlue
        self.cornerColor()
        self.setupUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK:- User Define Methods
    func cornerColor(color: UIColor = ViewBGColor) {
        for lbl in self.bottomlbl {
            lbl.backgroundColor = color
        }
    }
    
    func setupUI() {
        
        self.DismissBTN.backgroundColor = .clear
        self.main_height.constant = Screen_height / 2
        self.ViewTitleLBL.textColor = DarkBlue
        self.ViewTitleLBL.font = Font.MontserratRegularFont(font: 24)
        self.ViewTitleLBL.text = "Choose Publication"
        
        self.OptionTBL.register(UINib.init(nibName: "PublishCell", bundle: nil), forCellReuseIdentifier: "PublishCell")
        self.OptionTBL.delegate = self
        self.OptionTBL.dataSource = self
        self.OptionTBL.reloadData()
        
    }

    @IBAction func TappedDissmis(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension PublishVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PublishCell = tableView.dequeueReusableCell(withIdentifier: "PublishCell") as! PublishCell
        cell.name.text = "Publiation name"
        cell.notes.text = "New BTC Balance"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
