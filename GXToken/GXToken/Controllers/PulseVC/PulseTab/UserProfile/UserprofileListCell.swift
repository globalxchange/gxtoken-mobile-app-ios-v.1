//
//  UserprofileListCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 22/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class UserprofileListCell: UITableViewCell {
    
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var ProfileView: UIView!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var OptionBTN: UIButton!
    
    @IBOutlet weak var NoteView: UIView!
    @IBOutlet weak var NoteLBL: UILabel!
    
    @IBOutlet weak var ActionView: UIView!
    @IBOutlet weak var LikeallIMG: UIImageView!
    @IBOutlet weak var LoveallIMG: UIImageView!
    @IBOutlet weak var likedetaillbl: UILabel!
    
    @IBOutlet weak var LikeView: UIView!
    @IBOutlet weak var LikeIMG: UIImageView!
    @IBOutlet weak var LikeLBL: UILabel!
    @IBOutlet weak var LikeBTN: UIButton!
    
    @IBOutlet weak var CommentView: UIView!
    @IBOutlet weak var CommentIMG: UIImageView!
    @IBOutlet weak var CommentLBL: UILabel!
    @IBOutlet weak var CommentBTN: UIButton!
    
    @IBOutlet weak var ShareView: UIView!
    @IBOutlet weak var ShareIMG: UIImageView!
    @IBOutlet weak var ShareLBL: UILabel!
    @IBOutlet weak var ShareBTN: UIButton!
    
    @objc public var MenuactionHandler: ((Int) -> Void)?
    @objc public var LikeactionHandler: ((Int) -> Void)?
    @objc public var CommentactionHandler: ((Int) -> Void)?
    @objc public var ShareactionHandler: ((Int) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.MainView.backgroundColor = ViewBGColor
        
        self.ProfileView.backgroundColor = ViewBGColor
        
        self.profile.clipsToBounds = true
        self.profile.layer.cornerRadius = self.profile.frame.height / 2
        
        self.NoteView.backgroundColor = LightWhite
        self.NoteView.clipsToBounds = true
        self.NoteView.layer.cornerRadius = 15
        self.NoteLBL.textColor = .black
        self.NoteLBL.font = Font.MontserratRegularFont(font: 17)
        
        self.likedetaillbl.textColor = TextColor
        self.likedetaillbl.font = Font.MontserratRegularFont(font: 17)
        
        self.ActionView.backgroundColor = ViewBGColor
        self.LikeView.backgroundColor = ViewBGColor
        self.LikeLBL.text = "Like"
        self.LikeLBL.textColor = TextColor
        self.LikeLBL.font = Font.MontserratRegularFont(font: 14)
        
        self.CommentView.backgroundColor = ViewBGColor
        self.CommentLBL.text = "Comment"
        self.CommentLBL.textColor = TextColor
        self.CommentLBL.font = Font.MontserratRegularFont(font: 14)
        
        self.ShareView.backgroundColor = ViewBGColor
        self.ShareLBL.text = "Share"
        self.ShareLBL.textColor = TextColor
        self.ShareLBL.font = Font.MontserratRegularFont(font: 14)
        
    }

    func SetupProfileName(string1: String, string2: String) {
        let mainstring: String = string1 + string2
        let myMutableString = mainstring.Attributestring(attribute: [
            (string1, Font.MontserratRegularFont(font: 20), .black),
            (string2, Font.MontserratSemiBoldFont(font: 12.0), LightGray)
        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [string1 ,string2])))
        self.titleLBL.attributedText = myMutableString
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func TappedAction(_ sender: UIButton) {
        if sender == self.OptionBTN {
            self.MenuactionHandler?(sender.tag)
        }
        else if sender == self.LikeBTN {
            self.LikeactionHandler?(sender.tag)
        }
        else if sender == self.CommentBTN {
            self.CommentactionHandler?(sender.tag)
        }
        else {
            self.ShareactionHandler?(sender.tag)
        }
    }
    
}
