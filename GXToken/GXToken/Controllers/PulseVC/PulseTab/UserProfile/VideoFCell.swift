//
//  VideoFCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 22/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class VideoFCell: UICollectionViewCell {
    
    @IBOutlet weak var pictureIMG: UIImageView!
    @IBOutlet weak var titleLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.pictureIMG.clipsToBounds = true
        self.pictureIMG.layer.cornerRadius = 10
        
        self.titleLBL.textColor = LightGray
        self.titleLBL.font = Font.MontserratRegularFont(font: 12)
    }
    
}
