//
//  AppsVCVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class AppsVC: BaseAppVC {
    
    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    @IBOutlet weak var SettingBTN: UIButton!
    
    @IBOutlet weak var MainSubview: UIView!
    
    @IBOutlet weak var StackView: UIView!
    @IBOutlet weak var TopCaroselView: UIView!
    @IBOutlet weak var TopTitleLBL: UILabel!
    @IBOutlet weak var Carousel: iCarousel!
    @IBOutlet weak var TopSelectionLBL: UILabel!
    
    @IBOutlet weak var CategoryListView: UIView!
    @IBOutlet weak var CategoryTitleLBL: UILabel!
    @IBOutlet weak var Cat_Carousel: iCarousel!
    @IBOutlet weak var CatSelectionLBL: UILabel!
    
    @IBOutlet weak var BottomListView: UIView!
    @IBOutlet weak var ListTBL: UITableView!
    
    // MARK:- Variable Define
    var carousel_arry : [CatAppsApp] = []
    var category_arry : [AppCategoriesCategory] = []
    var cat_app_arry : [CatAppsApp] = []
    var Selected_CatID: String = ""
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.SetupUI()
    }

    // MARK:- User define methods
    
    func SetupUI() {
        self.MainSubview.backgroundColor = ViewBGColor
        
        self.StackView.backgroundColor = ViewBGColor
        self.TopCaroselView.backgroundColor = ViewBGColor
        self.TopTitleLBL.backgroundColor = ViewBGColor
        self.TopTitleLBL.textColor = DarkBlue
        
        let str1 = "GXLive App\n"
        let str2 = "Powered By The GXToken"
        let mainstring: String = str1 + str2
        let myMutableString = mainstring.Attributestring(attribute: [(str1, Font.MontserratRegularFont(font: 27), DarkBlue), (str2, Font.MontserratRegularFont(font: 13), .black)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1, str2])))
        self.TopTitleLBL.attributedText = myMutableString
        
        self.TopSelectionLBL.font = Font.MontserratRegularFont(font: 13)
        self.TopSelectionLBL.textColor = ShadowDarkBlue
        
        self.Carousel.type = .rotary
        self.Carousel.backgroundColor = ViewBGColor
        self.GetCarouselList()
        
        self.CategoryListView.backgroundColor = ViewBGColor
        self.CategoryTitleLBL.text = "Categories"
        self.CategoryTitleLBL.backgroundColor = ViewBGColor
        self.CategoryTitleLBL.textColor = DarkBlue
        self.Cat_Carousel.backgroundColor = ViewBGColor
        self.Cat_Carousel.type = .invertedRotary
        self.GetCategoryList()
        
        self.CatSelectionLBL.font = Font.MontserratRegularFont(font: 13)
        self.CatSelectionLBL.textColor = ShadowDarkBlue
        
        self.BottomListView.backgroundColor = ViewBGColor
        self.ListTBL.backgroundColor = ViewBGColor
        self.ListTBL.register(UINib.init(nibName: "ApplistCell", bundle: nil), forCellReuseIdentifier: "ApplistCell")
        self.ListTBL.tableFooterView = UIView.init()
    }
    
    // MARK:- API Calling
    func GetCarouselList() {
        ApiLoader.shared.showHUD()
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Cat_Applist.value()) { (responseObject) in
            let root = CatAppsRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status && root.count >= 1 {
                self.carousel_arry = root.apps
                self.Carousel.delegate = self
                self.Carousel.dataSource = self
                self.Carousel.reloadData()
            }
            ApiLoader.shared.stopHUD()
        } onError: { (message, code) in
            
        }
    }
    
    func GetCategoryList() {
        ApiLoader.shared.showHUD()
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_AppCategories.value()) { (responseObject) in
            let root = AppCategoriesRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status {
                self.category_arry = root.categories
                self.Cat_Carousel.delegate = self
                self.Cat_Carousel.dataSource = self
                self.Cat_Carousel.reloadData()
//                let cate = self.category_arry.filter({ (cat) -> Bool in
//                    return cat.name.uppercased() == "test".uppercased()
//                })
//                self.Selected_CatID = (cate.first?.categoryId)!
                self.GetCat_Applist(catid: self.Selected_CatID)
                
                self.ListTBL.delegate = self
                self.ListTBL.dataSource = self
            }
            ApiLoader.shared.stopHUD()
        } onError: { (message, code) in
            
        }
    }
    
    func GetCat_Applist(catid: String) {
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Cat_Applist.value(), Parameters: ["category_id" : catid], Headers: [:]) {  (responseObject) in
            let root = CatAppsRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status {
                self.cat_app_arry.removeAll()
                self.cat_app_arry = root.apps
                self.ListTBL.isHidden = false
                self.ListTBL.reloadData()
            }
        } onError: { (message, code) in
            self.cat_app_arry.removeAll()
            self.ListTBL.isHidden = true
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        if sender == self.MenuBTN {
            let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
            Menu.OptionSelection = "pulse"
            self.navigationController?.pushViewController(Menu, animated: true)
        }
        else {
            let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
            let vc = UINavigationController(rootViewController: setting)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }

    @IBAction func TappedSearch(_ sender: UIButton) {
        let vc = AppsSearchVC.init(nibName: "AppsSearchVC", bundle: nil)
        if sender.tag == 100 {
            vc.SelectFrom = "Top"
            vc.carousel_arry = self.carousel_arry
        }
        else {
            vc.SelectFrom = "Category"
            vc.category_arry = self.category_arry
        }
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func tapped_search() {
        let vc = AppsSearchVC.init(nibName: "AppsSearchVC", bundle: nil)
        vc.SelectFrom = "AppList"
        vc.cat_app_arry = self.cat_app_arry
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK:- AppsSearchDelegate
// MARK:-
extension AppsVC: AppsSearchDelegate {
    func SelectedApp(app: CatAppsApp, Index: Int) {
        if Index == 0 {
            let selected = self.carousel_arry.enumerated().filter { (obj) -> Bool in
                obj.element.appName == app.appName
            }.map ({ $0.offset }).first
            self.Carousel.scroll(byNumberOfItems: selected!, duration: 1)
        }
        else {
            let selected = self.cat_app_arry.enumerated().filter { (obj) -> Bool in
                obj.element.appName == app.appName
            }.map ({ $0.offset }).first
            self.ListTBL.beginUpdates()
            self.ListTBL.scrollToRow(at: IndexPath.init(row: selected!, section: 0), at: .top, animated: true)
            self.ListTBL.endUpdates()
        }
    }
    
    func SelectedCategory(cat: AppCategoriesCategory) {
        let selected = self.category_arry.enumerated().filter { (obj) -> Bool in
            obj.element.name == cat.name
        }.map ({ $0.offset }).first
        self.Cat_Carousel.scroll(byNumberOfItems: selected!, duration: 1)
    }
}

//MARK:- Carousel Delegates
//
extension AppsVC: iCarouselDataSource, iCarouselDelegate {
    
    func updateTopSelection(index: Int) {
        let banner = self.carousel_arry[index]
        self.TopSelectionLBL.text = banner.appName
    }
    
    func updateCatSelection(index: Int) {
        let cat = self.category_arry[index]
        self.CatSelectionLBL.text = cat.name
        let cate = self.category_arry.filter({ (result) -> Bool in
            return result.name.uppercased() == cat.name.uppercased()
        })
        self.Selected_CatID = (cate.first?.categoryId)!
        self.GetCat_Applist(catid: self.Selected_CatID)
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        if carousel == self.Carousel {
            return self.carousel_arry.count
        }
        else {
            return self.category_arry.count
        }
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return 1
    }

    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var itemView: AppCarouselView
        if let view = view {
            itemView = view as! AppCarouselView
        } else {
            itemView = AppCarouselView(frame: CGRect(x: 0, y: 0, width: carousel.frame.height, height: carousel.frame.height))
        }
        if carousel == self.Carousel {
            let banner = self.carousel_arry[index]
            itemView.setupBanner(obj: banner, Index: index)
            self.updateTopSelection(index: carousel.currentItemIndex)
        }
        else {
            let cat = self.category_arry[index]
            itemView.setupCategory(obj: cat, Index: index)
            self.updateCatSelection(index: index == (self.category_arry.count - 1) ? 0 : self.Cat_Carousel.currentItemIndex)
        }
        return itemView
    }

    private func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.1
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        if carousel == self.Carousel {
            self.updateTopSelection(index: carousel.currentItemIndex)
        }
        else {
            self.updateCatSelection(index: self.Cat_Carousel.currentItemIndex)
        }
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        if carousel == self.Carousel {
            self.updateTopSelection(index: index)
        }
        else {
            self.updateCatSelection(index: index)
        }
    }
    
}

//MARK:- UITableView Delegates
//MARK:-
extension AppsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cat_app_arry.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        view.backgroundColor = ViewBGColor
        let TitleLBL = UILabel.init(frame: CGRect.init(x: 20, y: 5, width: view.frame.width - 60, height: 20))
        TitleLBL.textColor = DarkBlue
        let cate = self.category_arry.filter({ (cat) -> Bool in
            return cat.categoryId == self.Selected_CatID
        }).first
        TitleLBL.text = cate?.name
        view.addSubview(TitleLBL)
        
        let search = UIButton.init(frame: CGRect.init(x: Screen_width - 50, y: 0, width: 30, height: 30))
        search.tag = 300
        search.setTitle("", for: .normal)
        search.setImage(UIImage.init(named: "IC_Search"), for: .normal)
        search.addTarget(self, action: #selector(tapped_search), for: .touchUpInside)
        view.addSubview(search)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ApplistCell = tableView.dequeueReusableCell(withIdentifier: "ApplistCell") as! ApplistCell
        let app = self.cat_app_arry[indexPath.row]
        let str1 = app.appName + "\n"
        let str2 = app.shortDescription!
        let mainstring: String = str1 + str2
        let myMutableString = mainstring.Attributestring(attribute: [(str1, Font.MontserratRegularFont(font: 17), DarkBlue), (str2, Font.MontserratRegularFont(font: 11), .black)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1, str2])))
        cell.titlelbl.attributedText = myMutableString
        cell.iconIMG.downloadedFrom(link: app.appIcon, contentMode: .scaleAspectFill, radious: 10)
        cell.INstallBTN.tag = indexPath.row
        cell.InstallAction = {
            let animation = AnimationVC.init(nibName: "AnimationVC", bundle: nil)
            animation.AppCode = app.appCode
            animation.ComeFrom = "Pulse_App_Install"
            animation.AnimationDismissAction = {
                self.dismiss(animated: false, completion: nil)
            }
            let vc = UINavigationController(rootViewController: animation)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.cat_app_arry[indexPath.row]
        let vc = AppDetailsVC.init(nibName: "AppDetailsVC", bundle: nil)
        vc.CatApp = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
