//
//  InstallAppVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 07/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class InstallAppVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet var bgLBL: [UILabel]!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var Stack: UIStackView!
    
    @IBOutlet weak var iOS_main: UIView!
    @IBOutlet weak var iOS_sub: UIView!
    @IBOutlet weak var ios_AppIMG: UIImageView!
    @IBOutlet weak var iOS_LBL: UILabel!
    @IBOutlet weak var IOS_BTN: UIButton!
    
    @IBOutlet weak var Android_main: UIView!
    @IBOutlet weak var Android_sub: UIView!
    @IBOutlet weak var Android_AppIMG: UIImageView!
    @IBOutlet weak var Android_LBL: UILabel!
    @IBOutlet weak var Android_BTN: UIButton!
    
    @IBOutlet weak var CloseBTN: UIButton!
    @IBOutlet weak var InviteBTN: UIButton!
    
    // TODO:- Variable Define
    var ios_link: String = ""
    var android_link: String = ""
    open var InstallDismissAction: (() -> ())?
    
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        self.MainView.clipsToBounds = true
        self.cornerColor(color: UIColor.white)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.NavLogo_IMG.tintColor = DarkBlue
    }
    
    // MARK:- API call
    // MARK:-
    
    // MARK:- User Define Methods
    func setupView() {
        
        let str1 = "Select Platform\n\n"
        let str2 = "What Operating System Does Your Phone User"
        let mainstring: String = str1 + str2
        let myMutableString = mainstring.Attributestring(attribute: [(str1, Font.MontserratRegularFont(font: 30), DarkBlue), (str2, Font.MontserratRegularFont(font: 12), .lightGray)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1, str2])))
        self.TitleLBL.attributedText = myMutableString
        
        self.iOS_main.backgroundColor = ViewBGColor
        
        self.iOS_sub.backgroundColor = ViewBGColor
        self.iOS_sub.clipsToBounds = true
        self.iOS_sub.layer.cornerRadius = 10
        self.iOS_sub.layer.borderColor = LightGroup.cgColor
        self.iOS_sub.layer.borderWidth = 1
        self.iOS_sub.layer.shadowColor = UIColor.black.cgColor
        self.iOS_sub.layer.shadowRadius = 1
        self.iOS_sub.layer.shadowOpacity = 0.4
        self.iOS_sub.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        self.ios_AppIMG.image = UIImage.init(named: "IC_Apple_Invite")
        
        self.iOS_LBL.textColor = ShadowDarkBlue
        self.iOS_LBL.font = Font.MontserratRegularFont(font: 13)
        self.iOS_LBL.text = "iOS"
        
        
        self.Android_main.backgroundColor = ViewBGColor
        
        self.Android_sub.backgroundColor = ViewBGColor
        self.Android_sub.clipsToBounds = true
        self.Android_sub.layer.cornerRadius = 10
        self.Android_sub.layer.borderColor = LightGroup.cgColor
        self.Android_sub.layer.borderWidth = 1
        self.Android_sub.layer.shadowColor = UIColor.black.cgColor
        self.Android_sub.layer.shadowRadius = 1
        self.Android_sub.layer.shadowOpacity = 0.4
        self.Android_sub.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        self.Android_AppIMG.image = UIImage.init(named: "IC_Android_Invite")
        
        self.Android_LBL.textColor = ShadowDarkBlue
        self.Android_LBL.font = Font.MontserratRegularFont(font: 13)
        self.Android_LBL.text = "Android"
        
        self.CloseBTN.clipsToBounds = true
        self.CloseBTN.layer.borderWidth = 1
        self.CloseBTN.layer.borderColor = DarkBlue.cgColor
        self.CloseBTN.setTitle("Close", for: .normal)
        self.CloseBTN.setTitleColor(DarkBlue, for: .normal)
        
        self.InviteBTN.clipsToBounds = true
        self.InviteBTN.backgroundColor = DarkBlue
        self.InviteBTN.setTitle("Invite", for: .normal)
        self.InviteBTN.setTitleColor(.white, for: .normal)
        
    }
    
    func cornerColor(color: UIColor = UIColor.white) {
        for lbl in self.bgLBL {
            lbl.backgroundColor = color
        }
    }
    
    // MARK:- IBAction Methods
    @IBAction func TappedBUtton(_ sender: UIButton) {
        if sender == self.CloseBTN {
            self.dismiss(animated: true) {
                self.InstallDismissAction!()
            }
        }
        else if sender == self.InviteBTN {
            
        }
        else if sender == self.IOS_BTN {
            
        }
        else {
            
        }
    }

}
