//
//  AppsSearchVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 07/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

protocol AppsSearchDelegate {
    func SelectedApp(app: CatAppsApp, Index: Int)
    func SelectedCategory(cat: AppCategoriesCategory)
}

class AppsSearchVC: BaseAppVC {

    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    @IBOutlet weak var SettingBTN: UIButton!
    
    @IBOutlet weak var MainSubview: UIView!
    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var ListTBL: TPKeyboardAvoidingTableView!
    
    // Variable Defines
    var SelectFrom: String = ""
    var carousel_arry : [CatAppsApp] = []
    var category_arry : [AppCategoriesCategory] = []
    var cat_app_arry : [CatAppsApp] = []
    var filter: [Any] = []
    var delegate: AppsSearchDelegate!
    
    // MARK:- View life cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.SetupUI()
    }

    // MARK:- User define methods
    
    func SetupUI() {
        self.MainSubview.backgroundColor = ViewBGColor
        if self.SelectFrom.uppercased() == "Top".uppercased() {
            self.SearchBar.placeholder = "Search All App"
            self.filter = self.carousel_arry
        }
        else if self.SelectFrom.uppercased() == "Category".uppercased() {
            self.SearchBar.placeholder = "Search Category"
            self.filter = self.category_arry
        }
        else {
            self.SearchBar.placeholder = "Search App"
            self.filter = self.cat_app_arry
        }
        self.SearchBar.delegate = self
        self.ListTBL.register(UINib.init(nibName: "ApplistCell", bundle: nil), forCellReuseIdentifier: "ApplistCell")
        self.ListTBL.tableFooterView = UIView.init()
        self.ListTBL.delegate = self
        self.ListTBL.dataSource = self
        self.ListTBL.reloadData()
    }

    // MARK:- IBAction Methods
    
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        if sender == self.MenuBTN {
            let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
            Menu.OptionSelection = "pulse"
            self.navigationController?.pushViewController(Menu, animated: true)
        }
        else {
            let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
            let vc = UINavigationController(rootViewController: setting)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }

}

//MARK:- UITableView Delegates
//MARK:-
extension AppsSearchVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filter.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ApplistCell = tableView.dequeueReusableCell(withIdentifier: "ApplistCell") as! ApplistCell
        if self.SelectFrom.uppercased() == "Top".uppercased() {
            let data: CatAppsApp = self.filter[indexPath.row] as! CatAppsApp
            let str1 = data.appName + "\n"
            let str2 = data.shortDescription!
            let mainstring: String = str1 + str2
            let myMutableString = mainstring.Attributestring(attribute: [(str1, Font.MontserratRegularFont(font: 17), DarkBlue), (str2, Font.MontserratRegularFont(font: 11), .black)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1, str2])))
            cell.titlelbl.attributedText = myMutableString
            cell.iconIMG.downloadedFrom(link: data.appIcon, contentMode: .scaleAspectFill, radious: 10)
            cell.INstallBTN.tag = indexPath.row
            cell.INstallBTN.setTitle("Select", for: .normal)
            cell.InstallAction = {
                
            }
        }
        else if self.SelectFrom.uppercased() == "Category".uppercased() {
            let data: AppCategoriesCategory = self.filter[indexPath.row] as! AppCategoriesCategory
            let str1 = data.name + "\n"
            let str2 = data.descriptionField!
            let mainstring: String = str1 + str2
            let myMutableString = mainstring.Attributestring(attribute: [(str1, Font.MontserratRegularFont(font: 17), DarkBlue), (str2, Font.MontserratRegularFont(font: 11), .black)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1, str2])))
            cell.titlelbl.attributedText = myMutableString
            cell.iconIMG.downloadedFrom(link: data.icon, contentMode: .scaleAspectFill, radious: 10)
            cell.INstallBTN.tag = indexPath.row
            cell.INstallBTN.setTitle("Select", for: .normal)
            cell.InstallAction = {
                
            }
        }
        else {
            let data: CatAppsApp = self.filter[indexPath.row] as! CatAppsApp
            let str1 = data.appName + "\n"
            let str2 = data.shortDescription!
            let mainstring: String = str1 + str2
            let myMutableString = mainstring.Attributestring(attribute: [(str1, Font.MontserratRegularFont(font: 17), DarkBlue), (str2, Font.MontserratRegularFont(font: 11), .black)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1, str2])))
            cell.titlelbl.attributedText = myMutableString
            cell.iconIMG.downloadedFrom(link: data.appIcon, contentMode: .scaleAspectFill, radious: 10)
            cell.INstallBTN.tag = indexPath.row
            cell.INstallBTN.setTitle("Install", for: .normal)
            cell.InstallAction = {
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.SelectFrom.uppercased() == "Top".uppercased() {
            let data: CatAppsApp = self.filter[indexPath.row] as! CatAppsApp
//            self.delegate.SelectedApp(app: data, Index: 0)
            let vc = AppDetailsVC.init(nibName: "AppDetailsVC", bundle: nil)
            vc.CatApp = data
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if self.SelectFrom.uppercased() == "Category".uppercased() {
            let data: AppCategoriesCategory = self.filter[indexPath.row] as! AppCategoriesCategory
            self.delegate.SelectedCategory(cat: data)
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let data: CatAppsApp = self.filter[indexPath.row] as! CatAppsApp
//            self.delegate.SelectedApp(app: data, Index: 1)
            let vc = AppDetailsVC.init(nibName: "AppDetailsVC", bundle: nil)
            vc.CatApp = data
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension AppsSearchVC: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.SearchBar.showsCancelButton = true
        self.filter.removeAll()
        if self.SelectFrom.uppercased() == "Top".uppercased() {
            self.filter = self.carousel_arry
        }
        else if self.SelectFrom.uppercased() == "Category".uppercased() {
            self.filter = self.category_arry
        }
        else {
            self.filter = self.cat_app_arry
        }
        self.ListTBL.reloadData()
        return true
    }

    func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
        self.SearchBar.showsCancelButton = false
        self.filter.removeAll()
        if self.SelectFrom.uppercased() == "Top".uppercased() {
            self.filter = self.carousel_arry
        }
        else if self.SelectFrom.uppercased() == "Category".uppercased() {
            self.filter = self.category_arry
        }
        else {
            self.filter = self.cat_app_arry
        }
        self.ListTBL.reloadData()
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.filter.removeAll()
        if self.SelectFrom.uppercased() == "Top".uppercased() {
            let result = self.carousel_arry.filter { (obj) -> Bool in
                return obj.appName.contains(self.SearchBar.text!)
            }
            self.filter = result
        }
        else if self.SelectFrom.uppercased() == "Category".uppercased() {
            let result = self.category_arry.filter { (obj) -> Bool in
                return obj.name.contains(self.SearchBar.text!)
            }
            self.filter = result
        }
        else {
            let result = self.cat_app_arry.filter { (obj) -> Bool in
                return obj.appName.contains(self.SearchBar.text!)
            }
            self.filter = result
        }
        self.ListTBL.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.SearchBar.showsCancelButton = false
        self.SearchBar.text = ""
        self.filter.removeAll()
        if self.SelectFrom.uppercased() == "Top".uppercased() {
            self.filter = self.carousel_arry
        }
        else if self.SelectFrom.uppercased() == "Category".uppercased() {
            self.filter = self.category_arry
        }
        else {
            self.filter = self.cat_app_arry
        }
        self.ListTBL.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.filter.removeAll()
        if self.SelectFrom.uppercased() == "Top".uppercased() {
            let result = self.carousel_arry.filter { (obj) -> Bool in
                return obj.appName.contains(text)
            }
            self.filter = result
        }
        else if self.SelectFrom.uppercased() == "Category".uppercased() {
            let result = self.category_arry.filter { (obj) -> Bool in
                return obj.name.contains(text)
            }
            self.filter = result
        }
        else {
            let result = self.cat_app_arry.filter { (obj) -> Bool in
                return obj.appName.contains(text)
            }
            self.filter = result
        }
        self.ListTBL.reloadData()
        return true
    }
    
}
