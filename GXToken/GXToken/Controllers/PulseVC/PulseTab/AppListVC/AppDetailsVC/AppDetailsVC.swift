//
//  AppDetailsVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 18/11/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class AppDetailsVC: BaseAppVC {

    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var MenuBack: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    @IBOutlet weak var SettingBTN: UIButton!
    
    @IBOutlet weak var MainSubview: UIView!
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var bannerIMG: UIImageView!
    @IBOutlet weak var BannertHeight: NSLayoutConstraint!
    @IBOutlet weak var AppIMG: UIImageView!
    @IBOutlet weak var DetailLBL: UILabel!
    @IBOutlet weak var OverviewBTN: UIButton!
    @IBOutlet weak var InstallBTN: UIButton!
    
    // MARK:- Variable Define
    var CatApp : CatAppsApp!
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.SetupUI()
    }

    // MARK:- User define methods
    
    func SetupUI() {
        self.MainSubview.backgroundColor = ViewBGColor
        
        self.BannertHeight.constant = Screen_width / 2
        
        self.bannerIMG.downloadedFrom(link: self.CatApp.coverPhoto, contentMode: .scaleAspectFill, radious: 0)
        
        self.AppIMG.downloadedFrom(link: self.CatApp.appIcon, contentMode: .scaleAspectFill, radious: 10)
        
        let str1 = self.CatApp.appName + "\n\n\n"
        let str2 = self.CatApp.longDescription!
        let mainstring: String = str1 + str2
        let myMutableString = mainstring.Attributestring(attribute: [(str1, Font.MontserratRegularFont(font: 25), DarkBlue), (str2, Font.MontserratRegularFont(font: 13), .black)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [str1, str2])))
        self.DetailLBL.attributedText = myMutableString
        
        self.OverviewBTN.backgroundColor = ViewBGColor
        self.OverviewBTN.layer.borderWidth = 1
        self.OverviewBTN.layer.borderColor = DarkBlue.cgColor
        self.OverviewBTN.setTitleColor(DarkBlue, for: .normal)
        self.OverviewBTN.setTitle("Overview", for: .normal)
        
        self.InstallBTN.backgroundColor = ViewBGColor
        self.InstallBTN.layer.borderWidth = 1
        self.InstallBTN.layer.borderColor = DarkBlue.cgColor
        self.InstallBTN.setTitleColor(DarkBlue, for: .normal)
        self.InstallBTN.setTitle("Install", for: .normal)
    }
    
    // MARK:- API Calling

    // MARK:- IBAction Methods
    
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        if sender == self.MenuBTN {
            let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
            Menu.OptionSelection = "pulse"
            self.navigationController?.pushViewController(Menu, animated: true)
        }
        else if sender == self.MenuBack {
            self.navigationController?.popViewController(animated: true)
        }
        else {
            let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
            let vc = UINavigationController(rootViewController: setting)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func TappedOption(_ sender: UIButton) {
    }
    
}
