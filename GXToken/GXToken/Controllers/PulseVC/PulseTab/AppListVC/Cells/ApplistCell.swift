//
//  ApplistCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 21/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class ApplistCell: UITableViewCell {

    @IBOutlet weak var View: UIView!
    @IBOutlet weak var iconIMG: UIImageView!
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var INstallBTN: UIButton!
    
    open var InstallAction: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        View.backgroundColor = ViewBGColor
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        
        iconIMG.clipsToBounds = true
        iconIMG.layer.cornerRadius = 10
        iconIMG.layer.borderColor = LightGroup.cgColor
        iconIMG.layer.borderWidth = 1
        
        INstallBTN.backgroundColor = DarkBlue
        INstallBTN.setTitleColor(.white, for: .normal)
        INstallBTN.setTitle("Install", for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func InstallAction(_ sender: Any) {
        self.InstallAction!()
    }
}
