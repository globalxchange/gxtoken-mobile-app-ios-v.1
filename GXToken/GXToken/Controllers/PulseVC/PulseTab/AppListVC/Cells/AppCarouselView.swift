//
//  AppCarousel.swift
//  GXToken
//
//  Created by Hiren Joshi on 29/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class AppCarouselView: UIView {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var Icon: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    func loadViewFromNib() {
        
        let view = UINib(nibName: "AppCarouselView", bundle: Bundle(for: type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.backgroundColor = .clear
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
        
        self.backgroundColor = .clear
        
        self.mainView.backgroundColor = ViewBGColor
        self.mainView.clipsToBounds = true
        self.mainView.layer.cornerRadius = 10
        self.mainView.layer.borderColor = LightGroup.cgColor
        self.mainView.layer.borderWidth = 1
        self.mainView.layer.shadowColor = UIColor.black.cgColor
        self.mainView.layer.shadowRadius = 1
        self.mainView.layer.shadowOpacity = 0.4
        self.mainView.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        self.Icon.clipsToBounds = true
        self.Icon.layer.cornerRadius = 10
    }
    
    func setupBanner(obj: CatAppsApp, Index: Int) {
        if obj.appIcon.IsStrEmpty() {
            self.Icon.image = UIImage.init(named: "IC_Application")
        }
        else {
            self.Icon.downloadedFrom(link: obj.appIcon, contentMode: .scaleAspectFit, radious: 10)
        }
    }
    
    func setupCategory(obj: AppCategoriesCategory, Index: Int) {
        if obj.icon.IsStrEmpty() {
            self.Icon.image = UIImage.init(named: "IC_Application")
        }
        else {
            self.Icon.downloadedFrom(link: obj.icon, contentMode: .scaleAspectFill, radious: 10)
        }
    }

}
