//
//  PulseTabbarVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class PulseTabbarVC: UITabBarController {
    
    // MARK:- Outlet Controls
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    // MARK:- User define methods
    
    
    // MARK:- API Calling

    // MARK:- IBAction Methods

}

extension PulseTabbarVC: UITabBarControllerDelegate {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
//        if tabBar.tag == 0 {
//            item.title = "News"
//        }
//        else if tabBar.tag == 1 {
//            item.title = "App"
//        }
//        else if tabBar.tag == 2 {
//            item.title = "Videos"
//        }
//        else {
//            item.title = "Profile"
//        }
    }

    // UITabBarControllerDelegate
    private func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        print("Selected view controller")
    }
}
