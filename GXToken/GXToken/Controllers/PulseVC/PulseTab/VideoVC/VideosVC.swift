//
//  VideosVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit
import AVFoundation
import InstantSearchVoiceOverlay

class VideosVC: BaseAppVC {

    // MARK:- Outlet Controls
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var MenuBTN: UIButton!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    @IBOutlet weak var SettingBTN: UIButton!
    
    @IBOutlet weak var MainSubview: UIView!
    
    @IBOutlet weak var SearchVIew: UIView!
    @IBOutlet weak var SubSearch: UIView!
    @IBOutlet weak var TXTSearch: UITextField!
    @IBOutlet weak var Seprator: UILabel!
    @IBOutlet weak var SepakerBTN: UIButton!
    
    @IBOutlet weak var List_TBL: TPKeyboardAvoidingTableView!
    
    // MARK:- Variable Defines
    let voiceOverlayController = VoiceOverlayController()
    var video_arry : [PulseVideoData] = []
    var FilterArry : [PulseVideoData] = []
    
    // MARK:- View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.NavView.backgroundColor = DarkBlue
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullwhitelogo")
        self.NavLogo_IMG.tintColor = .white
        
        self.nodatafound.frame = self.MainSubview.frame
        self.nodatafound.didActionBlock = {
            self.GetVideoList()
        }
        self.GetVideoList()
        self.SetupUI()
    }

    // MARK:- User define methods
    
    func SetupUI() {
        self.MainSubview.backgroundColor = ViewBGColor
        
        self.SearchVIew.backgroundColor = ViewBGColor
        self.SubSearch.backgroundColor = ViewBGColor
        self.SubSearch.clipsToBounds = true
        self.SubSearch.layer.cornerRadius = 10
        self.SubSearch.layer.borderWidth = 1
        self.SubSearch.layer.borderColor = LightGroup.cgColor
        self.Seprator.backgroundColor = LightGroup
        
        self.TXTSearch.backgroundColor = ViewBGColor
        self.TXTSearch.delegate = self
        self.TXTSearch.font = Font.MontserratRegularFont(font: 18.0)
        
        self.SepakerBTN.backgroundColor = ViewBGColor
        self.List_TBL.backgroundColor = ViewBGColor
        
        self.List_TBL.register(UINib.init(nibName: "VideoCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
        
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.List_TBL.frame.width, height: 30))
        footer.backgroundColor = ViewBGColor
        self.List_TBL.tableFooterView = footer
        
    }
    
    // MARK:- API Calling
    func GetVideoList() {
        ApiLoader.shared.showHUD()
        NetworkingRequests.shared.requestsGET(CommanApiCall.API_Pulse_Videos.value()) { (responseObject) in
            let root = PulseVideoRootClass.init(fromDictionary: responseObject as NSDictionary)
            if root.status {
                self.video_arry = root.data
                self.FilterArry = self.video_arry
                self.List_TBL.isHidden = false
                self.SearchVIew.isHidden = false
                self.nodatafound.isHidden = true
                
                self.List_TBL.delegate = self
                self.List_TBL.dataSource = self
                self.List_TBL.reloadData()
            }
            else {
                self.List_TBL.isHidden = true
                self.SearchVIew.isHidden = true
                self.nodatafound.isHidden = false
            }
            ApiLoader.shared.stopHUD()
        } onError: { (message, code) in
            self.List_TBL.isHidden = true
            self.SearchVIew.isHidden = true
            self.nodatafound.isHidden = false
            ApiLoader.shared.stopHUD()
        }
    }

    // MARK:- IBAction Methods
    
    @IBAction func TappedNavActionBTN(_ sender: UIButton) {
        if sender == self.MenuBTN {
            let Menu = MenuVC.init(nibName: "MenuVC", bundle: nil)
            Menu.OptionSelection = "pulse"
            self.navigationController?.pushViewController(Menu, animated: true)
        }
        else {
            let setting = SettingVC.init(nibName: "SettingVC", bundle: nil)
            let vc = UINavigationController(rootViewController: setting)
            vc.setNavigationBarHidden(true, animated: true)
            vc.modalPresentationStyle = .overCurrentContext
            present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func TappedSpeaker(_ sender: Any) {
        main {
            self.voiceOverlayController.start(on: self, textHandler: { (text, final, Other) in
                print("voice output: \(String(describing: text))")
                print("voice output: is it final? \(String(describing: final))")
                if final {
                    self.FilterArry.removeAll()
                    self.FilterArry = self.video_arry.filter({ (videos) -> Bool in
                        return videos.title.uppercased().contains(text.uppercased())
                    })
                    self.List_TBL.reloadData()
                }
            }, errorHandler: { (error) in
                print("voice output: error \(String(describing: error))")
            })
        }
    }

}

extension VideosVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.FilterArry.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VideoCell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as! VideoCell
        let data = self.FilterArry[indexPath.row]
        if data.image.IsStrEmpty() {
            cell.banner.image = UIImage.init(named: "DemoImage")
        }
        else {
            cell.banner.downloadedFrom(link: data.image, contentMode: .scaleAspectFill, radious: 10)
        }
        cell.title.text = data.title
        cell.auther.text = data.userId
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.FilterArry[indexPath.row]
        let video = VideoDetailVC.init(nibName: "VideoDetailVC", bundle: nil)
        video.videoID = data.video
        let vc = UINavigationController(rootViewController: video)
        vc.setNavigationBarHidden(true, animated: true)
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true, completion: nil)
    }
    
}

extension VideosVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.FilterArry.removeAll()
        self.FilterArry = self.video_arry
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.TXTSearch.resignFirstResponder()
        self.view.endEditing(true)
        self.FilterArry.removeAll()
        self.FilterArry = self.video_arry
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        self.FilterArry.removeAll()
        if newString.length == 0 {
            self.FilterArry = self.video_arry
        }
        else {
            self.FilterArry = self.video_arry.filter({ (videos) -> Bool in
                return videos.title.uppercased().contains(newString.uppercased)
            })
        }
        self.List_TBL.reloadData()
        return true
    }
    
}
