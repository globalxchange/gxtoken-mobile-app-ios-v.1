//
//  VideoCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 26/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class VideoCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var banner: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var ico: UIImageView!
    @IBOutlet weak var auther: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.view.backgroundColor = ViewBGColor
        self.view.clipsToBounds = true
        self.view.layer.cornerRadius = 10
        self.view.layer.borderColor = LightGroup.cgColor
        self.view.layer.borderWidth = 1
        
        self.title.font = Font.MontserratRegularFont(font: 18.0)
        self.title.textColor = ShadowDarkBlue
        self.title.textAlignment = .left
        
        self.auther.font = Font.MontserratRegularFont(font: 13.0)
        self.auther.textColor = ShadowDarkBlue
        self.auther.textAlignment = .right
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
