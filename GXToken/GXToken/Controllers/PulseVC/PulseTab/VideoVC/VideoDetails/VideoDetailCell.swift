//
//  VideoDetailCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 27/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class VideoDetailCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var banner: UIImageView!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.view.backgroundColor = ViewBGColor
        self.view.clipsToBounds = true
        self.view.layer.cornerRadius = 10
        self.view.layer.borderColor = LightGroup.cgColor
        self.view.layer.borderWidth = 1
        
        self.title.textAlignment = .left
    }

    func Setuptitle(title: String, details: String) {
        let mainstring: String = title + details
        let myMutableString = mainstring.Attributestring(attribute: [(title, Font.MontserratRegularFont(font: 17), DarkBlue), (details, Font.MontserratRegularFont(font: 11), DarkBlue)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [title, details])))
        self.title.attributedText = myMutableString
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
