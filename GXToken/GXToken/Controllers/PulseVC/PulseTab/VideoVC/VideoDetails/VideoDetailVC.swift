//
//  VideoDetailVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 27/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class VideoDetailVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    
    @IBOutlet var bottomlbl: [UILabel]!
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet weak var SepratorLBL: UILabel!
    @IBOutlet weak var Logo: UIImageView!
    
    @IBOutlet weak var VideoView: UIView!
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var Segments: UISegmentedControl!
    @IBOutlet weak var ListTBL: UITableView!
    
    // MARK:- Variable Defines
    var videoID: String = ""
    
    // MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        self.MainView.clipsToBounds = true
        
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.GetVideo()
        self.Setup()
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.NavLogo_IMG.tintColor = DarkBlue
    }

    // MARK:- API Calling
    func GetVideo() {
        ApiLoader.shared.showHUD()
        NetworkingRequests.shared.requestPOST(CommanApiCall.API_Pulse_videoid.value(), Parameters: ["video_id": self.videoID], Headers: [:]) { (responseObject, status) in
            if status {
                
            }
            else {
                self.dismiss(animated: true, completion: nil)
            }
            ApiLoader.shared.stopHUD()
        } onError: { (message, code) in
            ApiLoader.shared.stopHUD()
        }

    }
    
    // MARK:- User Define Methods

    func Setup() {
        self.SepratorLBL.backgroundColor = ShadowDarkBlue.withAlphaComponent(0.3)
        self.VideoView.backgroundColor = ViewBGColor
        
        self.TitleLBL.textColor = ShadowDarkBlue
        self.TitleLBL.font = Font.MontserratRegularFont(font: 18)
        self.TitleLBL.text = "How To User The GXToken Mobile App For IOS"
        
        self.Segments.backgroundColor = ViewBGColor
        self.Segments.tintColor = ViewBGColor
        self.Segments.selectedSegmentIndex = 1
        self.Segments.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: DarkBlue, NSAttributedString.Key.font: Font.MontserratSemiBoldFont(font: 15)], for: .selected)
        self.Segments.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: DarkBlue, NSAttributedString.Key.font: Font.MontserratRegularFont(font: 15)], for: .normal)
        
        self.ListTBL.register(UINib.init(nibName: "VideoDetailCell", bundle: nil), forCellReuseIdentifier: "VideoDetailCell")
        
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.ListTBL.frame.width, height: 30))
        footer.backgroundColor = ViewBGColor
        self.ListTBL.tableFooterView = footer
        
        self.ListTBL.delegate = self
        self.ListTBL.dataSource = self
        self.ListTBL.reloadData()
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedSegment(_ sender: UISegmentedControl) {
        self.ListTBL.reloadData()
    }
    
}

extension VideoDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VideoDetailCell = tableView.dequeueReusableCell(withIdentifier: "VideoDetailCell") as! VideoDetailCell
        cell.banner.backgroundColor = DarkBlue
        cell.Setuptitle(title: "What is Bitcoin?\n", details: "Bitcoin is digital and virtual currency created in 2009 that uses peer-to-peer technology to faciliate instant payments.")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
}
