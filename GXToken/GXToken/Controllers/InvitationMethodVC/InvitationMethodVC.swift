//
//  InvitationMethodVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 09/10/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class InvitationMethodVC: BaseAppVC {
    
    // MARK:- IBOutlets
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var NavLogo_IMG: UIImageView!
    
    @IBOutlet var bottomlbl: [UILabel]!
    
    @IBOutlet weak var NavView: UIView!
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet weak var SepratorLBL: UILabel!
    @IBOutlet weak var Logo: UIImageView!
    
    @IBOutlet weak var TitleIMG: UIImageView!
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet var MethodView: [UIView]!
    @IBOutlet var subMethod: [UIView]!
    @IBOutlet var MethodIMG: [UIImageView]!
    @IBOutlet var MethodLBL: [UILabel]!
    @IBOutlet var MethodBTN: [UIButton]!
    
    @IBOutlet weak var ProcessBTN: UIButton!
    
    var Selected_Index: Int = -1
    
    
    // MARK:- View Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        self.MainView.clipsToBounds = true
        
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.Setup()
        self.NavLogo_IMG.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "fullcolourlogo")
        self.NavLogo_IMG.tintColor = DarkBlue
    }

    // MARK:- API Calling
    
    
    // MARK:- User Define Methods

    func Setup() {
        self.SepratorLBL.backgroundColor = ShadowDarkBlue.withAlphaComponent(0.3)
        
        self.TitleIMG.image = UIImage.init(named: "IC_Invite_Step")
        
        let title = "Invitation Method\n\n"
        let detail = "How Do You Want To Send Your Friend The App?"
        let mainstring: String = title + detail
        let myMutableString = mainstring.Attributestring(attribute: [(title, Font.MontserratRegularFont(font: 30.0), DarkBlue), (detail, Font.MontserratRegularFont(font: 12.0), DarkBlue)], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [title, detail])))
        self.TitleLBL.attributedText = myMutableString
        
        for (index, item) in self.MethodView.enumerated() {
            item.backgroundColor = ViewBGColor
            self.subMethod[index].backgroundColor = ViewBGColor
            self.subMethod[index].clipsToBounds = true
            self.subMethod[index].layer.cornerRadius = 5
            self.subMethod[index].layer.borderWidth = 0.3
            self.subMethod[index].layer.borderColor = ShadowBlue.withAlphaComponent(0.5).cgColor
            self.MethodLBL[index].font = Font.MontserratRegularFont(font: 11)
            if index == 0 {
                self.MethodIMG[index].image = UIImage.init(named: "IC_Gmail")
                self.MethodLBL[index].text = "Email"
            }
            else {
                self.MethodIMG[index].image = UIImage.init(named: "IC_Text")
                self.MethodLBL[index].text = "Text"
            }
        }
        
        self.ProcessBTN.backgroundColor = DarkBlue
        self.ProcessBTN.setTitleColor(.white, for: .normal)
        self.ProcessBTN.setTitle("Process", for: .normal)
        self.ProcessBTN.layer.cornerRadius = 10
        self.ProcessBTN.alpha = 0.5
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedDF_MainDismissBTN(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func TappedMethodBTN(_ sender: UIButton) {
        if sender.tag == 0 {
            
        }
        else {
            
        }
        self.ProcessBTN.alpha = 1
        self.Selected_Index = sender.tag
    }

    @IBAction func ProcessBTN(_ sender: Any) {
        if self.Selected_Index != -1 {
            if self.Selected_Index == 0 {
                let method = InviteOptionVC.init(nibName: "InviteOptionVC", bundle: nil)
                let vc = UINavigationController(rootViewController: method)
                vc.setNavigationBarHidden(true, animated: true)
                vc.modalPresentationStyle = .overCurrentContext
                present(vc, animated: true, completion: nil)
            }
            else {
                let method = InviteOptionVC.init(nibName: "InviteOptionVC", bundle: nil)
                method.isMobile = true
                let vc = UINavigationController(rootViewController: method)
                vc.setNavigationBarHidden(true, animated: true)
                vc.modalPresentationStyle = .overCurrentContext
                present(vc, animated: true, completion: nil)
            }
        }
    }
}
