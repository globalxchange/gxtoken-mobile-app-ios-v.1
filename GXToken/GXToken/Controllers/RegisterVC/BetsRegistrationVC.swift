//
//  BetsRegistrationVC.swift
//  GXToken
//
//  Created by Hiren on 24/07/20.
//  Copyright © 2020 Shorupan Pirakaspathy. All rights reserved.
//

import UIKit
import JWT
import CRNotifications
import IHKeyboardAvoiding

class BetsRegistrationVC: BaseAppVC {

    // MARK:- IBOutlet Define
    @IBOutlet weak var UserMain: UIView!
    @IBOutlet weak var Stackview: UIStackView!
    @IBOutlet weak var Stack_Center: NSLayoutConstraint!
    
    @IBOutlet weak var LogoView: UIView!
    @IBOutlet var LogoIMG: [UIImageView]!
    
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var UserView: UIView!
    @IBOutlet weak var TXTUser: UITextField!
    @IBOutlet weak var UserLine: UILabel!
    @IBOutlet weak var UserLBL: UILabel!
    
    @IBOutlet var BottomView: [UIView]!
    @IBOutlet weak var UserBTN: UIButton!
    
    @IBOutlet weak var ProfileMain: UIView!
    
    @IBOutlet weak var ProTitleLBL: UILabel!
    
    @IBOutlet weak var ProfilePicBTN: UIButton!
    @IBOutlet weak var ReUploadBTN: UIButton!
    
    @IBOutlet weak var CompleteBTN: UIButton!
    
    // MARK:- Variable define
    
    var ProfilePicName: String = ""
    var Username: String = ""
    var imageURL: String = ""
    var Dev_Email: String = "shorupan@gmail.com"
    
    // MARK:- View Life Cycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.backBarButtonItem?.tintColor = .white
        self.setNeedsStatusBarAppearanceUpdate()
        
        for view in self.BottomView {
            view.backgroundColor = DarkBlue
        }
        
        for img in self.LogoIMG {
            img.image = FileManagment.shared.getImageFromDocumentDirectory(imagename: "colouredicon")
        }
        
        self.setupUserView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK:- User Define Methods
    
    func setupUserView() {
        KeyboardAvoiding.avoidingView = self.Stackview
        self.UserMain.isHidden = false
        self.ProfileMain.isHidden = true
        
        self.Stack_Center.constant = 0
        self.TitleLBL.text = "Just One Sec. You Need To Compete Your Bets Profile Setup. Please Select A Unique Bets Username"
        self.TitleLBL.font = Font.MontserratRegularFont(font: 17)
        
        self.TXTUser.placeholder = "@Username"
        self.TXTUser.font = Font.MontserratBoldFont(font: 13)
        self.TXTUser.text = "@"
        self.TXTUser.textColor = ShadowDarkBlue
        self.TXTUser.delegate = self
        self.UserBTN.tintColor = ShadowDarkBlue
        self.UserLine.backgroundColor = ShadowDarkBlue
        self.TXTUser.attributedPlaceholder =
            NSAttributedString(string: "@Username", attributes: [NSAttributedString.Key.foregroundColor : UIColor.lightGray, NSAttributedString.Key.font : Font.MontserratBoldFont(font: 13)])
        
        self.UserLBL.text = "Congrats, @________ Is Available"
        self.UserLBL.font = Font.MontserratBoldFont(font: 16)
        self.UserLBL.textColor = ShadowDarkBlue
        
        self.BottomView[0].isHidden = true
        self.UserBTN.setTitle("Confirm Username", for: .normal)
        self.UserBTN.setTitleColor(.white, for: .normal)
    }
    
    func SetupProfileView(username: String, style: Int = 0) {
        self.UserMain.isHidden = true
        self.ProfileMain.isHidden = false
        
        self.ProTitleLBL.text = "Great Job '\(username)'. Just Upload A Profile Pic & You Are Done!"
        self.ProTitleLBL.font = Font.MontserratRegularFont(font: 17)
        
        self.ProfilePicBTN.clipsToBounds = true
        self.ProfilePicBTN.layer.borderColor = ShadowDarkBlue.cgColor
        self.ProfilePicBTN.layer.borderWidth = 1.0
        self.ProfilePicBTN.layer.cornerRadius = self.ProfilePicBTN.frame.height / 2
        
        self.BottomView[1].isHidden = true
        self.ReUploadBTN.isHidden = true
        
        self.ReUploadBTN.setTitle("Upload Another Pic", for: .normal)
        self.ReUploadBTN.setTitleColor(DarkBlue, for: .normal)
        self.ReUploadBTN.clipsToBounds = true
        self.ReUploadBTN.layer.borderColor = ShadowDarkBlue.cgColor
        self.ReUploadBTN.layer.borderWidth = 1.0
        
        self.CompleteBTN.setTitle("Complete Setup", for: .normal)
        self.CompleteBTN.setTitleColor(.white, for: .normal)
        
        switch style {
        case 0:
            self.ProfilePicBTN.imageView!.style = .sector
        case 1:
            self.ProfilePicBTN.imageView!.style = .centerExpand
        case 2:
            self.ProfilePicBTN.imageView!.style = .centerShrink
        case 3:
            self.ProfilePicBTN.imageView!.style = .roundWith(lineWdith: 5, lineColor: ShadowDarkBlue)
        case 4:
            self.ProfilePicBTN.imageView!.style = .wave
        default:break
        }
    }
    
    // MARK:- API Calling
    
    func VerifyUsername(uname: String) {
        background {
            let paramDict = GXUsernameCheckDict.init(email: CommonSetters().email.removeWhiteSpace(), username: uname)
            
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_GXRegister.value(), Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                main {
                    let payload = responseObject["payload"] as! String
                    if statuscode {
                        if payload.uppercased() == "Username Available".uppercased() {
                            self.BottomView[0].isHidden = false
                            self.Username = uname
                            self.UserLBL.text = "Congrats, " + self.TXTUser.text! + " Is Available"
                        }
                        else {
                            self.BottomView[0].isHidden = true
                            self.UserLBL.text = payload
                        }
                    }
                    else {
                        self.BottomView[0].isHidden = true
                        self.UserLBL.text = payload
                    }
                }
            }) { (msg, code) in
                main {
                    
                }
            }
        }
    }
    
    func GenerateJWT_taken() {
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .minute, value: 2, to: Date().localDate())
        var claims = ClaimSet()
        claims.issuer = "gxjwtenchs512"
        claims.expiration = date
        claims["email"] = self.Dev_Email
        claims["name"] = self.ProfilePicName.replaceLocalized(fromvalue: ["@", "_"], tovalue: ["", ""])
        let JWT_Token = JWT.encode(claims: claims, algorithm: .hs512("uyrw7826^&(896GYUFWE&*#GBjkbuaf".data(using: .utf8)!))
        
        let path_inside_brain = "root/Testing"
        let url = String.init(format: "%@email=%@&path=%@&token=%@&name=%@", CommanApiCall.API_UploadBaseURL.value(), self.Dev_Email, path_inside_brain, JWT_Token, self.ProfilePicName.replaceLocalized(fromvalue: ["@", "_"], tovalue: ["", ""]))
        
        self.view.isUserInteractionEnabled = false
        NetworkingRequests.shared.uploadImage(url, param: ["": ""], image: (self.ProfilePicBTN.imageView?.image)!, filename: "files", imageName: self.ProfilePicName, completion: { (response, isSuccess, message) in
            self.view.isUserInteractionEnabled = true
            if isSuccess {
                  let responseDic  = response as!Dictionary<String,Any>
                if responseDic["status"] as! Bool{
                    let localDic = responseDic["payload"] as! Dictionary<String,Any>
                    if localDic.keys.contains("upload_success") {
                        let status = localDic["upload_success"] as! Bool
                        if status {
                            if localDic.keys.contains("url") {
                                let profileImageURLStr = localDic["url"] as! String
                                self.BottomView[1].isHidden = false
                                self.imageURL = profileImageURLStr
                            }
                            else {
                                CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: localDic["message"] as! String, dismissDelay: 3, completion: {
                                    
                                })
                            }
                        }
                    }
                } else {
                    let localDic = responseDic["payload"] as! Dictionary<String,Any>
                    self.imageURL = ""
                    self.BottomView[1].isHidden = true
                    self.ProfilePicBTN.setImage(UIImage.init(named: "Profiledummy"), for: .normal)
                    self.ProfilePicName = ""
                    CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: localDic["message"] as! String, dismissDelay: 3, completion: {
                        
                    })
                }
            }
            else {
                self.imageURL = ""
                self.BottomView[1].isHidden = true
                self.ProfilePicBTN.setImage(UIImage.init(named: "Profiledummy"), for: .normal)
                self.ProfilePicName = ""
                CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message!, dismissDelay: 3, completion: {
                    
                })
            }
        }) { (progress_value, status, message) in
            if status {
                self.BottomView[1].isHidden = false
                self.ProfilePicBTN.uploadImage(image:(self.ProfilePicBTN.imageView?.image)!, progress: Float(progress_value!))
            }
            else {
                self.BottomView[1].isHidden = true
                self.imageURL = ""
                CRNotifications.showNotification(type: CRNotifications.info, title: "ERROR!", message: message!, dismissDelay: 3, completion: {
                    
                })
            }
        }
    }
    
    func RegisterBets() {
        background {
            let header = CommanHeader.init(email: CommonSetters().email.removeWhiteSpace(), token: UserInfoData.shared.GetUserToken()!)
            let paramDict = RegisterBetsParamDict.init(email: CommonSetters().email.removeWhiteSpace())
            
            NetworkingRequests.shared.requestPOST(CommanApiCall.API_UserBIO.value(), Parameters: paramDict.description, Headers: header.description, onSuccess: { (responseObject, statuscode) in
                main {
                    if statuscode {
                        let userinfo = UserBioRootClass.init(fromDictionary: responseObject as NSDictionary)
                        UserInfoData.shared.SaveObjectdata(data: userinfo.payload, forkey: BetsRegister)
                        if userinfo.payload.betsLevel {
                            App?.SetHomeVC()
                        }
                        else {
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                    else {
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                }
            }) { (msg, code) in
                main {
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func TappedUserBTN(_ sender: UIButton) {
        self.SetupProfileView(username: self.Username, style: 4)
    }
    
    @IBAction func TappedProfileBTN(_ sender: UIButton) {
        if sender == self.ProfilePicBTN || sender == self.ReUploadBTN {
            AttachmentHandler.shared.ShowSelectedAttachOption(vc: self, constant: [.photoLibrary, .camera])
            AttachmentHandler.shared.imagePickedBlock = { (selectedIMG) in
                self.ProfilePicBTN.setImage(selectedIMG, for: .normal)
                if !CommonSetters().Username.IsStrEmpty() {
                    self.ProfilePicName = String.init(format: "%@_%0.0f", CommonSetters().Username, NSDate().timeIntervalSince1970 as CVarArg)
                    self.ReUploadBTN.isHidden = false
                    self.GenerateJWT_taken()
                }
            }
        }
        else {
            if self.imageURL.count != 0 {
                background {
//                    ApiLoader.shared.showHUD()
                    let paramDict = GXUsernameDict.init(email: CommonSetters().email.removeWhiteSpace(), username: self.Username, image: self.imageURL)
                    
                    NetworkingRequests.shared.requestPOST(CommanApiCall.API_GXRegister.value(), Parameters: paramDict.description, Headers: AppUtillity.shared.Getheader(), onSuccess: { (responseObject, statuscode) in
                        main {
                            if statuscode {
                                if responseObject.keys.contains("payload") {
                                    let message = responseObject["payload"] as! String
                                    if message.uppercased() == "User Registration Success".uppercased() {
                                        self.RegisterBets()
                                    }
                                }
                            }
                            else {
//                                ApiLoader.shared.stopHUD()
                            }
                        }
                    }) { (msg, code) in
                        main {
//                            ApiLoader.shared.stopHUD()
                        }
                    }
                }
            }
        }
    }

}

extension BetsRegistrationVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.TXTUser {
            self.TappedUserBTN(self.UserBTN)
        }
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.TXTUser {
            
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.TXTUser {
            let currentString: NSString = textField.text! as NSString
            let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
            var username: String = ""
            if newString.hasPrefix("@") {
                username = String.init(format: "%@", newString)
            }
            else {
                username = String.init(format: "%@%@", "@", newString)
            }
            if username.count > 1 {
                self.VerifyUsername(uname: username)
                return true
            }
            self.TXTUser.text = String.init(format: "%@", username)
            self.UserLBL.text = "Congrats, @_______ Is Available"
            self.BottomView[0].isHidden = true
            return false
        }
        return true
    }
    
}
