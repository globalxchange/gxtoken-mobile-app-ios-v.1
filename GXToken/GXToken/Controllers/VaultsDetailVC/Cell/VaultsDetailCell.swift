//
//  VaultsDetailCell.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

class VaultsDetailCell: UITableViewCell {

    @IBOutlet weak var MainVIew: UIView!
    @IBOutlet weak var TitleLBL: UILabel!
    @IBOutlet weak var PriceLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = ViewBGColor
        self.backgroundColor = ViewBGColor
        self.MainVIew.backgroundColor = ViewBGColor
        self.MainVIew.layer.cornerRadius = 10
        self.MainVIew.layer.borderColor = LightGroup.cgColor
        self.MainVIew.layer.borderWidth = 1
        self.MainVIew.layer.borderColor = LightGroup.cgColor
        self.MainVIew.layer.shadowColor = LightGroup.cgColor
        self.MainVIew.layer.shadowRadius = 1
        self.MainVIew.layer.shadowOpacity = 0.4
        self.MainVIew.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        self.TitleLBL.font = Font.MontserratRegularFont(font: 17)
        self.TitleLBL.textColor = DarkBlue
        
        self.PriceLBL.font = Font.MontserratRegularFont(font: 17)
        self.PriceLBL.textColor = DarkBlue
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
