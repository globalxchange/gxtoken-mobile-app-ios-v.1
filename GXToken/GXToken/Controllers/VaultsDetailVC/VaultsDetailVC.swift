//
//  VaultsDetailVC.swift
//  GXToken
//
//  Created by Hiren Joshi on 01/09/20.
//  Copyright © 2020 Hiren Joshi. All rights reserved.
//

import UIKit

let up = "▲"
let left = "◀︎"
let right = "▶︎"
let down = "▼"

class VaultsDetailVC: BaseAppVC {

    // MARK:- IBOutlets Define
    // Main UI Design
    @IBOutlet weak var DismissBTN: UIButton!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet var bottomlbl: [UILabel]!
    
    @IBOutlet weak var Nautch: UILabel!
    @IBOutlet weak var BalanceDetailLBL: UILabel!
    
    @IBOutlet weak var TitleLBL: UILabel!
    
    @IBOutlet weak var OptionTBL: UITableView!
    @IBOutlet weak var TBL_Height: NSLayoutConstraint!
    
    @IBOutlet weak var Stack: UIStackView!
    @IBOutlet weak var AddBTN: UIButton!
    @IBOutlet weak var TradeBTN: UIButton!
    @IBOutlet weak var SendBTN: UIButton!
    
    // Open Trade UI Design
    @IBOutlet weak var SDMainView: UIView!
    @IBOutlet weak var BackBTN: UIButton!
    @IBOutlet weak var SDNautch: UILabel!
    @IBOutlet weak var SDBalanceDetailLBL: UILabel!
    
    @IBOutlet weak var SDOptionTBL: UITableView!
    
    
    // MARK:- Variable Define
    
    var Data : UserBalanceConvertedCrypto!
    var OpenOrder : [TraderOrderData]!
    var HistoryData = [HistorySection]()
    
    // MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.DismissBTN.isHidden = false
        self.DismissBTN.backgroundColor = .clear
        self.setupView()
    }

    // MARK:- User Define Methods
    
    func setupView() {
        self.GetOpenTrade()
        self.MainView.isHidden = false
        self.SDMainView.isHidden = true
        
        self.MainView.backgroundColor = ViewBGColor
        self.MainView.layer.cornerRadius = 20
        for lbl in self.bottomlbl {
            lbl.backgroundColor = ViewBGColor
        }
        
        self.Nautch.backgroundColor = DarkBlue

        self.SetBalance(detailtitle: "Balance \n", strprice: String.init(format: "%f ", self.Data.value).WithoutCoinPriceThumbRules(Coin: self.Data.symbol)!, strcoin: self.Data.symbol, detailnote1: "\n▲ 4.59%", detailnote2: " (+212.64)")
        
        self.TitleLBL.text = "Examine Your Transactions"
        self.TitleLBL.textColor = DarkBlue
        self.TitleLBL.font = Font.MontserratRegularFont(font: 17)
        
        self.OptionTBL.register(UINib.init(nibName: "VaultsDetailCell", bundle: nil), forCellReuseIdentifier: "VaultsDetailCell")
        self.OptionTBL.isHidden = false
        self.OptionTBL.backgroundColor = .clear
        self.TBL_Height.constant = CGFloat(80 * 4)
        
        self.AddBTN.setTitle("Add", for: .normal)
        self.AddBTN.titleLabel!.font = Font.MontserratRegularFont(font: 17)
        self.AddBTN.setTitleColor(DarkBlue, for: .normal)
        self.AddBTN.backgroundColor = ViewBGColor
        self.AddBTN.layer.borderColor = UIColor.black.cgColor
        self.AddBTN.layer.shadowColor = UIColor.black.cgColor
        self.AddBTN.layer.shadowRadius = 1
        self.AddBTN.layer.shadowOpacity = 0.4
        self.AddBTN.layer.shadowOffset = CGSize(width: 0, height: 0)
        
        self.TradeBTN.setTitle("Trade", for: .normal)
        self.TradeBTN.titleLabel!.font = Font.MontserratRegularFont(font: 17)
        self.TradeBTN.setTitleColor(.white, for: .normal)
        self.TradeBTN.backgroundColor = DarkBlue
        
        self.SendBTN.setTitle("Send", for: .normal)
        self.SendBTN.titleLabel!.font = Font.MontserratRegularFont(font: 17)
        self.SendBTN.setTitleColor(DarkBlue, for: .normal)
        self.SendBTN.backgroundColor = ViewBGColor
        self.SendBTN.backgroundColor = ViewBGColor
        self.SendBTN.layer.borderColor = UIColor.black.cgColor
        self.SendBTN.layer.shadowColor = UIColor.black.cgColor
        self.SendBTN.layer.shadowRadius = 1
        self.SendBTN.layer.shadowOpacity = 0.4
        self.SendBTN.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    func SetBalance(detailtitle: String, strprice: String, strcoin: String, detailnote1: String, detailnote2: String) {
        let mainstring: String = detailtitle + strprice + strcoin + detailnote1 + detailnote2
        let myMutableString = mainstring.Attributestring(attribute: [
            (detailtitle, Font.MontserratRegularFont(font: 18), DarkBlue),
            (strprice, Font.MontserratSemiBoldFont(font: 40.0), DarkBlue),
            (strcoin, Font.MontserratSemiBoldFont(font: 25), DarkBlue),
            (detailnote1, Font.MontserratSemiBoldFont(font: 13), DarkGreen),
            (detailnote2, Font.MontserratRegularFont(font: 13), DarkBlue)
        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [detailtitle ,strprice ,strcoin ,detailnote1 ,detailnote2])))
        self.BalanceDetailLBL.attributedText = myMutableString
    }
    
    func setupSDview(base: String, selected: TraderOrderData) {
        self.GetHistoryTrade(base: base)
        self.MainView.isHidden = true
        self.SDMainView.isHidden = false
        
        self.SDMainView.backgroundColor = ViewBGColor
        self.SDMainView.layer.cornerRadius = 20
        
        self.SDNautch.backgroundColor = DarkBlue
        
        let value = String.init(format: " %@ %@ \n", selected.status, selected.status.uppercased() == "OPEN" ? "Orders" : "Trades")
        
        self.SetSDBalance(detailtitle: value, strprice: String.init(format: "%f", selected.price).WithoutCoinPriceThumbRules(Coin: self.Data.symbol)!, strcoin: self.Data.symbol)
        
        self.SDOptionTBL.register(UINib.init(nibName: "VaultsDetailCell", bundle: nil), forCellReuseIdentifier: "VaultsDetailCell")
        self.SDOptionTBL.isHidden = false
        self.SDOptionTBL.backgroundColor = .clear
    }
    
    func SetSDBalance(detailtitle: String, strprice: String, strcoin: String) {
        let mainstring: String = detailtitle + strprice + strcoin
        let myMutableString = mainstring.Attributestring(attribute: [
            (detailtitle, Font.MontserratRegularFont(font: 18), DarkBlue),
            (strprice, Font.MontserratSemiBoldFont(font: 40.0), DarkBlue),
            (strcoin, Font.MontserratSemiBoldFont(font: 25), DarkBlue)
        ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [detailtitle ,strprice ,strcoin])))
        self.SDBalanceDetailLBL.attributedText = myMutableString
    }
    
    // MARK:- API Calling
    
    func GetOpenTrade() {
        background {
            ApiLoader.shared.showHUD()
//            let param = OrderHistParamDict.init(basepair: self.Data.symbol)
            let header = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
            NetworkingRequests.shared.requestsGET(CommanApiCall.API_OpenTradeOrder.value(), Parameters: [:], Headers: header.description, onSuccess: { (responseObject) in
                main {
                    let Data = TraderOrderRootClass.init(fromDictionary: responseObject as NSDictionary)
                    if Data.status {
                        self.OpenOrder = Data.data
                    }
                    self.OptionTBL.delegate = self
                    self.OptionTBL.dataSource = self
                    self.OptionTBL.reloadData()
                    ApiLoader.shared.stopHUD()
                }
            }) { (Message, Code) in
                main {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func GetHistoryTrade(base: String) {
        background {
            ApiLoader.shared.showHUD()
            let param = OrderHistParamDict.init(basepair: base)
            let header = CommanHeader.init(email: CommonSetters().email, token: CommonSetters().token)
            NetworkingRequests.shared.requestsGET(CommanApiCall.API_CloseTradeOrder.value(), Parameters: param.description, Headers: header.description, onSuccess: { (responseObject) in
                main {
                    let Data = OrderHistoryRootClass.init(fromDictionary: responseObject as NSDictionary)
                    if Data.status {
                        self.ArrangeHistorySection(data: Data.data)
                    }
                    self.SDOptionTBL.delegate = self
                    self.SDOptionTBL.dataSource = self
                    self.SDOptionTBL.reloadData()
                    ApiLoader.shared.stopHUD()
                }
            }) { (Message, Code) in
                main {
                    ApiLoader.shared.stopHUD()
                }
            }
        }
    }
    
    func ArrangeHistorySection(data: [OrderHistoryData]) {
        for item in data {
            let sectionkey = item.timeStamp.getDateStringFromUTC()
            var old = self.HistoryData.filter { (obj) -> Bool in
                return obj.SectionKey == sectionkey
            }.first
            if old == nil {
                var section = HistorySection()
                section.SectionKey = sectionkey
                section.SectionArray = [OrderHistoryData]()
                section.SectionArray.append(item)
                self.HistoryData.append(section)
            }
            else {
                old?.SectionArray.append(item)
                let index = self.HistoryData.enumerated().filter({ $0.element.SectionKey == sectionkey }).map({ $0.offset }).first
                self.HistoryData.remove(at: index!)
                self.HistoryData.append(old!)
            }
        }
    }
    
    // MARK:- IBAction Methods
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func TappedAddBTN(_ sender: UIButton) {
        let vc = TradeStep1VC.init(nibName: "TradeStep1VC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.BalanceData = self.Data
        vc.FromCome = "Add"
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func TappedTradeBTN(_ sender: Any) {
        App?.SetHomeVC(selected: self.Data.symbol)
    }
    
    @IBAction func TappedSendBTN(_ sender: Any) {
        let vc = TradeStep1VC.init(nibName: "TradeStep1VC", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.BalanceData = self.Data
        vc.FromCome = "Send"
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func TappedSDBackBTN(_ sender: Any) {
        self.MainView.isHidden = false
        self.SDMainView.isHidden = true
    }
    
}

extension VaultsDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == self.OptionTBL {
            return 1
        }
        else {
            return self.HistoryData.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.OptionTBL {
            return self.OpenOrder.count
        }
        else {
            return self.HistoryData[section].SectionArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == self.OptionTBL {
            return 0
        }
        else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == self.OptionTBL {
            return nil
        }
        else {
            let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
            view.backgroundColor = ViewBGColor
            
            let title = UILabel.init(frame: CGRect.init(x: 5, y: 15, width: tableView.frame.width, height: 20))
            title.font = Font.MontserratSemiBoldFont(font: 15)
            title.textColor = DarkBlue
            title.text = self.HistoryData[section].SectionKey
            view.addSubview(title)
            return view
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: VaultsDetailCell = tableView.dequeueReusableCell(withIdentifier: "VaultsDetailCell") as! VaultsDetailCell
        if tableView == self.OptionTBL {
            self.ConfigCell(cell: cell, indexPath: indexPath)
        }
        else {
            self.ConfigHistoryCell(cell: cell, indexPath: indexPath)
        }
        return cell
    }
    
    func ConfigCell(cell: VaultsDetailCell, indexPath: IndexPath) {
        let data = self.OpenOrder[indexPath.row]
        if data.type.uppercased() == "BUY".uppercased() {
            let arrow = "▲"
            let value = String.init(format: " %@ %@", data.status, data.status.uppercased() == "OPEN" ? "Orders" : "Trades")
            let mainstring: String = arrow + value
            let myMutableString = mainstring.Attributestring(attribute: [
                (arrow, Font.MontserratRegularFont(font: 17), DarkBlue),
                (value, Font.MontserratRegularFont(font: 17.0), DarkBlue)
            ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [arrow ,value])))
            cell.TitleLBL.attributedText = myMutableString
        }
        else {
            let arrow = "▼"
            let value = String.init(format: " %@ %@", data.status, data.status.uppercased() == "OPEN" ? "Orders" : "Trades")
            let mainstring: String = arrow + value
            let myMutableString = mainstring.Attributestring(attribute: [
                (arrow, Font.MontserratRegularFont(font: 17), DarkRed),
                (value, Font.MontserratRegularFont(font: 17.0), DarkBlue)
            ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [arrow ,value])))
            cell.TitleLBL.attributedText = myMutableString
        }
        cell.PriceLBL.text = String.init(format: "%f", self.OpenOrder[indexPath.row].price).CoinPriceThumbRules(Coin: self.Data.symbol)!
    }
    
    func ConfigHistoryCell(cell: VaultsDetailCell, indexPath: IndexPath) {
        let sectiondata = self.HistoryData[indexPath.section]
        let data = sectiondata.SectionArray[indexPath.row]
        if data.type.uppercased() == "BUY".uppercased() {
            let arrow = "▲ "
            let value = String.init(format: " %f", data.completedAmount).WithoutCoinPriceThumbRules(Coin: "GXT")!
            let mainstring: String = arrow + value.appending(" GXT")
            let myMutableString = mainstring.Attributestring(attribute: [
                (arrow, Font.MontserratRegularFont(font: 17), DarkBlue),
                (value, Font.MontserratRegularFont(font: 17.0), DarkBlue)
            ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [arrow ,value])))
            cell.TitleLBL.attributedText = myMutableString
            cell.PriceLBL.text = String.init(format: "%f", data.price).CoinPriceThumbRules(Coin: self.Data.symbol)!
        }
        else {
            let arrow = "▼ "
            let value = String.init(format: " %f", data.amount).WithoutCoinPriceThumbRules(Coin: "GXT")!
            let mainstring: String = arrow + value.appending(" GXT")
            let myMutableString = mainstring.Attributestring(attribute: [
                (arrow, Font.MontserratRegularFont(font: 17), DarkRed),
                (value, Font.MontserratRegularFont(font: 17.0), DarkBlue)
            ], with: mainstring, with: NSArray.init(array: mainstring.FindSubString(inputStr: mainstring, subStrings: [arrow ,value])))
            cell.TitleLBL.attributedText = myMutableString
            cell.PriceLBL.text = String.init(format: "%f", data.price).CoinPriceThumbRules(Coin: self.Data.symbol)!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.OptionTBL {
            let data = self.OpenOrder[indexPath.row]
            self.setupSDview(base: data.basePair, selected: data)
        }
        else {
            
        }
    }
    
}
