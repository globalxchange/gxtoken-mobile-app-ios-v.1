//
//	Data.swift
//
//	Create by Hiren Joshi on 2/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct TraderOrderData{

	var id : String!
	var amount : Double!
	var basePair : String!
	var cognitoName : String!
	var completedAmount : Double!
	var email : String!
	var filledAt : String!
	var lastTransactionAt : Int!
	var orderType : String!
	var price : Double!
	var remainingAmount : Double!
	var status : String!
	var timeStamp : Int!
	var type : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		id = dictionary["_id"] as? String
		amount = dictionary["amount"] as? Double ?? 0.0
		basePair = dictionary["basePair"] as? String
		cognitoName = dictionary["cognitoName"] as? String
		completedAmount = dictionary["completedAmount"] as? Double ?? 0.0
		email = dictionary["email"] as? String
		filledAt = dictionary["filledAt"] as? String
		lastTransactionAt = dictionary["lastTransactionAt"] as? Int
		orderType = dictionary["orderType"] as? String
		price = dictionary["price"] as? Double ?? 0.0
		remainingAmount = dictionary["remainingAmount"] as? Double ?? 0.0
		status = dictionary["status"] as? String
		timeStamp = dictionary["timeStamp"] as? Int
		type = dictionary["type"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["_id"] = id
		}
		if amount != nil{
			dictionary["amount"] = amount
		}
		if basePair != nil{
			dictionary["basePair"] = basePair
		}
		if cognitoName != nil{
			dictionary["cognitoName"] = cognitoName
		}
		if completedAmount != nil{
			dictionary["completedAmount"] = completedAmount
		}
		if email != nil{
			dictionary["email"] = email
		}
		if filledAt != nil{
			dictionary["filledAt"] = filledAt
		}
		if lastTransactionAt != nil{
			dictionary["lastTransactionAt"] = lastTransactionAt
		}
		if orderType != nil{
			dictionary["orderType"] = orderType
		}
		if price != nil{
			dictionary["price"] = price
		}
		if remainingAmount != nil{
			dictionary["remainingAmount"] = remainingAmount
		}
		if status != nil{
			dictionary["status"] = status
		}
		if timeStamp != nil{
			dictionary["timeStamp"] = timeStamp
		}
		if type != nil{
			dictionary["type"] = type
		}
		return dictionary
	}

}
