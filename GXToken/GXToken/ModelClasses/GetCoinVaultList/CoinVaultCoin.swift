//
//    CoinVaultCoin.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class CoinVaultCoin : NSObject, NSCoding{

    var assetType : String!
    var coinFlag : String!
    var coinImage : String!
    var coinName : String!
    var coinSymbol : String!
    var coinValue : Double!
    var coinValueUSD : Double!
    var price : CoinVaultPrice!
    var symbol : String!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        assetType = dictionary["asset_type"] as? String
        coinFlag = dictionary["coinFlag"] as? String
        coinImage = dictionary["coinImage"] as? String
        coinName = dictionary["coinName"] as? String
        coinSymbol = dictionary["coinSymbol"] as? String
        coinValue = dictionary["coinValue"] as? Double ?? 0.0
        coinValueUSD = dictionary["coinValueUSD"] as? Double ?? 0.0
        if let priceData = dictionary["price"] as? [String:Any]{
            price = CoinVaultPrice(fromDictionary: priceData)
        }
        symbol = dictionary["symbol"] as? String
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if assetType != nil{
            dictionary["asset_type"] = assetType
        }
        if coinFlag != nil{
            dictionary["coinFlag"] = coinFlag
        }
        if coinImage != nil{
            dictionary["coinImage"] = coinImage
        }
        if coinName != nil{
            dictionary["coinName"] = coinName
        }
        if coinSymbol != nil{
            dictionary["coinSymbol"] = coinSymbol
        }
        if coinValue != nil{
            dictionary["coinValue"] = coinValue
        }
        if coinValueUSD != nil{
            dictionary["coinValueUSD"] = coinValueUSD
        }
        if price != nil{
            dictionary["price"] = price.toDictionary()
        }
        if symbol != nil{
            dictionary["symbol"] = symbol
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
         assetType = aDecoder.decodeObject(forKey: "asset_type") as? String
         coinFlag = aDecoder.decodeObject(forKey: "coinFlag") as? String
         coinImage = aDecoder.decodeObject(forKey: "coinImage") as? String
         coinName = aDecoder.decodeObject(forKey: "coinName") as? String
         coinSymbol = aDecoder.decodeObject(forKey: "coinSymbol") as? String
         coinValue = aDecoder.decodeObject(forKey: "coinValue") as? Double ?? 0.0
         coinValueUSD = aDecoder.decodeObject(forKey: "coinValueUSD") as? Double ?? 0.0
         price = aDecoder.decodeObject(forKey: "price") as? CoinVaultPrice
         symbol = aDecoder.decodeObject(forKey: "symbol") as? String

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
    {
        if assetType != nil{
            aCoder.encode(assetType, forKey: "asset_type")
        }
        if coinFlag != nil{
            aCoder.encode(coinFlag, forKey: "coinFlag")
        }
        if coinImage != nil{
            aCoder.encode(coinImage, forKey: "coinImage")
        }
        if coinName != nil{
            aCoder.encode(coinName, forKey: "coinName")
        }
        if coinSymbol != nil{
            aCoder.encode(coinSymbol, forKey: "coinSymbol")
        }
        if coinValue != nil{
            aCoder.encode(coinValue, forKey: "coinValue")
        }
        if coinValueUSD != nil{
            aCoder.encode(coinValueUSD, forKey: "coinValueUSD")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if symbol != nil{
            aCoder.encode(symbol, forKey: "symbol")
        }

    }

}
