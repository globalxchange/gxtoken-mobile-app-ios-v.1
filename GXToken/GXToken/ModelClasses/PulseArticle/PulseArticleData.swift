//
//	PulseArticleData.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct PulseArticleData{

	var v : Int!
	var id : String!
	var applicationId : String!
	var article : String!
	var articleMedia : [AnyObject]!
	var categoryType : [String]!
	var createdAt : String!
	var desc : String!
	var icon : String!
	var media : String!
	var navbarId : String!
	var status : String!
	var title : String!
	var updatedAt : String!
	var userId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		v = dictionary["__v"] as? Int
		id = dictionary["_id"] as? String
		applicationId = dictionary["application_id"] as? String
		article = dictionary["article"] as? String
		articleMedia = dictionary["article_media"] as? [AnyObject]
		categoryType = dictionary["categoryType"] as? [String]
		createdAt = dictionary["createdAt"] as? String
		desc = dictionary["desc"] as? String
		icon = dictionary["icon"] as? String
		media = dictionary["media"] as? String
		navbarId = dictionary["navbar_id"] as? String
		status = dictionary["status"] as? String
		title = dictionary["title"] as? String
		updatedAt = dictionary["updatedAt"] as? String
		userId = dictionary["user_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if v != nil{
			dictionary["__v"] = v
		}
		if id != nil{
			dictionary["_id"] = id
		}
		if applicationId != nil{
			dictionary["application_id"] = applicationId
		}
		if article != nil{
			dictionary["article"] = article
		}
		if articleMedia != nil{
			dictionary["article_media"] = articleMedia
		}
		if categoryType != nil{
			dictionary["categoryType"] = categoryType
		}
		if createdAt != nil{
			dictionary["createdAt"] = createdAt
		}
		if desc != nil{
			dictionary["desc"] = desc
		}
		if icon != nil{
			dictionary["icon"] = icon
		}
		if media != nil{
			dictionary["media"] = media
		}
		if navbarId != nil{
			dictionary["navbar_id"] = navbarId
		}
		if status != nil{
			dictionary["status"] = status
		}
		if title != nil{
			dictionary["title"] = title
		}
		if updatedAt != nil{
			dictionary["updatedAt"] = updatedAt
		}
		if userId != nil{
			dictionary["user_id"] = userId
		}
		return dictionary
	}

}
