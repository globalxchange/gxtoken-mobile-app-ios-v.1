//
//	AppLaunchMediaLink.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppLaunchMediaLink{

	var photo1 : String!
	var photo2 : String!
	var photo3 : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		photo1 = dictionary["photo_1"] as? String
		photo2 = dictionary["photo_2"] as? String
		photo3 = dictionary["photo_3"] as? String
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if photo1 != nil{
			dictionary["photo_1"] = photo1
		}
		if photo2 != nil{
			dictionary["photo_2"] = photo2
		}
		if photo3 != nil{
			dictionary["photo_3"] = photo3
		}
		return dictionary
	}

}