//
//	AppLaunchRootClass.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppLaunchRootClass{

	var apps : [AppLaunchApp]!
	var count : Int!
	var status : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		apps = [AppLaunchApp]()
		if let appsArray = dictionary["apps"] as? [[String:Any]]{
			for dic in appsArray{
				let value = AppLaunchApp(fromDictionary: dic)
				apps.append(value)
			}
		}
		count = dictionary["count"] as? Int
		status = dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if apps != nil{
			var dictionaryElements = [[String:Any]]()
			for appsElement in apps {
				dictionaryElements.append(appsElement.toDictionary())
			}
			dictionary["apps"] = dictionaryElements
		}
		if count != nil{
			dictionary["count"] = count
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

}