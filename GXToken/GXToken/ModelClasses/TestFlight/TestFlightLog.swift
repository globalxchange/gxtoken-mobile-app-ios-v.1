//
//	TestFlightLog.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct TestFlightLog{

	var id : String!
	var androidAppLink : String!
	var appCode : String!
	var date : String!
	var iosAppLink : String!
	var timestamp : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		id = dictionary["_id"] as? String
		androidAppLink = dictionary["android_app_link"] as? String
		appCode = dictionary["app_code"] as? String
		date = dictionary["date"] as? String
		iosAppLink = dictionary["ios_app_link"] as? String
		timestamp = dictionary["timestamp"] as? Double
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["_id"] = id
		}
		if androidAppLink != nil{
			dictionary["android_app_link"] = androidAppLink
		}
		if appCode != nil{
			dictionary["app_code"] = appCode
		}
		if date != nil{
			dictionary["date"] = date
		}
		if iosAppLink != nil{
			dictionary["ios_app_link"] = iosAppLink
		}
		if timestamp != nil{
			dictionary["timestamp"] = timestamp
		}
		return dictionary
	}

}
