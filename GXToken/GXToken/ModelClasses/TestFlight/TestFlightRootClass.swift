//
//	TestFlightRootClass.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct TestFlightRootClass{

	var count : Int!
	var logs : [TestFlightLog]!
	var status : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		count = dictionary["count"] as? Int
		logs = [TestFlightLog]()
		if let logsArray = dictionary["logs"] as? [NSDictionary]{
			for dic in logsArray{
				let value = TestFlightLog(fromDictionary: dic)
				logs.append(value)
			}
		}
		status = dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if count != nil{
			dictionary["count"] = count
		}
		if logs != nil{
			var dictionaryElements = [NSDictionary]()
			for logsElement in logs {
				dictionaryElements.append(logsElement.toDictionary())
			}
			dictionary["logs"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

}
