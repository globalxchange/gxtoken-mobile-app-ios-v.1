//
//	HomeChartsChart.swift
//
//	Create by Hiren Joshi on 4/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct HomeChartsChart{

	var basePair : String!
	var date : String!
	var stockin : Double!
	var stockout : Double!
	var timeStamp : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		basePair = dictionary["basePair"] as? String
		date = dictionary["date"] as? String
        if let indata = dictionary["in"] as? Double {
            stockin = indata
        }
        else {
            stockin = 0.1
        }
        if let outdata = dictionary["out"] as? Double {
            stockout = outdata
        }
        else {
            stockout = 0.1
        }
		timeStamp = dictionary["timeStamp"] as? Double
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if basePair != nil{
			dictionary["basePair"] = basePair
		}
		if date != nil{
			dictionary["date"] = date
		}
		if stockin != nil{
			dictionary["in"] = stockin
		}
		if stockout != nil{
			dictionary["out"] = stockout
		}
		if timeStamp != nil{
			dictionary["timeStamp"] = timeStamp
		}
		return dictionary
	}

}
