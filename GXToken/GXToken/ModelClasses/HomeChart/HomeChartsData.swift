//
//	HomeChartsData.swift
//
//	Create by Hiren Joshi on 4/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct HomeChartsData{

	var id : String!
	var basePair : String!
	var chart : [HomeChartsChart]!
	var currency : String!
	var diff : Double!
	var imageURL : String!
	var perChange : Double!
	var price : Double!
	var type : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		id = dictionary["_id"] as? String
		basePair = dictionary["basePair"] as? String
		chart = [HomeChartsChart]()
		if let chartArray = dictionary["chart"] as? [NSDictionary]{
			for dic in chartArray{
				let value = HomeChartsChart(fromDictionary: dic)
				chart.append(value)
			}
		}
		currency = dictionary["currency"] as? String
		diff = dictionary["diff"] as? Double ?? 0.0
		imageURL = dictionary["imageURL"] as? String
		perChange = dictionary["perChange"] as? Double ?? 0.0
		price = dictionary["price"] as? Double ?? 0.0
		type = dictionary["type"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["_id"] = id
		}
		if basePair != nil{
			dictionary["basePair"] = basePair
		}
		if chart != nil{
			var dictionaryElements = [NSDictionary]()
			for chartElement in chart {
				dictionaryElements.append(chartElement.toDictionary())
			}
			dictionary["chart"] = dictionaryElements
		}
		if currency != nil{
			dictionary["currency"] = currency
		}
		if diff != nil{
			dictionary["diff"] = diff
		}
		if imageURL != nil{
			dictionary["imageURL"] = imageURL
		}
		if perChange != nil{
			dictionary["perChange"] = perChange
		}
		if price != nil{
			dictionary["price"] = price
		}
		if type != nil{
			dictionary["type"] = type
		}
		return dictionary
	}

}
