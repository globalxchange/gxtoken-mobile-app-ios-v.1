//
//	AppBalanceCoinsData.swift
//
//	Create by Hiren Joshi on 12/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppBalanceCoinsData{

	var hr_change : Double!
	var id : String!
	var adminCoin : Bool!
	var assetType : String!
	var coinImage : String!
	var coinName : String!
	var coinSymbol : String!
	var coinValue : Double!
	var coinValueUSD : Double!
	var coinAddedBy : String!
	var coinAddress : String!
	var country : String!
	var date : String!
	var decimals : Double!
	var erc20Address : String!
	var ethToken : Bool!
	var gxcoin : Bool!
	var mktCap : Double!
	var nativeDeposit : Bool!
	var price : AppBalancePrice!
	var priceUsd : Double!
	var stableCoin : Bool!
	var supply : Double!
	var symbol : String!
	var ticker : String!
	var timestamp : Double!
	var token : Bool!
	var type : String!
	var usdPrice : Double!
	var volume24hr : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		hr_change = dictionary["_24hrchange"] as? Double ?? 0.0
		id = dictionary["_id"] as? String
		adminCoin = dictionary["adminCoin"] as? Bool
		assetType = dictionary["asset_type"] as? String
		coinImage = dictionary["coinImage"] as? String
		coinName = dictionary["coinName"] as? String
		coinSymbol = dictionary["coinSymbol"] as? String
		coinValue = dictionary["coinValue"] as? Double ?? 0.0
		coinValueUSD = dictionary["coinValueUSD"] as? Double ?? 0.0
		coinAddedBy = dictionary["coin_added_by"] as? String
		coinAddress = dictionary["coin_address"] as? String
		country = dictionary["country"] as? String
		date = dictionary["date"] as? String
		decimals = dictionary["decimals"] as? Double ?? 0.0
		erc20Address = dictionary["erc20Address"] as? String
		ethToken = dictionary["eth_token"] as? Bool
		gxcoin = dictionary["gxcoin"] as? Bool
		mktCap = dictionary["mkt_cap"] as? Double ?? 0.0
		nativeDeposit = dictionary["native_deposit"] as? Bool
		if let priceData = dictionary["price"] as? NSDictionary{
				price = AppBalancePrice(fromDictionary: priceData)
			}
		priceUsd = dictionary["price_usd"] as? Double ?? 0.0
		stableCoin = dictionary["stable_coin"] as? Bool
		supply = dictionary["supply"] as? Double ?? 0.0
		symbol = dictionary["symbol"] as? String
		ticker = dictionary["ticker"] as? String
		timestamp = dictionary["timestamp"] as? Double ?? 0.0
		token = dictionary["token"] as? Bool
		type = dictionary["type"] as? String
		usdPrice = dictionary["usd_price"] as? Double ?? 0.0
		volume24hr = dictionary["volume24hr"] as? Double ?? 0.0
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if hr_change != nil{
			dictionary["_24hrchange"] = hr_change
		}
		if id != nil{
			dictionary["_id"] = id
		}
		if adminCoin != nil{
			dictionary["adminCoin"] = adminCoin
		}
		if assetType != nil{
			dictionary["asset_type"] = assetType
		}
		if coinImage != nil{
			dictionary["coinImage"] = coinImage
		}
		if coinName != nil{
			dictionary["coinName"] = coinName
		}
		if coinSymbol != nil{
			dictionary["coinSymbol"] = coinSymbol
		}
		if coinValue != nil{
			dictionary["coinValue"] = coinValue
		}
		if coinValueUSD != nil{
			dictionary["coinValueUSD"] = coinValueUSD
		}
		if coinAddedBy != nil{
			dictionary["coin_added_by"] = coinAddedBy
		}
		if coinAddress != nil{
			dictionary["coin_address"] = coinAddress
		}
		if country != nil{
			dictionary["country"] = country
		}
		if date != nil{
			dictionary["date"] = date
		}
		if decimals != nil{
			dictionary["decimals"] = decimals
		}
		if erc20Address != nil{
			dictionary["erc20Address"] = erc20Address
		}
		if ethToken != nil{
			dictionary["eth_token"] = ethToken
		}
		if gxcoin != nil{
			dictionary["gxcoin"] = gxcoin
		}
		if mktCap != nil{
			dictionary["mkt_cap"] = mktCap
		}
		if nativeDeposit != nil{
			dictionary["native_deposit"] = nativeDeposit
		}
		if price != nil{
			dictionary["price"] = price.toDictionary()
		}
		if priceUsd != nil{
			dictionary["price_usd"] = priceUsd
		}
		if stableCoin != nil{
			dictionary["stable_coin"] = stableCoin
		}
		if supply != nil{
			dictionary["supply"] = supply
		}
		if symbol != nil{
			dictionary["symbol"] = symbol
		}
		if ticker != nil{
			dictionary["ticker"] = ticker
		}
		if timestamp != nil{
			dictionary["timestamp"] = timestamp
		}
		if token != nil{
			dictionary["token"] = token
		}
		if type != nil{
			dictionary["type"] = type
		}
		if usdPrice != nil{
			dictionary["usd_price"] = usdPrice
		}
		if volume24hr != nil{
			dictionary["volume24hr"] = volume24hr
		}
		return dictionary
	}

}
