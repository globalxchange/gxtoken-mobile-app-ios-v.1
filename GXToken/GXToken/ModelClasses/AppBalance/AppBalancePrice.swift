//
//	AppBalancePrice.swift
//
//	Create by Hiren Joshi on 12/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppBalancePrice{

	var aED : Double!
	var aRS : Double!
	var aUD : Double!
	var cAD : Double!
	var cNY : Double!
	var cOP : Double!
	var eUR : Double!
	var gBP : Double!
	var iDR : Double!
	var iNR : Double!
	var jPY : Double!
	var mXN : Double!
	var uSD : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		aED = dictionary["AED"] as? Double ?? 0.0
		aRS = dictionary["ARS"] as? Double ?? 0.0
		aUD = dictionary["AUD"] as? Double ?? 0.0
		cAD = dictionary["CAD"] as? Double ?? 0.0
		cNY = dictionary["CNY"] as? Double ?? 0.0
		cOP = dictionary["COP"] as? Double ?? 0.0
		eUR = dictionary["EUR"] as? Double ?? 0.0
		gBP = dictionary["GBP"] as? Double ?? 0.0
		iDR = dictionary["IDR"] as? Double ?? 0.0
		iNR = dictionary["INR"] as? Double ?? 0.0
		jPY = dictionary["JPY"] as? Double ?? 0.0
		mXN = dictionary["MXN"] as? Double ?? 0.0
		uSD = dictionary["USD"] as? Double ?? 0.0
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if aED != nil{
			dictionary["AED"] = aED
		}
		if aRS != nil{
			dictionary["ARS"] = aRS
		}
		if aUD != nil{
			dictionary["AUD"] = aUD
		}
		if cAD != nil{
			dictionary["CAD"] = cAD
		}
		if cNY != nil{
			dictionary["CNY"] = cNY
		}
		if cOP != nil{
			dictionary["COP"] = cOP
		}
		if eUR != nil{
			dictionary["EUR"] = eUR
		}
		if gBP != nil{
			dictionary["GBP"] = gBP
		}
		if iDR != nil{
			dictionary["IDR"] = iDR
		}
		if iNR != nil{
			dictionary["INR"] = iNR
		}
		if jPY != nil{
			dictionary["JPY"] = jPY
		}
		if mXN != nil{
			dictionary["MXN"] = mXN
		}
		if uSD != nil{
			dictionary["USD"] = uSD
		}
		return dictionary
	}

}
