//
//	AppBalanceRootClass.swift
//
//	Create by Hiren Joshi on 12/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppBalanceRootClass{

	var appCode : String!
	var coinsData : [AppBalanceCoinsData]!
	var profileId : String!
	var status : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		appCode = dictionary["app_code"] as? String
		coinsData = [AppBalanceCoinsData]()
		if let coinsDataArray = dictionary["coins_data"] as? [NSDictionary]{
			for dic in coinsDataArray{
				let value = AppBalanceCoinsData(fromDictionary: dic)
				coinsData.append(value)
			}
		}
		profileId = dictionary["profile_id"] as? String
		status = dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if appCode != nil{
			dictionary["app_code"] = appCode
		}
		if coinsData != nil{
			var dictionaryElements = [NSDictionary]()
			for coinsDataElement in coinsData {
				dictionaryElements.append(coinsDataElement.toDictionary())
			}
			dictionary["coins_data"] = dictionaryElements
		}
		if profileId != nil{
			dictionary["profile_id"] = profileId
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

}

struct SectionAppBalanceRootClass{

    var Fiatarray : [AppBalanceCoinsData]!
    var Cryptoarray : [AppBalanceCoinsData]!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        Fiatarray = [AppBalanceCoinsData]()
        if let sectionarrayArray = dictionary["Sectionarray"] as? [NSDictionary]{
            for dic in sectionarrayArray{
                let value = AppBalanceCoinsData(fromDictionary: dic)
                Fiatarray.append(value)
            }
        }
        Cryptoarray = [AppBalanceCoinsData]()
        if let sectionarrayArray = dictionary["Sectionarray"] as? [NSDictionary]{
            for dic in sectionarrayArray{
                let value = AppBalanceCoinsData(fromDictionary: dic)
                Cryptoarray.append(value)
            }
        }
        
    }

    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if Fiatarray != nil{
            var dictionaryElements = [NSDictionary]()
            for sectionarrayElement in Fiatarray {
                dictionaryElements.append(sectionarrayElement.toDictionary())
            }
            dictionary["Sectionarray"] = dictionaryElements
        }
        if Cryptoarray != nil{
            var dictionaryElements = [NSDictionary]()
            for sectionarrayElement in Cryptoarray {
                dictionaryElements.append(sectionarrayElement.toDictionary())
            }
            dictionary["Sectionarray"] = dictionaryElements
        }
        return dictionary
    }

}
