//
//	OrderHistoryFilledPrice.swift
//
//	Create by Hiren Joshi on 3/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct OrderHistoryFilledPrice{

	var amount : Double!
	var price : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		amount = dictionary["amount"] as? Double ?? 0.0
		price = dictionary["price"] as? Double ?? 0.0
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if amount != nil{
			dictionary["amount"] = amount
		}
		if price != nil{
			dictionary["price"] = price
		}
		return dictionary
	}

}
