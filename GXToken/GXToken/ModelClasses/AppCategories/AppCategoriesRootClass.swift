//
//	AppCarouselRootClass.swift
//
//	Create by Mom on 29/10/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppCategoriesRootClass{

	var categories : [AppCategoriesCategory]!
	var status : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		categories = [AppCategoriesCategory]()
		if let categoriesArray = dictionary["categories"] as? [NSDictionary]{
			for dic in categoriesArray{
				let value = AppCategoriesCategory(fromDictionary: dic)
				categories.append(value)
			}
		}
		status = dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if categories != nil{
			var dictionaryElements = [NSDictionary]()
			for categoriesElement in categories {
				dictionaryElements.append(categoriesElement.toDictionary())
			}
			dictionary["categories"] = dictionaryElements
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

}
