//
//	AppCarouselCategory.swift
//
//	Create by Mom on 29/10/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppCategoriesCategory{

	var id : String!
	var categoryId : String!
	var createdBy : String!
	var date : String!
	var descriptionField : String!
	var icon : String!
	var lastUpdated : Int!
	var name : String!
	var timestamp : Int!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		id = dictionary["_id"] as? String
		categoryId = dictionary["category_id"] as? String
		createdBy = dictionary["created_by"] as? String
		date = dictionary["date"] as? String
		descriptionField = dictionary["description"] as? String
		icon = dictionary["icon"] as? String
		lastUpdated = dictionary["lastUpdated"] as? Int
		name = dictionary["name"] as? String
		timestamp = dictionary["timestamp"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if id != nil{
			dictionary["_id"] = id
		}
		if categoryId != nil{
			dictionary["category_id"] = categoryId
		}
		if createdBy != nil{
			dictionary["created_by"] = createdBy
		}
		if date != nil{
			dictionary["date"] = date
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if icon != nil{
			dictionary["icon"] = icon
		}
		if lastUpdated != nil{
			dictionary["lastUpdated"] = lastUpdated
		}
		if name != nil{
			dictionary["name"] = name
		}
		if timestamp != nil{
			dictionary["timestamp"] = timestamp
		}
		return dictionary
	}

}
