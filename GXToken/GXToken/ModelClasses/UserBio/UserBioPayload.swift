//
//    UserBioPayload.swift
//
//    Create by Hiren on 6/8/2020
//    Copyright © 2020. All rights reserved.
//    Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class UserBioPayload : NSObject, NSCoding {
    

    var arcadeGames : [UserBioArcadeGame]!
    var betsLevel : Bool!
    var dealer : Bool!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        arcadeGames = [UserBioArcadeGame]()
        if let arcadeGamesArray = dictionary["arcade_games"] as? [NSDictionary]{
            for dic in arcadeGamesArray{
                let value = UserBioArcadeGame(fromDictionary: dic)
                arcadeGames.append(value)
            }
        }
        betsLevel = dictionary["bets_level"] as? Bool
        dealer = dictionary["dealer"] as? Bool
    }

    /**
     * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> NSDictionary
    {
        let dictionary = NSMutableDictionary()
        if arcadeGames != nil{
            var dictionaryElements = [NSDictionary]()
            for arcadeGamesElement in arcadeGames {
                dictionaryElements.append(arcadeGamesElement.toDictionary())
            }
            dictionary["arcade_games"] = dictionaryElements
        }
        if betsLevel != nil{
            dictionary["bets_level"] = betsLevel
        }
        if dealer != nil{
            dictionary["dealer"] = dealer
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        arcadeGames = aDecoder.decodeObject(forKey: "arcade_games") as? [UserBioArcadeGame]
         betsLevel = aDecoder.decodeObject(forKey: "bets_level") as? Bool
         dealer = aDecoder.decodeObject(forKey: "dealer") as? Bool

    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
    {
        if arcadeGames != nil{
            aCoder.encode(arcadeGames, forKey: "arcade_games")
        }
        if betsLevel != nil{
            aCoder.encode(betsLevel, forKey: "bets_level")
        }
        if dealer != nil{
            aCoder.encode(dealer, forKey: "dealer")
        }

    }
    
    func encode(with aCoder: NSCoder) {
        if arcadeGames != nil{
            aCoder.encode(arcadeGames, forKey: "arcade_games")
        }
        if betsLevel != nil{
            aCoder.encode(betsLevel, forKey: "bets_level")
        }
        if dealer != nil{
            aCoder.encode(dealer, forKey: "dealer")
        }
    }

}
