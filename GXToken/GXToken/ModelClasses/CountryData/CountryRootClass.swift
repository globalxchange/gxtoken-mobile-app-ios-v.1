//
//	CountryRootClass.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CountryRootClass{

	var alpha2Code : String!
	var alpha3Code : String!
	var altSpellings : [String]!
	var area : Float!
	var borders : [String]!
	var callingCodes : [String]!
	var capital : String!
	var cioc : String!
	var currencies : [CountryCurrency]!
	var demonym : String!
	var flag : String!
	var gini : Float!
	var languages : [CountryLanguage]!
	var latlng : [Float]!
	var name : String!
	var nativeName : String!
	var numericCode : String!
	var population : Int!
	var region : String!
	var regionalBlocs : [CountryRegionalBloc]!
	var subregion : String!
	var timezones : [String]!
	var topLevelDomain : [String]!
	var translations : CountryTranslation!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		alpha2Code = dictionary["alpha2Code"] as? String
		alpha3Code = dictionary["alpha3Code"] as? String
		altSpellings = dictionary["altSpellings"] as? [String]
		area = dictionary["area"] as? Float
		borders = dictionary["borders"] as? [String]
		callingCodes = dictionary["callingCodes"] as? [String]
		capital = dictionary["capital"] as? String
		cioc = dictionary["cioc"] as? String
		currencies = [CountryCurrency]()
		if let currenciesArray = dictionary["currencies"] as? [NSDictionary]{
			for dic in currenciesArray{
				let value = CountryCurrency(fromDictionary: dic)
				currencies.append(value)
			}
		}
		demonym = dictionary["demonym"] as? String
		flag = dictionary["flag"] as? String
		gini = dictionary["gini"] as? Float
		languages = [CountryLanguage]()
		if let languagesArray = dictionary["languages"] as? [NSDictionary]{
			for dic in languagesArray{
				let value = CountryLanguage(fromDictionary: dic)
				languages.append(value)
			}
		}
		latlng = dictionary["latlng"] as? [Float]
		name = dictionary["name"] as? String
		nativeName = dictionary["nativeName"] as? String
		numericCode = dictionary["numericCode"] as? String
		population = dictionary["population"] as? Int
		region = dictionary["region"] as? String
		regionalBlocs = [CountryRegionalBloc]()
		if let regionalBlocsArray = dictionary["regionalBlocs"] as? [NSDictionary]{
			for dic in regionalBlocsArray{
				let value = CountryRegionalBloc(fromDictionary: dic)
				regionalBlocs.append(value)
			}
		}
		subregion = dictionary["subregion"] as? String
		timezones = dictionary["timezones"] as? [String]
		topLevelDomain = dictionary["topLevelDomain"] as? [String]
		if let translationsData = dictionary["translations"] as? NSDictionary{
				translations = CountryTranslation(fromDictionary: translationsData)
			}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if alpha2Code != nil{
			dictionary["alpha2Code"] = alpha2Code
		}
		if alpha3Code != nil{
			dictionary["alpha3Code"] = alpha3Code
		}
		if altSpellings != nil{
			dictionary["altSpellings"] = altSpellings
		}
		if area != nil{
			dictionary["area"] = area
		}
		if borders != nil{
			dictionary["borders"] = borders
		}
		if callingCodes != nil{
			dictionary["callingCodes"] = callingCodes
		}
		if capital != nil{
			dictionary["capital"] = capital
		}
		if cioc != nil{
			dictionary["cioc"] = cioc
		}
		if currencies != nil{
			var dictionaryElements = [NSDictionary]()
			for currenciesElement in currencies {
				dictionaryElements.append(currenciesElement.toDictionary())
			}
			dictionary["currencies"] = dictionaryElements
		}
		if demonym != nil{
			dictionary["demonym"] = demonym
		}
		if flag != nil{
			dictionary["flag"] = flag
		}
		if gini != nil{
			dictionary["gini"] = gini
		}
		if languages != nil{
			var dictionaryElements = [NSDictionary]()
			for languagesElement in languages {
				dictionaryElements.append(languagesElement.toDictionary())
			}
			dictionary["languages"] = dictionaryElements
		}
		if latlng != nil{
			dictionary["latlng"] = latlng
		}
		if name != nil{
			dictionary["name"] = name
		}
		if nativeName != nil{
			dictionary["nativeName"] = nativeName
		}
		if numericCode != nil{
			dictionary["numericCode"] = numericCode
		}
		if population != nil{
			dictionary["population"] = population
		}
		if region != nil{
			dictionary["region"] = region
		}
		if regionalBlocs != nil{
			var dictionaryElements = [NSDictionary]()
			for regionalBlocsElement in regionalBlocs {
				dictionaryElements.append(regionalBlocsElement.toDictionary())
			}
			dictionary["regionalBlocs"] = dictionaryElements
		}
		if subregion != nil{
			dictionary["subregion"] = subregion
		}
		if timezones != nil{
			dictionary["timezones"] = timezones
		}
		if topLevelDomain != nil{
			dictionary["topLevelDomain"] = topLevelDomain
		}
		if translations != nil{
			dictionary["translations"] = translations.toDictionary()
		}
		return dictionary
	}

}
