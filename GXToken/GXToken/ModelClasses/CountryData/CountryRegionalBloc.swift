//
//	CountryRegionalBloc.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CountryRegionalBloc{

	var acronym : String!
	var name : String!
	var otherAcronyms : [AnyObject]!
	var otherNames : [AnyObject]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		acronym = dictionary["acronym"] as? String
		name = dictionary["name"] as? String
		otherAcronyms = dictionary["otherAcronyms"] as? [AnyObject]
		otherNames = dictionary["otherNames"] as? [AnyObject]
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if acronym != nil{
			dictionary["acronym"] = acronym
		}
		if name != nil{
			dictionary["name"] = name
		}
		if otherAcronyms != nil{
			dictionary["otherAcronyms"] = otherAcronyms
		}
		if otherNames != nil{
			dictionary["otherNames"] = otherNames
		}
		return dictionary
	}

}
