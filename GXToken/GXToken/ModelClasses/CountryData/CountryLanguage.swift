//
//	CountryLanguage.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CountryLanguage{

	var iso6391 : String!
	var iso6392 : String!
	var name : String!
	var nativeName : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		iso6391 = dictionary["iso639_1"] as? String
		iso6392 = dictionary["iso639_2"] as? String
		name = dictionary["name"] as? String
		nativeName = dictionary["nativeName"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if iso6391 != nil{
			dictionary["iso639_1"] = iso6391
		}
		if iso6392 != nil{
			dictionary["iso639_2"] = iso6392
		}
		if name != nil{
			dictionary["name"] = name
		}
		if nativeName != nil{
			dictionary["nativeName"] = nativeName
		}
		return dictionary
	}

}
