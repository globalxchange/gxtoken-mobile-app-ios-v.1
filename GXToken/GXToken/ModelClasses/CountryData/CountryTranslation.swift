//
//	CountryTranslation.swift
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CountryTranslation{

	var br : String!
	var de : String!
	var es : String!
	var fa : String!
	var fr : String!
	var hr : String!
	var it : String!
	var ja : String!
	var nl : String!
	var pt : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		br = dictionary["br"] as? String
		de = dictionary["de"] as? String
		es = dictionary["es"] as? String
		fa = dictionary["fa"] as? String
		fr = dictionary["fr"] as? String
		hr = dictionary["hr"] as? String
		it = dictionary["it"] as? String
		ja = dictionary["ja"] as? String
		nl = dictionary["nl"] as? String
		pt = dictionary["pt"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if br != nil{
			dictionary["br"] = br
		}
		if de != nil{
			dictionary["de"] = de
		}
		if es != nil{
			dictionary["es"] = es
		}
		if fa != nil{
			dictionary["fa"] = fa
		}
		if fr != nil{
			dictionary["fr"] = fr
		}
		if hr != nil{
			dictionary["hr"] = hr
		}
		if it != nil{
			dictionary["it"] = it
		}
		if ja != nil{
			dictionary["ja"] = ja
		}
		if nl != nil{
			dictionary["nl"] = nl
		}
		if pt != nil{
			dictionary["pt"] = pt
		}
		return dictionary
	}

}
