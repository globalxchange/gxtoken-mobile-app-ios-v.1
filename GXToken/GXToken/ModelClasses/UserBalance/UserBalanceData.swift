//
//	UserBalanceData.swift
//
//	Create by Hiren Joshi on 1/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct UserBalanceData{

	var convertedCrypto : [UserBalanceConvertedCrypto]!
	var crypto : [UserBalanceConvertedCrypto]!
	var fiat : [UserBalanceConvertedCrypto]!
	var totalCrypto : Double!
    var totalValue: Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		convertedCrypto = [UserBalanceConvertedCrypto]()
		if let convertedCryptoArray = dictionary["convertedCrypto"] as? [NSDictionary]{
			for dic in convertedCryptoArray{
				let value = UserBalanceConvertedCrypto(fromDictionary: dic)
				convertedCrypto.append(value)
			}
		}
		crypto = [UserBalanceConvertedCrypto]()
		if let cryptoArray = dictionary["crypto"] as? [NSDictionary]{
			for dic in cryptoArray{
				let value = UserBalanceConvertedCrypto(fromDictionary: dic)
				crypto.append(value)
			}
		}
		fiat = [UserBalanceConvertedCrypto]()
		if let fiatArray = dictionary["fiat"] as? [NSDictionary]{
			for dic in fiatArray{
				let value = UserBalanceConvertedCrypto(fromDictionary: dic)
				fiat.append(value)
			}
		}
        totalCrypto = dictionary["totalCrypto"] as? Double ?? 0.0
        totalValue = dictionary["totalValue"] as? Double ?? 0.0
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if convertedCrypto != nil{
			var dictionaryElements = [NSDictionary]()
			for convertedCryptoElement in convertedCrypto {
				dictionaryElements.append(convertedCryptoElement.toDictionary())
			}
			dictionary["convertedCrypto"] = dictionaryElements
		}
		if crypto != nil{
			var dictionaryElements = [NSDictionary]()
			for cryptoElement in crypto {
				dictionaryElements.append(cryptoElement.toDictionary())
			}
			dictionary["crypto"] = dictionaryElements
		}
		if fiat != nil{
			var dictionaryElements = [NSDictionary]()
			for fiatElement in fiat {
				dictionaryElements.append(fiatElement.toDictionary())
			}
			dictionary["fiat"] = dictionaryElements
		}
		if totalCrypto != nil{
			dictionary["totalCrypto"] = totalCrypto
		}
        if totalValue != nil{
            dictionary["totalValue"] = totalValue
        }
		return dictionary
	}

}
