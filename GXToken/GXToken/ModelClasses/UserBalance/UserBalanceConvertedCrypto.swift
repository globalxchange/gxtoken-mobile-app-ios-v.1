//
//	UserBalanceConvertedCrypto.swift
//
//	Create by Hiren Joshi on 1/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct UserBalanceConvertedCrypto{

	var icon : String!
	var name : String!
	var symbol : String!
	var value : Double!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		icon = dictionary["icon"] as? String
		name = dictionary["name"] as? String
		symbol = dictionary["symbol"] as? String
		value = dictionary["value"] as? Double ?? 0.0
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if icon != nil{
			dictionary["icon"] = icon
		}
		if name != nil{
			dictionary["name"] = name
		}
		if symbol != nil{
			dictionary["symbol"] = symbol
		}
		if value != nil{
			dictionary["value"] = value
		}
		return dictionary
	}

}
