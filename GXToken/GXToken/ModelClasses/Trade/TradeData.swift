//
//	TradeData.swift
//
//	Create by Hiren Joshi on 5/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct TradeData{

	var tradeOrderID : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		tradeOrderID = dictionary["tradeOrderID"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if tradeOrderID != nil{
			dictionary["tradeOrderID"] = tradeOrderID
		}
		return dictionary
	}

}
