//
//	AppListRootClass.swift
//
//	Create by Hiren Joshi on 12/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppListRootClass{

	var appList : [String]!
	var appNames : [String]!
	var count : Int!
	var status : Bool!
	var userApps : [AppListUserApp]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		appList = dictionary["app_list"] as? [String]
		appNames = dictionary["app_names"] as? [String]
		count = dictionary["count"] as? Int
		status = dictionary["status"] as? Bool
		userApps = [AppListUserApp]()
		if let userAppsArray = dictionary["userApps"] as? [NSDictionary]{
			for dic in userAppsArray{
				let value = AppListUserApp(fromDictionary: dic)
				userApps.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if appList != nil{
			dictionary["app_list"] = appList
		}
		if appNames != nil{
			dictionary["app_names"] = appNames
		}
		if count != nil{
			dictionary["count"] = count
		}
		if status != nil{
			dictionary["status"] = status
		}
		if userApps != nil{
			var dictionaryElements = [NSDictionary]()
			for userAppsElement in userApps {
				dictionaryElements.append(userAppsElement.toDictionary())
			}
			dictionary["userApps"] = dictionaryElements
		}
		return dictionary
	}

}
