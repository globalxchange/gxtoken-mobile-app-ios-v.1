//
//	AppListUserApp.swift
//
//	Create by Hiren Joshi on 12/9/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct AppListUserApp: Codable {

	var appCode : String!
	var appIcon : String!
	var appName : String!
	var profileId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		appCode = dictionary["app_code"] as? String
		appIcon = dictionary["app_icon"] as? String
		appName = dictionary["app_name"] as? String
		profileId = dictionary["profile_id"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if appCode != nil{
			dictionary["app_code"] = appCode
		}
		if appIcon != nil{
			dictionary["app_icon"] = appIcon
		}
		if appName != nil{
			dictionary["app_name"] = appName
		}
		if profileId != nil{
			dictionary["profile_id"] = profileId
		}
		return dictionary
	}

}
