//
//	CatAppsRootClass.swift
//
//	Create by Mom on 5/11/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CatAppsRootClass{

	var apps : [CatAppsApp]!
	var count : Int!
	var status : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		apps = [CatAppsApp]()
		if let appsArray = dictionary["apps"] as? [NSDictionary]{
			for dic in appsArray{
                let value = CatAppsApp(fromDictionary: dic as! [String : Any])
				apps.append(value)
			}
		}
		count = dictionary["count"] as? Int
		status = dictionary["status"] as? Bool
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if apps != nil{
			var dictionaryElements = [NSDictionary]()
			for appsElement in apps {
                dictionaryElements.append(appsElement.toDictionary() as NSDictionary)
			}
			dictionary["apps"] = dictionaryElements
		}
		if count != nil{
			dictionary["count"] = count
		}
		if status != nil{
			dictionary["status"] = status
		}
		return dictionary
	}

}
