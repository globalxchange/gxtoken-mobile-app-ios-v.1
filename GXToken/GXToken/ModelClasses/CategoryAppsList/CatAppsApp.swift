//
//    CatAppsApp.swift
//    Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CatAppsApp{

    var gXNativeapp : Bool!
    var id : String!
    var androidAppLink : String!
    var appCode : String!
    var appIcon : String!
    var appName : String!
    var carouselData : [CatAppsCarouselData]!
    var categories : [CatAppsCategory]!
    var colorCodes : [String]!
    var colouredicon : String!
    var colourtvlogo : String!
    var coverPhoto : String!
    var createdBy : String!
    var customBrandRegistration : String!
    var data : CatAppsData!
    var date : String!
    var fullcolourlogo : String!
    var fullwhitelogo : String!
    var icedInterestRateDifferential : Int!
    var interestRateDifferential : Int!
    var iosAppLink : String!
    var irdData : CatAppsData!
    var irdCoins : [AnyObject]!
    var linuxAppLink : AnyObject!
    var longDescription : String!
    var macAppLink : AnyObject!
    var mediaLinks : CatAppsData!
    var mobileApp : Bool!
    var ownersData : [AnyObject]!
    var ownershipCapital : Int!
    var ownershipCapitalBalance : Int!
    var ownershipCapitalRequired : Int!
    var ownershipCoin : String!
    var pathFeesData : CatAppsData!
    var pathFeesId : [AnyObject]!
    var profileStartCode : String!
    var registrationLink : String!
    var registrationLinkTest : String!
    var registrationlink : String!
    var scc1backgroundcolor : String!
    var scc2backgroundcolor : String!
    var scc3backgroundcolor : String!
    var scc4backgroundcolor : String!
    var shortDescription : String!
    var socialApps : [CatAppsSocialApp]!
    var spendcryptocardlogo1 : String!
    var timestamp : Int!
    var userLabel : String!
    var website : String!
    var whiteicon : String!
    var whitetvlogo : String!
    var windowsAppLink : AnyObject!


    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        gXNativeapp = dictionary["GXNativeapp"] as? Bool
        id = dictionary["_id"] as? String
        androidAppLink = dictionary["android_app_link"] as? String
        appCode = dictionary["app_code"] as? String
        appIcon = dictionary["app_icon"] as? String
        appName = dictionary["app_name"] as? String
        carouselData = [CatAppsCarouselData]()
        if let carouselDataArray = dictionary["carousel_data"] as? [[String:Any]]{
            for dic in carouselDataArray{
                let value = CatAppsCarouselData(fromDictionary: dic as NSDictionary)
                carouselData.append(value)
            }
        }
        categories = [CatAppsCategory]()
        if let categoriesArray = dictionary["categories"] as? [[String:Any]]{
            for dic in categoriesArray{
                let value = CatAppsCategory(fromDictionary: dic as NSDictionary)
                categories.append(value)
            }
        }
        colorCodes = dictionary["color_codes"] as? [String]
        colouredicon = dictionary["colouredicon"] as? String
        colourtvlogo = dictionary["colourtvlogo"] as? String
        coverPhoto = dictionary["cover_photo"] as? String
        createdBy = dictionary["created_by"] as? String
        customBrandRegistration = dictionary["custom_brand_registration"] as? String
        if let dataData = dictionary["data"] as? [String:Any]{
            data = CatAppsData(fromDictionary: dataData as NSDictionary)
            }
        date = dictionary["date"] as? String
        fullcolourlogo = dictionary["fullcolourlogo"] as? String
        fullwhitelogo = dictionary["fullwhitelogo"] as? String
        icedInterestRateDifferential = dictionary["iced_interest_rate_differential"] as? Int
        interestRateDifferential = dictionary["interest_rate_differential"] as? Int
        iosAppLink = dictionary["ios_app_link"] as? String
        if let irdDataData = dictionary["irdData"] as? [String:Any]{
                irdData = CatAppsData(fromDictionary: irdDataData as NSDictionary)
            }
        irdCoins = dictionary["ird_coins"] as? [AnyObject]
        linuxAppLink = dictionary["linux_app_link"] as? AnyObject
        longDescription = dictionary["long_description"] as? String
        macAppLink = dictionary["mac_app_link"] as? AnyObject
        if let mediaLinksData = dictionary["media_links"] as? [String:Any]{
                mediaLinks = CatAppsData(fromDictionary: mediaLinksData as NSDictionary)
            }
        mobileApp = dictionary["mobileApp"] as? Bool
        ownersData = dictionary["owners_data"] as? [AnyObject]
        ownershipCapital = dictionary["ownership_capital"] as? Int
        ownershipCapitalBalance = dictionary["ownership_capital_balance"] as? Int
        ownershipCapitalRequired = dictionary["ownership_capital_required"] as? Int
        ownershipCoin = dictionary["ownership_coin"] as? String
        if let pathFeesDataData = dictionary["pathFeesData"] as? [String:Any]{
                pathFeesData = CatAppsData(fromDictionary: pathFeesDataData as NSDictionary)
            }
        pathFeesId = dictionary["pathFeesId"] as? [AnyObject]
        profileStartCode = dictionary["profile_start_code"] as? String
        registrationLink = dictionary["registration_link"] as? String
        registrationLinkTest = dictionary["registration_link_test"] as? String
        registrationlink = dictionary["registrationlink"] as? String
        scc1backgroundcolor = dictionary["scc1backgroundcolor"] as? String
        scc2backgroundcolor = dictionary["scc2backgroundcolor"] as? String
        scc3backgroundcolor = dictionary["scc3backgroundcolor"] as? String
        scc4backgroundcolor = dictionary["scc4backgroundcolor"] as? String
        shortDescription = dictionary["short_description"] as? String
        socialApps = [CatAppsSocialApp]()
        if let socialAppsArray = dictionary["socialApps"] as? [[String:Any]]{
            for dic in socialAppsArray{
                let value = CatAppsSocialApp(fromDictionary: dic as NSDictionary)
                socialApps.append(value)
            }
        }
        spendcryptocardlogo1 = dictionary["spendcryptocardlogo1"] as? String
        timestamp = dictionary["timestamp"] as? Int
        userLabel = dictionary["user_label"] as? String
        website = dictionary["website"] as? String
        whiteicon = dictionary["whiteicon"] as? String
        whitetvlogo = dictionary["whitetvlogo"] as? String
        windowsAppLink = dictionary["windows_app_link"] as? AnyObject
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if gXNativeapp != nil{
            dictionary["GXNativeapp"] = gXNativeapp
        }
        if id != nil{
            dictionary["_id"] = id
        }
        if androidAppLink != nil{
            dictionary["android_app_link"] = androidAppLink
        }
        if appCode != nil{
            dictionary["app_code"] = appCode
        }
        if appIcon != nil{
            dictionary["app_icon"] = appIcon
        }
        if appName != nil{
            dictionary["app_name"] = appName
        }
        if carouselData != nil{
            var dictionaryElements = [[String:Any]]()
            for carouselDataElement in carouselData {
                dictionaryElements.append(carouselDataElement.toDictionary() as! [String : Any])
            }
            dictionary["carousel_data"] = dictionaryElements
        }
        if categories != nil{
            var dictionaryElements = [[String:Any]]()
            for categoriesElement in categories {
                dictionaryElements.append(categoriesElement.toDictionary() as! [String : Any])
            }
            dictionary["categories"] = dictionaryElements
        }
        if colorCodes != nil{
            dictionary["color_codes"] = colorCodes
        }
        if colouredicon != nil{
            dictionary["colouredicon"] = colouredicon
        }
        if colourtvlogo != nil{
            dictionary["colourtvlogo"] = colourtvlogo
        }
        if coverPhoto != nil{
            dictionary["cover_photo"] = coverPhoto
        }
        if createdBy != nil{
            dictionary["created_by"] = createdBy
        }
        if customBrandRegistration != nil{
            dictionary["custom_brand_registration"] = customBrandRegistration
        }
        if data != nil{
            dictionary["data"] = data.toDictionary()
        }
        if date != nil{
            dictionary["date"] = date
        }
        if fullcolourlogo != nil{
            dictionary["fullcolourlogo"] = fullcolourlogo
        }
        if fullwhitelogo != nil{
            dictionary["fullwhitelogo"] = fullwhitelogo
        }
        if icedInterestRateDifferential != nil{
            dictionary["iced_interest_rate_differential"] = icedInterestRateDifferential
        }
        if interestRateDifferential != nil{
            dictionary["interest_rate_differential"] = interestRateDifferential
        }
        if iosAppLink != nil{
            dictionary["ios_app_link"] = iosAppLink
        }
        if irdData != nil{
            dictionary["irdData"] = irdData.toDictionary()
        }
        if irdCoins != nil{
            dictionary["ird_coins"] = irdCoins
        }
        if linuxAppLink != nil{
            dictionary["linux_app_link"] = linuxAppLink
        }
        if longDescription != nil{
            dictionary["long_description"] = longDescription
        }
        if macAppLink != nil{
            dictionary["mac_app_link"] = macAppLink
        }
        if mediaLinks != nil{
            dictionary["media_links"] = mediaLinks.toDictionary()
        }
        if mobileApp != nil{
            dictionary["mobileApp"] = mobileApp
        }
        if ownersData != nil{
            dictionary["owners_data"] = ownersData
        }
        if ownershipCapital != nil{
            dictionary["ownership_capital"] = ownershipCapital
        }
        if ownershipCapitalBalance != nil{
            dictionary["ownership_capital_balance"] = ownershipCapitalBalance
        }
        if ownershipCapitalRequired != nil{
            dictionary["ownership_capital_required"] = ownershipCapitalRequired
        }
        if ownershipCoin != nil{
            dictionary["ownership_coin"] = ownershipCoin
        }
        if pathFeesData != nil{
            dictionary["pathFeesData"] = pathFeesData.toDictionary()
        }
        if pathFeesId != nil{
            dictionary["pathFeesId"] = pathFeesId
        }
        if profileStartCode != nil{
            dictionary["profile_start_code"] = profileStartCode
        }
        if registrationLink != nil{
            dictionary["registration_link"] = registrationLink
        }
        if registrationLinkTest != nil{
            dictionary["registration_link_test"] = registrationLinkTest
        }
        if registrationlink != nil{
            dictionary["registrationlink"] = registrationlink
        }
        if scc1backgroundcolor != nil{
            dictionary["scc1backgroundcolor"] = scc1backgroundcolor
        }
        if scc2backgroundcolor != nil{
            dictionary["scc2backgroundcolor"] = scc2backgroundcolor
        }
        if scc3backgroundcolor != nil{
            dictionary["scc3backgroundcolor"] = scc3backgroundcolor
        }
        if scc4backgroundcolor != nil{
            dictionary["scc4backgroundcolor"] = scc4backgroundcolor
        }
        if shortDescription != nil{
            dictionary["short_description"] = shortDescription
        }
        if socialApps != nil{
            var dictionaryElements = [[String:Any]]()
            for socialAppsElement in socialApps {
                dictionaryElements.append(socialAppsElement.toDictionary() as! [String : Any])
            }
            dictionary["socialApps"] = dictionaryElements
        }
        if spendcryptocardlogo1 != nil{
            dictionary["spendcryptocardlogo1"] = spendcryptocardlogo1
        }
        if timestamp != nil{
            dictionary["timestamp"] = timestamp
        }
        if userLabel != nil{
            dictionary["user_label"] = userLabel
        }
        if website != nil{
            dictionary["website"] = website
        }
        if whiteicon != nil{
            dictionary["whiteicon"] = whiteicon
        }
        if whitetvlogo != nil{
            dictionary["whitetvlogo"] = whitetvlogo
        }
        if windowsAppLink != nil{
            dictionary["windows_app_link"] = windowsAppLink
        }
        return dictionary
    }

}
