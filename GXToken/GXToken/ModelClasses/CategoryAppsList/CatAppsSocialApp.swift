//
//	CatAppsSocialApp.swift
//
//	Create by Mom on 5/11/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CatAppsSocialApp{

	var link : String!
	var name : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		link = dictionary["link"] as? String
		name = dictionary["name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if link != nil{
			dictionary["link"] = link
		}
		if name != nil{
			dictionary["name"] = name
		}
		return dictionary
	}

}
