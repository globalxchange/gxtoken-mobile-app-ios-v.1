//
//	CatAppsCategory.swift
//
//	Create by Mom on 5/11/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CatAppsCategory{

	var categoryId : String!
	var name : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		categoryId = dictionary["category_id"] as? String
		name = dictionary["name"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if categoryId != nil{
			dictionary["category_id"] = categoryId
		}
		if name != nil{
			dictionary["name"] = name
		}
		return dictionary
	}

}
