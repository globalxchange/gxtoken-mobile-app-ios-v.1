//
//	CatAppsCarouselData.swift
//
//	Create by Mom on 5/11/2020
//	Copyright © 2020. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

struct CatAppsCarouselData{

	var buttonlink : String!
	var buttontext : String!
	var descriptionField : String!
	var image : String!
	var title : String!
	var videolink : String!
	var videoname : String!
	var videothumbnail : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: NSDictionary){
		buttonlink = dictionary["buttonlink"] as? String
		buttontext = dictionary["buttontext"] as? String
		descriptionField = dictionary["description"] as? String
		image = dictionary["image"] as? String
		title = dictionary["title"] as? String
		videolink = dictionary["videolink"] as? String
		videoname = dictionary["videoname"] as? String
		videothumbnail = dictionary["videothumbnail"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if buttonlink != nil{
			dictionary["buttonlink"] = buttonlink
		}
		if buttontext != nil{
			dictionary["buttontext"] = buttontext
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if image != nil{
			dictionary["image"] = image
		}
		if title != nil{
			dictionary["title"] = title
		}
		if videolink != nil{
			dictionary["videolink"] = videolink
		}
		if videoname != nil{
			dictionary["videoname"] = videoname
		}
		if videothumbnail != nil{
			dictionary["videothumbnail"] = videothumbnail
		}
		return dictionary
	}

}
